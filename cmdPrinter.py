"""Contains print functions that offer coloring or extended information printing."""
import sys
import threading

printer_lock = threading.Lock()


class CMDColor():
    """color defines for print functions"""

    BLACK = "0;30;40m"
    RED = "0;31;40m"
    GREEN = "0;32;40m"
    YELLOW = "0;33;40m"     # used for status change
    BLUE = "0;34;40m"
    PURPLE = "0;35;40m"
    CYAN = "0;36;40m"       # used for debug msg
    WHITE = "0;37;40m"      # used for default msg

    BG_RED = "0;37;41m"     # used for printing error messages
    BG_GREEN = "0;30;42m"   # used for Tcom
    BG_YELLOW = "0;30;43m"
    BG_BLUE = "0;37;44m"
    # used for deadly exceptions (e.g. queue.Full, .Empty)
    BG_PURPLE = "0;30;45m"
    BG_CYAN = "0;30;46m"
    BG_WHITE = "0;30;47m"   # used for default loc

    RESET = "0m"


def print_msg(calling_instance, msg, calling_func="", where="", color_loc=CMDColor.BG_WHITE, color_msg=CMDColor.RESET):
    """prints a message with defined coloring and extended information.

    prints a message in given form:
    [<class name of calling instance>]@<calling function> (<where>): <msg>

    Args:
      calling_instance (object): the calling instance. print_msg should be called using calling_instance=self.
      msg (string): the message to be printed.
      calling_func (string): the calling functions name.        if not given it is determined using sys._getframe which gives the direct calling function.
      where (string): optional param to specify calling point in the program further.
      color_loc=CMDColor.BG_WHITE (string): color specifier for location part of printout (until :)
      color_msg=CMDColor.RESET (string):  color specifier for message part of printout (after :)
    """
    with printer_lock:  # lock shell bc of multithreading
        ESC = "\u001b["
        print(ESC + color_loc +
              "[" + type(calling_instance).__name__ + "]" +
              "@" + (("(" + calling_func + ")") if calling_func else sys._getframe(1).f_code.co_name) + ":" +
              ((" (" + where + ")") if where else "") +
              ESC + CMDColor.RESET +
              ESC + color_msg +
              " " + str(msg) +
              ESC + CMDColor.RESET)


def print_dbg(*args):
    """prints args in debug color. use like print() builtin."""
    with printer_lock:
        ESC = "\u001b["
        print(ESC + CMDColor.CYAN, args, ESC + CMDColor.RESET)
