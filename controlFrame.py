"""module file controlFrame. used to slim down the main file

classes:
    ControlFrame: the part of the gui where the control buttons are located.
"""
from tkinter import *
from tkinter import ttk
from ttkthemes import themed_tk as tk  # TODO: update GUI with themed tk
from tkinter import font


from configuration import HandlerConfig as config
from errorHandler import HandshakeException
from helper import mcuMessages as MCU
from helper import helpMessages as help

from PIL import Image, ImageTk

from programStates import States, Modes, WindupStates
from cmdPrinter import print_msg, print_dbg, CMDColor
from videoCounter import VideoCounterMessages as vcm

# import RPi.GPIO as GPIO


class ControlFrame(Frame):
    """frame where the control buttons are located.

    the access of main process' members is done directly via self.parent.member. this might be not the
    prettiest solution but the frame files are to be seen as extension from the main file with the only task to
    split the main file in smaller files.
    Buttons trigger onButtonClick callbacks, the enabled button
    change according to the system state.

    buttons:
        rewind: start the reel, where the tape is wound originally. used to drive in reverse
            direction and wind up the tape at the original reel after test run. rewinding using the
            gear is not possible because of the handler design. (pushing the tape back could lead
            the tape to getting out of the guideway)
        step back: rewind, but only one stepper motor step
        drive:  start the gear for driving in forward direction.
        step: drive, but only one stepper motor step
        stop: stop the current task
        run auto: start testing /driving /marking task. parameterframe specifies the test run
        run pos1: like start auto, but no stopping at marking position
        valve open/close: sets the valves into position for testing (probecard touching tape) at
            close, sets to position for driving at open
    """

    def __init__(self, parent, **options):
        """constructor. parent must be main process.)"""
        ttk.Frame.__init__(self, parent, **options)
        self.parent = parent
        self.help_msg_dict = dict()

        self.windup_enabled_var = IntVar()
        self.windup_enabled_var.set(1)  # set to enabled by default

        self.add_widgets()
        self.setup_help_msg_dict()

    def onClickRewind(self, step=False):
        """sends the rewind command to the mcu after stopping handler for safety reasons

        args:
            step (bool): specifies if only one step should be driven (instead of drive until stop is
                clicked)
        """
        print_msg(self, "")
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.REWIND_MSG)
            return

        self.parent.stop_handler()
        self.parent.set_state(States.DRIVE)

        # self.parent.portUSB0.write(MCU.IR_OFF)  # disable infrared barrier
        self.stop_windup()
        # self.parent.portUSB0.write(MCU.OPEN_BRIDGE)  # set gear bridge to open position
        self.parent.portUSB0.write(MCU.TOP_VALVE_UP)   # top valve up
        self.parent.portUSB0.write(MCU.BOT_VALVE_DOWN)   # bottom valve down
        if step:
            self.parent.portUSB0.write(MCU.STEP_BACK)  # make one step
            self.parent.reset_handler(no_handshake=True)
            return

        self.auto_windup(MCU.AUTOWINDUP_L)
        self.parent.portUSB0.write(
            MCU.START_REWIND(self.parent.rewind_speed.get()))

        # TODO: DISABLED. AS HANDLER CAN'T DRIVE BACK ATM
        # else:
        #     self.parent.portUSB0.write(MCU.START_REWIND(self.parent.rewind_speed.get()))

    def onClickStop(self):
        """"stops the current handler movement/task

        if current state is pause, newstate is set to idle and onclickpause is called. this triggers
        the needed resume routine but leads to the idle state (as stop should do)
        """
        # show some help if needed
        print_msg(self, "")
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.STOP_MSG)
            return

        # self.parent.portUSB0.write(MCU.IR_OFF) # TODO: remove after testing phase: just to make sure i can stop all movement with the stop button

        self.parent.reset_handler()  # resets all flags, also calls stop_handler
        self.parent.counter_frame.pbProgress.stop()

    def onClickPause(self, target_id):
        """triggers a pause.

        the pause flag gets set, and the pause on test or marking pos is  triggered with
        the corresponding check_pause_flag.

        args:
            target_id: specifies the pos to go into pause
                "m".. marking pos
                "t".. testing pos
        """
        # show some help if needed
        print_msg(self, "")
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.PAUSE_MSG)
            return
        if target_id == "m":
            print_msg(self, "pausing at marking pos, state: " +
                      States(self.parent.state).name)
            self.parent.pause_flag_marking = True
            self.bPauseMarking.state(["pressed"])
            # self.bPauseMarking
        elif target_id == "t":
            # in counting only mode just set the flag and trigger the pause directly
            if self.parent.mode == Modes.COUNTING_ONLY or self.parent.mode == Modes.COUNTING_ONLY_BACKWARDS:
                print_msg(self, "pausing counting packages, state: " +
                          States(self.parent.state).name)
                self.parent.pause_flag_counting = True
                self.parent._check_pause_flag_counting()
            else:
                print_msg(self, "pausing at testing pos, state: " +
                          States(self.parent.state).name)
                self.parent.pause_flag_testing = True
                self.bPauseTesting.state(["pressed"])

    def onClickResume(self, resume_button):
        """resumes from a pause

        resumes to the state stored in parent.safed_paused_state, the state that was paused
        the retest/remark flag specifies, if the task was run at least once. if it is False, no
        testing / marking has occurred, so the task gets started at resume.
        """
        print_msg(self, "")
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.RESUME_MSG)
            return

        if self.parent.state == States.PAUSE:
            print_msg(self, "safed pause state: " +
                      States(self.parent.safed_paused_state).name)
            resume_button.config(state="disabled")

            # saved paused state is the task that could have paused
            # if its marking, the next state can be testing, depending on the position
            if self.parent.safed_paused_state == States.MARKING:
                # the position was not marked (pause was started before marking not during, and no marking was started manually)
                if self.parent.remark is False:
                    self.parent.start_marking()
                    return
                self.parent.remark = False

                # check if current pos is also testing pos, is always checked after marking!
                if self.parent.testing_stop_needed():
                    return

            if self.parent.safed_paused_state == States.TESTING:
                # the pos was not tested (pause was started before testing not during, and no testing was started manually)
                if self.parent.retest is False:
                    self.parent.start_testing()
                    return
                self.parent.retest = False

            # otherwise its running or end of task (is checked in start_positioning)
            self.parent.start_positioning()

    def onClickDrive(self):
        """sends the drive command to the mcu after stopping handler for safety reasons"""
        print_msg(self, "")

        # show some help if needed
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(help.DRIVE_MSG)
            return

        self.parent.stop_handler()
        self.parent.set_state(States.DRIVE)
        self.stop_windup()

        # self.parent.portUSB0.write(MCU.IR_ON)  # enable infrared barrier
        self.auto_windup(MCU.AUTOWINDUP_R)
        # self.parent.portUSB0.flush()
        if self.ValveTopStop.get() == True:
            self.parent.portUSB0.write(MCU.TOP_VALVE_UP)   # top valve up
        if self.ValveBotStop.get() == True:
            self.parent.portUSB0.write(
                MCU.BOT_VALVE_DOWN)   # bottom valve down
        # start gear (O .. endless driving)
        self.parent.portUSB0.write(
            MCU.START_DRIVE(self.parent.drive_speed.get()))

    def onClickRun(self, mode="b"):
        """starts the test run task.

        the mode and state get set according to mode. then a new template gets taken to prevent
        missing packages because the template tracking tracked to the left border of the frame at
        rewind. !! this means that the position at which the tape is at click is ALWAYS used taken
        as test/marking position !! the counter gets reset at this point, so after stop it can be
        still displaying the final values from the last run.
        the task start with testing, because the first package position is now at test position and
        needs to be tested too.

        args:
            mode: running mode: b .. auto (both positions)
                                t .. test only
                                m .. marking only
        """
        print_msg(self, "mode: " + mode)

        # show some help if needed
        if self.parent.state == States.HELP:
            if mode == "Automatic":     # auto mode
                self.parent.dialog_handler.showHelp(help.RUN_MSG)
            elif mode == "Testing Only":   # test only
                self.parent.dialog_handler.showHelp(help.TESTONLY_MSG)
            elif mode == "Marking Only":   # mark only
                self.parent.dialog_handler.showHelp(help.MARKONLY_MSG)
            elif mode == "Count Only":   # count only
                self.parent.dialog_handler.showHelp(help.COUNTONLY_MSG)
            elif mode == "Count Only Backwards":  # count only backwards
                self.parent.dialog_handler.showHelp(help.COUNTONLY_MSG)
            return

        self.parent.stop_handler()  # stop handler for safety issues
        if not mode == "Feed in":
            if not self.parent.drive_timeout_set and self.parent.dialog_handler.noTimeoutSet_AskToSet():
                if self.parent.dialog_handler.setDriveTimerPopup():
                    self.parent.set_drive_timeout()
            if self.parent.drive_timeout_set:
                if mode == "Automatic":
                    self.parent.set_mode(Modes.AUTO)
                    self.parent.set_state(States.RUNNING)
                elif mode == "Testing Only":
                    self.parent.set_mode(Modes.TEST_ONLY)
                    self.parent.set_state(States.RUNNING)
                elif mode == "Marking Only":
                    self.parent.set_mode(Modes.MARKING_ONLY)
                    self.parent.set_state(States.RUNNING)
                elif mode == "Count Only":
                    self.parent.set_mode(Modes.COUNTING_ONLY)
                    self.parent.set_state(States.RUNNING)
                elif mode == "Count Only Backwards":
                    self.parent.set_mode(Modes.COUNTING_ONLY_BACKWARDS)
                    self.parent.set_state(States.RUNNING)
                try:
                    self.parent.command_handshake(self.parent.handshake_to_vid,
                                                  self.parent.handshake_from_vid,
                                                  (vcm.TEMPLATE, None), vcm.READY)
                except HandshakeException:
                    return
                # self.parent.reset_counter()
                if not self.parent.marking_stop_needed() and not self.parent.testing_stop_needed():
                    print_msg(self, "start positioning.")
                    self.parent.start_positioning()
                # if self.parent.packages_total.get() == -1:
                #     self.parent.counter_frame.pbProgress.start()

        elif mode == "Feed in":
            self.parent.set_mode(Modes.FEED_IN)
            self.parent.set_state(States.RUNNING)
            if not self.parent.marking_stop_needed() and not self.parent.testing_stop_needed():
                print_msg(self, "start positioning.")
                self.parent.start_positioning()

    def onClickStep(self):
        """sends the drive one step command to the mcu after stopping handler for safety reasons"""
        print_msg(self, "")

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.STEP_MSG)
            return

        # self.parent.stop_handler()
        # self.parent.set_state(States.DRIVE)

        # self.parent.portUSB0.write(MCU.IR_ON)  # enable infrared barrier

        # self.auto_windup(MCU.AUTOWINDUP_R)
        # self.parent.portUSB0.write(MCU.CLOSE_BRIDGE)  # set gear bridge to drive position
        # self.parent.portUSB0.write(MCU.TOP_VALVE_UP)    # top valve up
        # self.parent.portUSB0.write(MCU.BOT_VALVE_DOWN)    # bottom valve down
        self.parent.portUSB0.write(MCU.STEP)  # make one step

        # no handshake here, because basically there is no need to inform the video counter after
        # just driving one step and the handshake would take so long the user would notice when
        # clicking step a couple of times fastly.
        self.parent.reset_handler(no_handshake=True)

    def TestMissingFailPackages(self):
        if self.MissingFailTest.get() == TRUE:
            try:
                self.parent.command_handshake(self.parent.handshake_to_vid,
                                              self.parent.handshake_from_vid,
                                              (vcm.TESTMISSINGFAILPACKAGES, True), vcm.READY)
            except HandshakeException:
                return
        else:
            try:
                self.parent.command_handshake(self.parent.handshake_to_vid,
                                              self.parent.handshake_from_vid,
                                              (vcm.TESTMISSINGFAILPACKAGES, False), vcm.READY)
            except HandshakeException:
                return

    def ValveTop(self):
        """ON/OFF Valve Top"""
        # show some help if needed
        self.parent.portUSB0.write(MCU.TOP_VALVE_UP)

    def ValveBottom(self):
        """ON/OFF Valve Bottom"""
        # show some help if needed
        self.parent.portUSB0.write(MCU.BOT_VALVE_DOWN)

    def onClickValve(self, open):
        """sends the command to open or close the probecard holder, according to open.to

        args:
            open (bool): specifies the direction. True -> driving position, False -> testing
            position (contact)
        """
        print_msg(self, "open = {}".format(open))

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.VALVE_MSG)
            return

        self.parent.stop_handler()
        if open:
            if self.ValveTopStop.get() == TRUE:
                self.parent.portUSB0.write(MCU.TOP_VALVE_UP)    # top valve up
            if self.ValveBotStop.get() == TRUE:
                self.parent.portUSB0.write(
                    MCU.BOT_VALVE_DOWN)  # bottom valve down
            pass
        else:
            if self.ValveTopStop.get() == TRUE:
                self.parent.portUSB0.write(MCU.TOP_VALVE_DOWN)
            if self.ValveBotStop.get() == TRUE:
                self.parent.portUSB0.write(MCU.BOT_VALVE_UP)
            pass

    def onClickRetest(self):
        """triggers a test task in pause mode

        after a retest was triggered the pause resumes to running
        """
        print_msg(self, "Retest")

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.RETEST_MSG)
            return

        # set pause flag to go into pause after testing
        self.parent.pause_flag_testing = True
        self.parent.start_testing()

    def onClickRemark(self):
        """triggers a test task in pause mode

        after a retest was triggered the pause resumes to running
        """
        print_msg(self, "Remark")

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.REMARK_MSG)
            return

        # set pause flag to go into pause after testing
        self.parent.pause_flag_marking = True
        self.parent.start_marking()

    def onClickWindup(self):
        """ switches windup on or off
        """
        print_msg(self, "Windup")

        if self.parent.windup_state == WindupStates.AUTO:
            self.stop_windup()
        elif self.parent.windup_state == WindupStates.STOPPED:
            self.auto_windup(MCU.AUTOWINDUP_R)
        elif self.parent.windup_state == WindupStates.DISABLED:
            print_msg(
                self, "windup is disabled -> please enable to turn windup on/off")
        else:
            print_msg(self, "unknown windup_state:" +
                      self.parent.windup_state, color_loc=CMDColor.BG_RED)

    def onClickEnableWindup(self):
        """ enabled or disables autowindup functionality generally. button located in the menu bar
            if the windup is disabled, the starting of the autowindup is not allowed
        """
        print_msg(self, "WindupEnable")
        # print_msg(self, str(self.windup_enabled_var.get()))

        if self.windup_enabled_var.get() == 0:
            self.stop_windup()
            self.bWindup.state(['disabled'])
            self.parent.windup_state = WindupStates.DISABLED
            print_msg(self, "windup disabled -> allow by enabling manually")

        else:
            # set state to stopped before calling stop_windup tp preventstate=DISABLED warning
            self.parent.windup_state = WindupStates.STOPPED
            self.stop_windup()
            self.bWindup.state(['!disabled'])
            print_msg(self, "windup enabled -> set to stopped")

        # print_dbg(self.bWindup.state())

    def auto_windup(self, windup_dir):
        """ handles the messages for windup auto mode and changes the button state accordingly
        """
        if self.parent.windup_state is WindupStates.DISABLED:
            print_msg(self, "windup is disabled -> allow by enabling manually")

        else:
            print_msg(self, "starting autowindup")
            self.bWindup.state(['pressed'])
            self.parent.windup_state = WindupStates.AUTO
            self.parent.portUSB0.write(windup_dir)
            self.parent.portUSB0.write(MCU.IR_ON)

    def stop_windup(self):
        """ handles the messages for windup stop and changes the button state accordingly
        """
        if self.parent.windup_state is WindupStates.DISABLED:
            print_msg(self, "windup is disabled -> allow by enabling manually")

        else:
            self.bWindup.state(['!pressed'])
            print_msg(self, "stopping autowindup")
            self.parent.windup_state = WindupStates.STOPPED

        # stop windup regardless of the mode, as it should be stopped when set to disabled anyways
        self.parent.portUSB0.write(MCU.IR_OFF)

    def get_help_msg_callback(self, event):
        """callback that is used by widgets that have no "command" parameter to trigger help popup.
        popup is only triggered if state=HELP, else just do nothing -> that means the bound widgets are
        always clickable, but the callback just triggers no action.
        a callback bound to a event by bind() always gets the event given as argument. the event triggering
        widget can be accessed via event.widget"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(
                self.help_msg_dict[event.widget])

    def add_widgets(self):
        """"adds the needed buttons

        command=lambda: self.onClickAction(arg) is used so it is possible to call the constructor
        with an argument in the onClick callable.
        """
        self.configure(width=config.CONTROL_WIDTH)

        # add frames for better layout
        self.runFrame = ttk.LabelFrame(
            self, padding=config.PADDING, text="Run")
        self.stopFrame = ttk.LabelFrame(
            self, padding=config.PADDING, text="Stop")
        self.manualControlFrame = ttk.LabelFrame(
            self, padding=config.PADDING, text="Manual Control")
        self.spacerframe = ttk.Frame(self)

        self.runFrame.grid(
            row=1, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.stopFrame.grid(
            row=3, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.spacerframe.grid(row=4, column=1)
        self.grid_rowconfigure(4, weight=1)
        self.manualControlFrame.grid(
            row=5, column=1, pady=config.PADDING_GRID, sticky="ew")

        self.cbChooseMode = ttk.Combobox(self.runFrame, values=[
                                         "Automatic", "Testing Only", "Marking Only", "Count Only", "Count Only Backwards", "Feed in"], width=10)
        self.cbChooseMode.current(0)

        # rewindImage = Image.open('rewind.bmp')
        # rewindImage.thumbnail((config.BUTTON_WIDTH * 4.5, config.BUTTON_WIDTH * 2))
        # self.rewindPhoto = ImageTk.PhotoImage(rewindImage)

        # #custom styles
        # stop button -> red border
        # styleStopButtonBorder = ttk.Style(self.parent)
        # styleStopButtonBorder.config('Stop.')
        styleStopButton = ttk.Style(self.parent)
        # , background='#FF000')
        styleStopButton.configure('Stop.TButton', bordercolor="red2")
        # print_dbg(styleStopButton.layout('Stop.TButton'))
        # styleStopButtonBorder = ttk.Style(self.parent)
        # styleStopButtonBorder.configure('Stop.Button.button')

        self.bWindup = ttk.Button(self.manualControlFrame, width=config.WINDUP_BUTTON_WIDTH, text=u"\u21BA",
                                  command=self.onClickWindup)
        self.bRewind = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH, text=u"\u25C0\u25C0",
                                  command=lambda: self.onClickRewind())
        self.bRewindStep = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH, text=u"\u25C0",
                                      command=lambda: self.onClickRewind(step=True))
        self.bStop = ttk.Button(self.stopFrame, text='Stop', style='Stop.TButton',
                                command=self.onClickStop)
        self.bStop2 = ttk.Button(self.manualControlFrame, text='Stop', style='Stop.TButton',
                                 command=self.onClickStop)
        self.bResume = ttk.Button(self.stopFrame, text="Resume",
                                  command=lambda: self.onClickResume(self.bResume))
        self.bPauseMarking = ttk.Button(self.stopFrame, text="Pause @ Marking",
                                        command=lambda: self.onClickPause("m"))
        self.bPauseTesting = ttk.Button(self.stopFrame, text="Pause @ Testing",
                                        command=lambda: self.onClickPause("t"))
        self.bStep = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH, text=u"\u25B6",
                                command=self.onClickStep)
        self.bRunDrive = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH, text=u"\u25B6\u25B6",
                                    command=self.onClickDrive)
        self.bRunBothPos = ttk.Button(self.runFrame, text='Run', width=8,
                                      command=lambda: self.onClickRun(mode=str(self.cbChooseMode.get())))
        self.bRetest = ttk.Button(self.stopFrame, text=chr(8634) + ' Testing',
                                  command=lambda: self.onClickRetest())
        self.bRemark = ttk.Button(self.stopFrame, text=chr(8634) + ' Marking',
                                  command=lambda: self.onClickRemark())

        self.bValveOpen = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH,
                                     text="open", command=lambda: self.onClickValve(True))   # u"\u25B2"
        self.bValveClose = ttk.Button(self.manualControlFrame, width=config.BUTTON_WIDTH,
                                      text="close", command=lambda: self.onClickValve(False))  # u"\u25BC"
        self.ValveBotStop = IntVar()
        self.ValveTopStop = IntVar()
        self.MissingFailTest = IntVar()
        self.CheckboxValveTop = ttk.Checkbutton(
            self.manualControlFrame, variable=self.ValveTopStop, text="Valve Top ON", command=self.ValveTop)
        self.CheckboxValveBottom = ttk.Checkbutton(
            self.manualControlFrame, variable=self.ValveBotStop, text="Valve Bottom ON", command=self.ValveBottom)
        self.CheckboxMissingFailTest = ttk.Checkbutton(
            self.manualControlFrame, variable=self.MissingFailTest, text="test all packages ON", command=self.TestMissingFailPackages)

        self.lRun = ttk.Label(self.runFrame, text="Mode:")
        self.lDrive = ttk.Label(self.manualControlFrame,
                                text="Drive", anchor="center")
        self.lStep = ttk.Label(self.manualControlFrame,
                               text="Step", anchor="center")
        self.lValveCommands = ttk.Label(
            self.manualControlFrame, text="Valve", anchor="center")

        # Run Frame
        self.lRun.grid(row=1, column=1, sticky="w", padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.cbChooseMode.grid(row=1, column=2, sticky="w",
                               padx=config.PAD_WIDGETS_X)
        self.bRunBothPos.grid(row=1, column=3, sticky="e", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)

        # Stop Frame
        self.bStop.grid(row=1, column=1, sticky="ew",
                        columnspan=2, padx=0, pady=config.PAD_WIDGETS_Y)
        self.bResume.grid(row=2, column=1, sticky="ew",
                          columnspan=2, padx=0, pady=config.PAD_WIDGETS_Y)
        self.bPauseMarking.grid(row=3, column=1, sticky="ew", padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.bRemark.grid(row=4, column=1, sticky="ew", padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.bPauseTesting.grid(row=3, column=2, sticky="ew", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.bRetest.grid(row=4, column=2, sticky="ew", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)

        # Manual Control Frame
        self.bStop2.grid(row=0, column=0, sticky="ew",
                         columnspan=6, padx=0, pady=config.PAD_WIDGETS_Y)
        self.bWindup.grid(row=0, column=6, sticky="e",
                          columnspan=1, padx=0, pady=config.PAD_WIDGETS_Y)
        self.bRunDrive.grid(row=1, column=5, sticky="ew", columnspan=2, padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.bRewind.grid(row=1, column=0, sticky="ew", columnspan=2, padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.lDrive.grid(row=1, column=2, sticky="ew", columnspan=3,
                         padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bStep.grid(row=2, column=5, sticky="ew", columnspan=2, padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.bRewindStep.grid(row=2, column=0, sticky="ew", columnspan=2, padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.lStep.grid(row=2, column=2, sticky="ew", columnspan=3,
                        padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bValveOpen.grid(row=3, column=5, sticky="ew", columnspan=2, padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.bValveClose.grid(row=3, column=0, sticky="ew", columnspan=2, padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.lValveCommands.grid(row=3, column=2, sticky="ew", columnspan=3,
                                 padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.CheckboxValveTop.grid(row=4, column=0)
        self.CheckboxValveBottom.grid(row=4, column=5)
        self.CheckboxMissingFailTest.grid(row=5, column=0, columnspan=10)
        for col_to_config in range(7):
            self.manualControlFrame.columnconfigure(col_to_config, weight=1)

        # self.runFrame.grid_columnconfigure(0, weight=1)
        self.runFrame.grid_columnconfigure(2, weight=1)
        self.stopFrame.grid_columnconfigure(1, weight=1)
        self.stopFrame.grid_columnconfigure(2, weight=1)
        self.manualControlFrame.grid_columnconfigure(1, weight=1)
        self.manualControlFrame.grid_columnconfigure(3, weight=1)

        self.cbChooseMode.bind("<Button-1>", self.get_help_msg_callback)

        # add to the menu bar
        self.parent.menu_control.add("checkbutton",
                                     label="Enable Windup",
                                     variable=self.windup_enabled_var,
                                     onvalue=1, offvalue=0,
                                     command=self.onClickEnableWindup)

    def setup_help_msg_dict(self):
        """set up the help_msg_dict, a dictionary where you can access the corresponding help
        messages via the widget object. the widget object is accessable by event.widget, where
        event is always given to the callback bound by bind"""
        # comboboxes
        self.help_msg_dict[self.cbChooseMode] = help.MODE_SELECTOR_MSG

    def set_state(self, state):
        """disables or enabled the buttons according to the state"""

        # print_msg(self, "Setting state to: " + str(States(state).name))
        enabled = [self.bStop, self.bPauseTesting, self.bPauseMarking]
        # buttons here are not en/disabled by this function
        exclude = [self.bWindup]

        # except from pause, everything is enabled- pause is not needed here.
        if state == States.IDLE:
            # print_msg(self, "State is IDLE")
            enabled = [self.bRunDrive, self.bStep, self.bRewind, self.bRewindStep,
                       self.bValveOpen, self.bValveClose, self.CheckboxValveBottom, self.CheckboxValveTop, self.CheckboxMissingFailTest]
            enabled.extend([self.bRunBothPos, self.cbChooseMode])
            if self.parent.connection_state == "c":  # test start can only happen if handler is connected
                enabled.extend([self.bRunBothPos, self.cbChooseMode])
        # driving test only/ mark only disables other pause options than the own one
        elif state == States.RUNNING or state == States.TESTING:
            if self.parent.mode == Modes.TEST_ONLY:
                self.bPauseTesting.configure(text="Pause@Testing")
                enabled = [self.bStop, self.bPauseTesting]
            elif self.parent.mode == Modes.MARKING_ONLY:
                enabled = [self.bStop, self.bPauseMarking]
            elif self.parent.mode == Modes.COUNTING_ONLY:
                self.bPauseTesting.configure(text="Pause@Counting")
                enabled = [self.bStop, self.bPauseTesting]
            elif self.parent.mode == Modes.COUNTING_ONLY_BACKWARDS:
                self.bPauseTesting.configure(text="Pause@Counting")
                enabled = [self.bStop, self.bPauseTesting]
            elif self.parent.mode == Modes.FEED_IN:
                self.bPauseTesting.configure(text="Pause@Counting")
                enabled = [self.bStop, self.bPauseTesting]
            else:
                self.bPauseTesting.configure(text="Pause@Testing")
                enabled = [self.bStop, self.bPauseMarking, self.bPauseTesting]
        # driving disables the automated functions and the pause
        elif state == States.DRIVE:
            # print_msg(self, "State is DRIVE")
            enabled = [self.bStop, self.bStop2, self.bRunDrive,
                       self.bStep, self.bRewind, self.bRewindStep]
            # if self.parent.mode == Modes.COUNTING_ONLY_BACKWARDS:
            #     self.bPauseTesting.configure(text="Pause@Counting")
            #     enabled = [self.bStop, self.bPauseTesting]
        # while in pause the valves are enabled.
        elif state == States.PAUSE:
            # print_msg(self, "State is PAUSE")
            # if mode is counting only, there is no need for an enabled pause button in pause state
            if self.parent.mode == Modes.COUNTING_ONLY:
                enabled = [self.bStop]
            if self.parent.mode == Modes.COUNTING_ONLY_BACKWARDS:
                enabled = [self.bStop]
            if self.parent.mode == Modes.FEED_IN:
                enabled = [self.bStop]

            enabled.extend([self.bValveClose, self.bValveOpen, self.bResume])
            print_dbg(enabled)
            if self.parent.safed_paused_state == States.MARKING:
                enabled.append(self.bRemark)
            if self.parent.safed_paused_state == States.TESTING:
                enabled.append(self.bRetest)

        elif state == States.SET_DRIVE_TIMEOUT:
            # print_msg(self, "State is SET_DRIVE_TIMEOUT")
            enabled = [self.bStop]

        elif state == States.HELP:  # enable all buttons
            enabled = [self.bRunDrive, self.bStep, self.bRewind, self.bRewindStep,
                       self.bValveOpen, self.bValveClose, self.bRunBothPos, self.bRetest, self.bStop,
                       self.bPauseTesting, self.bRemark, self.bPauseMarking, self.bResume]

        for w in self.winfo_children():
            if w in exclude:
                continue
            if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                w.state(['disabled'])
            if isinstance(w, ttk.LabelFrame):
                for w_sub in w.winfo_children():
                    if w_sub in exclude:
                        continue
                    if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                        w_sub.state(['disabled'])

        for w in enabled:  # iterates through all childs of the parameter frame
            if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                w.state(['!disabled'])
            # children of frames need to be iterated through too
            if isinstance(w, ttk.LabelFrame):
                for w_sub in w.winfo_children():
                    if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                        w_sub.state(['!disabled'])

        # if idle state is reentered, unpress pending pause buttons
        if state == States.IDLE:
            self.bPauseMarking.state(["!pressed"])
            self.bPauseTesting.state(["!pressed"])
