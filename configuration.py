"""config file"""


class ParameterStruct():
    """parameter container for the automatic setup to various modules"""

    def __init__(self,
                 gear_speed, drive_speed, rewind_speed, packages_per_position,
                 template_width, img_path, img_path_missing, img_path_fail, package_name, distance_test):

        self.GEAR_SPEED = gear_speed
        self.DRIVE_SPEED = drive_speed
        self.REWIND_SPEED = rewind_speed
        self.PACKAGES_PER_POSITION = packages_per_position
        self.TEMPLATE_WIDTH = template_width
        # pic of template to be used as choosable button and thumbnail that shows the currently set module
        self.IMG_PATH = img_path
        self.IMG_PATH_MISSING = img_path_missing
        self.IMG_PATH_FAIL = img_path_fail
        self.PACKAGE_NAME = package_name
        self.DISTANCE_TEST = distance_test

    # The __iter__ method allows the instance to be iterable, so the parameters can be easily accessed in a loop.
    def __iter__(self):
        yield self.GEAR_SPEED
        yield self.DRIVE_SPEED
        yield self.REWIND_SPEED
        yield self.PACKAGES_PER_POSITION
        yield self.TEMPLATE_WIDTH
        yield self.IMG_PATH
        yield self.IMG_PATH_MISSING
        yield self.IMG_PATH_FAIL
        yield self.PACKAGE_NAME
        yield self.DISTANCE_TEST


class HandlerConfig():

    CURRENT_VERSION = "HandlerGUI v6_1"

    # serial port configurations
    # if serial connection fails check /dev for connected ttyUSBx
    SERIAL_PORT_MCU = "/dev/ttyUSB0"
    #SERIAL_PORT_MCU = "/dev/serial/by-id/usb-FTDI_TTL232R_FTBXVKMC-if00-port0"
    SERIAL_PORT_MCU = "/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0"
    SERIAL_BAUDRATE_MCU = SERIAL_BAUDRATE_TESTER = 9600

    TCP_HOST = "100.100.1.3"
    TCP_PORT = 12345
    TCP_BUFFER_SIZE = 1024

    TCP_CONNECTING_TIMEOUT = 2  # timeout for connecting attempt
    TCP_SENDRECV_TIMEOUT = 0.1   # timeout for communication socket
    # timeout for resonce, used at the connection still alive check
    TCP_IDLE_TIMEOUT = 20
    # max chars that can be read without end-of-msg delimiter without error
    MAX_RECV_CHAR = 50
    TESTER_TIMEOUT = 100         # timeout for the test procedure
    # max messages that can be received at testing that are not result (if the limit is succeeded, something is most likely wrong)
    MAX_RECV_MSG_NO_RESULT = 10

    # process communication parameters
    HANDSHAKE_REPLY_TIMEOUT = 1
    STARTUP_TIMEOUT = 5
    TCOM_COM_TIMEOUT_POPUP_TIMEOUT = 10 * 1

    # initial values for packages, distance to marking, speeds, ...
    POSITIONS = 1
    POSITIONS_TOTAL = -1
    PACKAGES_PER_POSITION = 2
    DISTANCE_TESTING_MARKING = 8  # positions from test position to marking position
    DISTANCE_TO_TESTING = 3

    # this are the actual speed values sent to the mcu (the displayed values are abstracted)
    GEAR_SPEED = 60
    DRIVE_SPEED = 160
    REWIND_SPEED = 50

    # maximum values
    GEAR_SPEED_LIMIT = 10
    WHEEL_SPEED_LIMIT = 50

    # gui parameters
    BUTTON_WIDTH = 8
    WINDUP_BUTTON_WIDTH = 3

    # videoCounter parameters
    HEIGHT_VIDEO = 240
    WIDTH_VIDEO = 400
    X_MIDDLE = int(WIDTH_VIDEO / 2)
    Y_MIDDLE = 120
    FRAMERATE = 60
    # PADDING_HORIZONTAL = 40  # additional space for matching target area
    PADDING_VERTICAL = 5
    TEMPLATE_WIDTH = 68  # width should be 68 for 6-pad modules
    # TEMPLATE_WIDTH = 100  # width should be 100 for 8-pad modules
    TEMPLATE_HEIGHT = 200

    FITTING_METHOD = 3  # "cv2.TM_CCORR_NORMED"

    # WATCHDOG_TIMEOUT_FACTOR = 1.2
    WATCHDOG_TIMEOUT_FACTOR = 2.0  # TODO: reset to smaller value after fixing the hw bug
    WATCHDOG_DEFAULT_TIMEOUT = 99

    # default values for automatic paramteter config  ->> ONLY DUMMY VALUES NOW, NEEDS TO BE SET UP TO THE REAL VALUES
    MODULE_SETUPS = [ParameterStruct(gear_speed=100,
                                     drive_speed=100,
                                     rewind_speed=100,
                                     packages_per_position=3,
                                     template_width=43,
                                     img_path="/home/pi/Desktop/templates/MOB6_front.png",
                                     img_path_missing="/home/pi/Desktop/templates/MOB6_front.png",
                                     img_path_fail="/home/pi/Desktop/templates/MOB6_front.png",
                                     package_name='MOB6',
                                     distance_test=30),
                     ParameterStruct(gear_speed=100,
                                     drive_speed=100,
                                     rewind_speed=100,
                                     packages_per_position=4,
                                     template_width=50,
                                     img_path="/home/pi/Desktop/templates/MOA8_front.png",
                                     img_path_missing="/home/pi/Desktop/templates/MOA8_front.png",
                                     img_path_fail="/home/pi/Desktop/templates/MOA8_front.png",
                                     package_name='MOA8',
                                     distance_test=20),
                     ParameterStruct(gear_speed=100,
                                     drive_speed=100,
                                     rewind_speed=100,
                                     packages_per_position=2,
                                     template_width=110,
                                     img_path="/home/pi/Desktop/templates/LTO_back.png",
                                     img_path_missing="/home/pi/Desktop/templates/missing/LTO_missing.png",
                                     img_path_fail="/home/pi/Desktop/templates/LTO_back.png",
                                     package_name='LTO',
                                     distance_test=20),
                     ParameterStruct(gear_speed=100,
                                     drive_speed=100,
                                     rewind_speed=100,
                                     packages_per_position=2,
                                     template_width=68,
                                     img_path="/home/pi/Desktop/templates/CLM_CDIF_front.png",
                                     img_path_missing="/home/pi/Desktop/templates/missing/video_now.png",
                                     img_path_fail="/home/pi/Desktop/templates/fails/CLM_CDIF_fail2.png",
                                     package_name='CLM_CDIF',
                                     distance_test=20),
                     ParameterStruct(gear_speed=100,
                                     drive_speed=100,
                                     rewind_speed=100,
                                     packages_per_position=2,
                                     template_width=50,
                                     img_path="/home/pi/Desktop/templates/SIM_front.png",
                                     img_path_missing="/home/pi/Desktop/templates/SIM_front.png",
                                     img_path_fail="/home/pi/Desktop/templates/SIM_front.png",
                                     package_name='SIM',
                                     distance_test=20)
                     ]
    PADDING = 10
    PADDING_SMALL = 5
    PADDING_GRID = 4
    PADDING_RESULT_VISUALISATION = 2
    PAD_WIDGETS_X = 6
    PAD_WIDGETS_Y = 3
    PARAMETER_WIDTH = 350
    CONTROL_WIDTH = 280
    COUNTER_WIDTH = 280
    ENTRY_WIDTH = 5

    # List of template image file paths
    TEMPLATES = ['/home/pi/Desktop/templates/MOB6_front.png',
                 '/home/pi/Desktop/templates/MOA8_front.png',
                 '/home/pi/Desktop/templates/LTO_back.png',
                 '/home/pi/Desktop/templates/CLM_CDIF_front.png',
                 '/home/pi/Desktop/templates/SIM_front.png']

    END_OF_TAPE = '/home/pi/Desktop/templates/end_of_tape.png'
    MISSING = "/home/pi/Desktop/templates/missing/video_now.png"
    FAIL = "/home/pi/Desktop/templates/fails/CLM_CDIF_fail2.png"
