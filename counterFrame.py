"""module file counterFrame. used to slim down the main file

classes:
    ConterFrame: the part of the gui where the counters and information elements are located.
"""
import tkinter as tk
from tkinter import ttk

import time
import threading

from configuration import HandlerConfig as config
from programStates import States, Modes
from cmdPrinter import print_dbg, print_msg
from helper import helpMessages as help
import helper
from helper import mcuMessages as MCU


class CounterFrame(tk.Frame):
    """frame where the counters and information elements are located.

    the access of maun process' members is done directly via self.parent.member. this might be not
    pretty but the frame files are to be seen as extension from the main file with the only task to
    split the main file in smaller files.

    counters:
            position means not packages. its just the "line" in the tape. so depending on the
            package one position can contain 2,3,4,... packages.
        pos: the current position since the last testing stop
        pos_total: the total number of position that went past test pos in this run.
        next_marking_pos: the next pos_total value where marking is needed. (does not specify the
            actual package pos that failed, but what value the pos_total counter has when it is at
            marking pos)
        marked: packages that have been marked yet.
        tested: packages that have been tested yet.
        passed: packages that have passed yet.
        failed: packages that have failed yet.

    buttons:
        history:shows all failed positions from the current/last test run
    """

    def __init__(self, parent, **options):
        """constructor. parent must be main process .self)"""
        ttk.Frame.__init__(self, parent, **options)
        self.parent = parent

        # dict that is used to store widgetspecific help messages, (key=widget, value=message)
        self.help_msg_dict = dict()

        self.add_widgets()
        # fill the help_msg_dict with the widgets and the corresponding messages
        self.setup_help_msg_dict()

        self.msg_lock = threading.Lock()
        # initiate thread for progress bar if test run is set to test indefinitely
        # var = self.parent.positions_total.get()
        # self.progress_bar_thread = threading.Thread(target=self._loop(var))
        # self.progress_bar_thread.start()

    def _loop(self, var):
        print_msg(self, "bin in loop")
        while True:
            with self.msg_lock:
                if var == -1:  # means that no specific number of test runs has been entered
                    self.pbProgress["mode"] = "indeterminate"
                    self.pbProgress.start(50)
            time.sleep(0.3)  # to prevent high cpu usage

    def onClickHistory(self):
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.HISTORY_MSG)
            return
        self.history = self.parent.dialog_handler.targetListInfo()

    def onClickReset(self):
        """resets the counter panel"""

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.RESET_MSG)
            return

        self.parent.reset_counter()

    def onClickSetPosTotal(self):
        """lets the user set the current position
        this is used to rewind and then drive to failed positions"""

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.SETPOSTOTAL_MSG)
            return

        new_pos_total = self.parent.dialog_handler.setPosTotalDialog()
        if new_pos_total is not None:
            # marking targets need to be recalculated to prevent targets at a position before the new current
            # positions in the list
            self.parent.marking_targets.clear()
            for pos, y_list in self.parent.failed_pos_history:
                # all failed positions in history after /at the current pos are used to calculate the new marking targets list
                if (pos + self.parent.distance_testing_marking.get()) >= new_pos_total:
                    self.parent.marking_targets.append(
                        [pos + self.parent.distance_testing_marking.get(), y_list])
            self.parent.next_marking_target.set(self.parent.marking_targets[0][0]
                                                if self.parent.marking_targets
                                                else (-1))
            self.parent.counter.set(0)
            self.parent.counter_packages.set(0)
            self.parent.counter_total.set(new_pos_total)

    def onClickGoTo(self):
        """the user specifies a position the handler should drive to"""

        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.GOTO_MSG)
            return
        # check if gear speed has changed
        if not self.parent.drive_timeout_set:
            if self.parent.dialog_handler.noTimeoutSet_AskToSet() and self.parent.dialog_handler.setDriveTimerPopup():
                self.parent.set_drive_timeout()
                self.parent.drive_timeout_set = True
            return

        target = self.parent.dialog_handler.getDriveToTargetPos()
        print_msg(self, "drive to pos {}".format(target))
        # only targets after the current  positions can be driven to
        # if target and target > self.parent.counter_total.get():
        self.parent.drive_to_target_pos = target
        self.parent.mode = Modes.DRIVE_TO_POS
        self.parent.start_positioning()

    def get_help_msg_callback(self, event):
        """callback that is used by widgets that have no "command" parameter to trigger help popup.
        popup is only triggered if state=HELP, else just do nothing -> that means the bound widgets are
        always clickable, but the callback just triggers no action.
        a callback bound to a event by bind() always gets the event given as argument. the event triggering
        widget can be accessed via event.widget"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(
                self.help_msg_dict[event.widget])

    def add_widgets(self):
        """"adds the needed counters and elements
        """

        self.configure(width=config.COUNTER_WIDTH)

        # add subframes with label
        self.resultsFrame = ttk.LabelFrame(self, padding=(
            config.PADDING, config.PADDING_SMALL), text="Results")
        self.progressFrame = ttk.LabelFrame(self, padding=(
            config.PADDING, config.PADDING_SMALL), text="Progress")
        self.actionsFrame = ttk.LabelFrame(
            self, padding=config.PADDING, text="Actions")
        self.testResultVisualsFrame = ttk.Labelframe(self,
                                                     padding=(
                                                         config.PADDING, config.PADDING_SMALL),
                                                     text="Result Visualisation")
        self.spacerframe = ttk.Frame(self)

        # align subframes and force them the size parameters declared above
        self.resultsFrame.grid(
            row=1, column=1, pady=config.PADDING_GRID, sticky="ew")
        # self.spacerframe.grid(row=2, column=1)
        # self.grid_rowconfigure(2, weight=1)
        self.progressFrame.grid(
            row=3, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.spacerframe.grid(row=4, column=1)
        self.grid_rowconfigure(4, weight=1)
        self.actionsFrame.grid(
            row=5, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.spacerframe.grid(row=6, column=1)
        self.grid_rowconfigure(6, weight=1)
        self.testResultVisualsFrame.grid(
            row=7, column=1, pady=config.PADDING_GRID, sticky="ew")

        # members of results frame
        self.lCounterPos = ttk.Label(self.resultsFrame, text="Position ")
        self.cCounterPos = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                     textvariable=self.parent.counter)
        self.lCounterPosPack = ttk.Label(self.resultsFrame, text="Packages ")
        self.cCounterPosPack = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                         textvariable=self.parent.counter_packages)
        self.lCounterPosTotal = ttk.Label(
            self.resultsFrame, text="Position (total) ")
        self.cCounterPosTotal = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                          textvariable=self.parent.counter_total)
        self.lNextMarkingTargetPos = ttk.Label(
            self.resultsFrame, text="Next marking target (Position) ")
        self.cNextMarkingTargetPos = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                               textvariable=self.parent.next_marking_target)
        self.lCounterTested = ttk.Label(
            self.resultsFrame, text="Packages (tested) ")
        self.cCounterTested = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                        textvariable=self.parent.packages_tested)
        self.padding = ttk.Label(self.resultsFrame, text="")
        self.lCounterPassed = ttk.Label(
            self.resultsFrame, text="Packages (passed) ")
        self.cCounterPassed = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                        textvariable=self.parent.packages_passed)
        self.lCounterFailed = ttk.Label(
            self.resultsFrame, text="Packages (failed) ")
        self.cCounterFailed = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                        textvariable=self.parent.packages_failed)
        self.lCounterMarked = ttk.Label(
            self.resultsFrame, text="Packages (marked)")
        self.cCounterMarked = ttk.Label(self.resultsFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                        textvariable=self.parent.packages_marked)

        self.lCounterPos.grid(row=1, column=1, sticky="e", pady=0)
        self.cCounterPos.grid(row=1, column=2, pady=0)
        self.lCounterPosPack.grid(row=2, column=1, sticky="e", pady=0)
        self.cCounterPosPack.grid(row=2, column=2, pady=0)
        self.lCounterPosTotal.grid(row=3, column=1, sticky="e", pady=0)
        self.cCounterPosTotal.grid(row=3, column=2, pady=0)
        self.lNextMarkingTargetPos.grid(row=4, column=1, sticky="e", pady=0)
        self.cNextMarkingTargetPos.grid(row=4, column=2, pady=0)
        self.lCounterTested.grid(row=6, column=1, sticky="e", pady=(6, 0))
        self.cCounterTested.grid(row=6, column=2, pady=(6, 0))
        self.lCounterPassed.grid(row=7, column=1, sticky="e", pady=0)
        self.cCounterPassed.grid(row=7, column=2, pady=0)
        self.lCounterFailed.grid(row=8, column=1, sticky="e", pady=0)
        self.cCounterFailed.grid(row=8, column=2, pady=0)
        self.lCounterMarked.grid(row=9, column=1, sticky="e", pady=0)
        self.cCounterMarked.grid(row=9, column=2, pady=0)
        self.resultsFrame.grid_columnconfigure(1, weight=1)

        # members of progress frame
        self.lCounterPassRatio = ttk.Label(
            self.progressFrame, text="Pass Ratio ", anchor="e")
        self.cCounterPassRatio = ttk.Label(self.progressFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                           textvariable=self.parent.pass_ratio)
        self.lEstimatedTime = ttk.Label(
            self.progressFrame, text="Estimated Time Left ", anchor="e")
        self.cEstimatedTime = ttk.Label(self.progressFrame, width=config.BUTTON_WIDTH, relief='ridge',
                                        textvariable=self.parent.estimated_time)

        self.lCounterPassRatio.grid(row=1, column=1, sticky="e")
        self.cCounterPassRatio.grid(row=1, column=2, sticky="e")
        self.lEstimatedTime.grid(row=2, column=1, sticky="e")
        self.cEstimatedTime.grid(row=2, column=2, sticky="e")

        self.pbProgress = ttk.Progressbar(self.progressFrame,
                                          orient="horizontal",
                                          length=config.COUNTER_WIDTH - 2 * config.PADDING,
                                          mode="determinate")
        self.pbProgress.grid(row=3, column=1, columnspan=2,
                             pady=(config.PADDING, 0), sticky="we")
        self.pbProgress["value"] = self.parent.packages_tested.get()

        self.progressFrame.grid_columnconfigure(1, weight=1)
        # ,maximum=(self.parent.positions_total if self.parent.positions_total != -1), value=3)

        # members of action frame
        self.bHistory = ttk.Button(
            self.actionsFrame, text="Show History", command=self.onClickHistory)
        self.bReset = ttk.Button(
            self.actionsFrame, text="Reset Counter", command=self.onClickReset)

        self.bHistory.grid(row=1, column=1, sticky="ew", padx=(
            0, config.PAD_WIDGETS_X), pady=config.PAD_WIDGETS_Y)
        self.bReset.grid(row=1, column=3, sticky="ew", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.actionsFrame.grid_columnconfigure(1, weight=1)
        self.actionsFrame.grid_columnconfigure(3, weight=1)

        # test result visuals frame
        self.fTestResultVisuals = ttk.Frame(self.testResultVisualsFrame)
        self.fTestResultVisuals.grid(
            row=1, column=1, sticky="w", pady=config.PAD_WIDGETS_Y)

        # configure a row with a relatively very high weight to add spacing in the frame (weight specifies how much a row gets)
        # self.grid_rowconfigure(0, weight=10)
        # self.grid_rowconfigure(4, weight=10)

        # binding of defaultwise non-clickable widgets is done here, bc this function is only called once and because of some
        # weird tkinter behavior, multiple calls to bind() (bind the same callback repeatedly to the same widget) lead to a
        # memory leak
        # labels:
        self.lCounterPos.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterPosPack.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterPosTotal.bind("<Button-1>", self.get_help_msg_callback)
        self.lNextMarkingTargetPos.bind(
            "<Button-1>", self.get_help_msg_callback)
        self.lCounterTested.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterPassed.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterFailed.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterMarked.bind("<Button-1>", self.get_help_msg_callback)
        self.lCounterPassRatio.bind("<Button-1>", self.get_help_msg_callback)
        self.lEstimatedTime.bind("<Button-1>", self.get_help_msg_callback)
        # entries:
        self.cCounterPos.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterPosPack.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterPosTotal.bind("<Button-1>", self.get_help_msg_callback)
        self.cNextMarkingTargetPos.bind(
            "<Button-1>", self.get_help_msg_callback)
        self.cCounterTested.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterPassed.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterFailed.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterMarked.bind("<Button-1>", self.get_help_msg_callback)
        self.cCounterPassRatio.bind("<Button-1>", self.get_help_msg_callback)
        self.cEstimatedTime.bind("<Button-1>", self.get_help_msg_callback)
        # progressbars
        self.pbProgress.bind("<Button-1>", self.get_help_msg_callback)
        # frames:
        self.testResultVisualsFrame.bind(
            "<Button-1>", self.get_help_msg_callback)

        # populate menubar

        self.parent.menu_control.add("command",
                                     label="GoTo Position",
                                     command=self.onClickGoTo)
        self.parent.menu_setup.add("command",
                                   label="Set Position",
                                   command=self.onClickSetPosTotal)

    def setup_help_msg_dict(self):
        """set up the help_msg_dict, a dictionary where you can access the corresponding help
        messages via the widget object. the widget object is accessable by event.widget, where
        event is always given to the callback bound by bind"""
        # labels:
        self.help_msg_dict[self.lCounterPos] = help.LABEL_COUNTERPOS_MSG
        self.help_msg_dict[self.lCounterPosPack] = help.LABEL_COUNTERPOSPACK_MSG
        self.help_msg_dict[self.lCounterPosTotal] = help.LABEL_COUNTERPOSTOTAL_MSG
        self.help_msg_dict[self.lNextMarkingTargetPos] = help.LABEL_NEXTMARKING_MSG
        self.help_msg_dict[self.lCounterTested] = help.LABEL_COUNTERTESTED_MSG
        self.help_msg_dict[self.lCounterPassed] = help.LABEL_COUNTERPASSED_MSG
        self.help_msg_dict[self.lCounterFailed] = help.LABEL_COUNTERFAILED_MSG
        self.help_msg_dict[self.lCounterMarked] = help.LABEL_COUNTERMARKED_MSG
        self.help_msg_dict[self.lCounterPassRatio] = help.LABEL_COUNTERPASSTIME_MSG
        self.help_msg_dict[self.lEstimatedTime] = help.LABEL_ESTTIME_MSG
        # entries:
        self.help_msg_dict[self.cCounterPos] = help.ENTRY_COUNTERPOS_MSG
        self.help_msg_dict[self.cCounterPosPack] = help.ENTRY_COUNTERPOSPACK_MSG
        self.help_msg_dict[self.cCounterPosTotal] = help.ENTRY_COUNTERPOSTOTAL_MSG
        self.help_msg_dict[self.cNextMarkingTargetPos] = help.ENTRY_NEXTMARKING_MSG
        self.help_msg_dict[self.cCounterTested] = help.ENTRY_COUNTERTESTED_MSG
        self.help_msg_dict[self.cCounterPassed] = help.ENTRY_COUNTERPASSED_MSG
        self.help_msg_dict[self.cCounterFailed] = help.ENTRY_COUNTERFAILED_MSG
        self.help_msg_dict[self.cCounterMarked] = help.ENTRY_COUNTERMARKED_MSG
        self.help_msg_dict[self.cCounterPassRatio] = help.ENTRY_COUNTERPASSTIME_MSG
        self.help_msg_dict[self.cEstimatedTime] = help.ENTRY_ESTTIME_MSG
        # progressbars:
        self.help_msg_dict[self.pbProgress] = help.PROGRESSBAR_RUNPROGRESS_MSG
        # frames:
        self.help_msg_dict[self.testResultVisualsFrame] = help.ENTRY_TESTVISUALS_FRAME_MSG

    def update_test_result_visuals(self, failed_pos_dict):
        # Function to update the test result visuals based on the provided 'failed_pos_dict'
        # 'failed_pos_dict' is a dictionary containing positions as keys and indexes as values
        print_dbg("updating visuals", failed_pos_dict)

        # test result frame gets constructed as a  new frame object at every change
        self.fTestResultVisuals.destroy()
        self.fTestResultVisuals = ttk.Frame(self.testResultVisualsFrame)

        if failed_pos_dict is not None:  # if None is given as dict, the Frame stays empty
            # pos specifies the position in tape-direction
            # If 'failed_pos_dict' is not None, update the visuals with the test results
            # Calculate the width of each individual visual representation for one package based on the total
            # number of packages and packages per position.
            one_package_visual_width = min(((config.COUNTER_WIDTH - 2 * config.PADDING) /
                                            (self.parent.packages.get() // self.parent.packages_per_position.get())),
                                           20)
            print_dbg(one_package_visual_width)  # for debugging
            # Configure styles for the visuals representing pass and fail results.
            style_pass = ttk.Style()
            style_pass.configure(
                "VisualsPass.TLabel", background="SpringGreen3", font=("TkDefaultFont", 8))
            style_fail = ttk.Style()
            style_fail.configure(
                "VisualsFail.TLabel", background="firebrick1", font=("TkDefaultFont", 8))
            # Configure styles for the visuals representing pass and fail results
            for pos in range(self.parent.packages.get() // self.parent.packages_per_position.get()):
                # Create a new frame 'pFrame' for each position
                pFrame = ttk.Frame(self.fTestResultVisuals)
                # y_idx is the position normal to the tape direction, it specifies the site at one position
                for y_idx in range(self.parent.packages_per_position.get()):
                    if pos in failed_pos_dict and y_idx in failed_pos_dict[pos]:
                        ttk.Label(pFrame, style="VisualsFail.TLabel", text="fail")\
                           .pack(fill=tk.X, side=tk.BOTTOM)
                    else:
                        ttk.Label(pFrame, style="VisualsPass.TLabel", text="pass")\
                           .pack(fill=tk.X, side=tk.BOTTOM)
                pFrame.pack(side=tk.RIGHT)

        self.fTestResultVisuals.grid(row=1, column=1, sticky="e")

    def set_state(self, state, oldstate):
        """disables or enabled the buttons according to the state"""

        if state is States.IDLE or state is States.HELP:
            enabled = [self.bHistory, self.bReset]
        else:
            enabled = [self.bHistory]

        if self.parent.positions_total.get() == -1:
            self.pbProgress["mode"] = "indeterminate"
            if state != oldstate:
                self.pbProgress.start()
            elif not (state == States.RUNNING or state == States.TESTING or States.DRIVE):
                self.pbProgress.stop()
        else:
            self.pbProgress["mode"] = "determinate"
            self.pbProgress["value"] = self.parent.packages_tested.get()

        # for widget in self.winfo_children():
        #     if isinstance(widget, ttk.Button):
        #         widget.config(state="disabled", cursor=self.parent.cursor)
        # for widget in enabled:
        #     widget.config(state="normal", cursor=self.parent.cursor)
        for w in self.winfo_children():
            if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                w.state(['disabled'])
            if isinstance(w, ttk.LabelFrame):
                for w_sub in w.winfo_children():
                    if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                        w_sub.state(['disabled'])

        for w in enabled:  # iterates through all childs of the parameter frame
            if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                w.state(['!disabled'])
            # children of frames need to be iterated through too
            if isinstance(w, ttk.LabelFrame):
                for w_sub in w.winfo_children():
                    if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Combobox)):
                        w_sub.state(['!disabled'])
