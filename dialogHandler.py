"""provides different popup- und dialog windows.

class:
    TimeoutMessageBox: A Popup with alterable choices and a timeout for lifetime
    ScrollBarBox: A popup containig a scrollable list
    DialogHandler: container class for different popup callables
    ModuleSelector:  class for window to choose package to test
    ResizeDialog: A dialog window for resizing templates
    TestInfoPopup: A popup window to display test information
    ThemedSimpleDialog: A simple dialog asking for input
"""
#from tkinter import *
from tkinter import HORIZONTAL, ttk
from tkinter import messagebox as mb
#from tkinter import simpledialog as sd
# from ttkthemes import themed_tk as tk # TODO: update GUI with themed tk

import tkinter as tk
import sys
from PIL import ImageTk, Image

from tkinter import font
import skimage
from skimage.metrics import structural_similarity as ssim

import numpy as np
import cv2
import imutils.video.pivideostream
import cv2

from configuration import HandlerConfig as config
from cmdPrinter import print_msg, print_dbg, CMDColor
from programStates import States, Modes
from videoCounter import VideoCounterMessages as vcm
from errorHandler import HandshakeException


class TimeoutMessagebox(tk.Toplevel):
    """a dialogwindow with a defined timeout after wich it closes and returns None

    args:
        title (string): the name of the window
        msg (str): message to be shown as info
        parent (str): parent tkinter object
        choices (list<string>): choices to be but on the buttons.
            the window returns the chosen string as choice at closing
        timeout (int): timeout, after which the window should be closed
            (< 0) deactivates the timeout

    returns:
        choice (string): if user chooses a button
        None:   if timeout causes closing

    """

    def __init__(self, title, msg, parent, choices=("OK", "Cancel"), timeout=(-1)):
        """constructor

        the choicebuttons are packed in a extra frame, to provide all choice to be
        displayed side by side in order of the argument list. this frame is packed last
        so the buttons are displayed on bottom of the window.
        """
        tk.Toplevel.__init__(self, parent)
        self.title(title)
        self.timeout = timeout
        self.retval = None

        self.msg = ttk.Label(self, text=msg)
        self.msg.pack(side="top")
        self.button_box = ttk.Frame(self)
        for choice in choices:
            _choice_button = ttk.Button(self.button_box, text=choice)
            _choice_button.bind("<Button-1>", self.onChoose)
            _choice_button.pack(side="left")
        self.button_box.pack()

    def onChoose(self, event):
        """if a button is clicked, return value is set and the destroy event triggered"""

        # returns the widgets member "text" from the triggering widget
        self.retval = event.widget["text"]
        self.destroy()

    def show(self):
        """callable that shows the constructed window

        the window is activated and displayed. if a timeout is set, the destroying after the
        timeout is scheduled using .after(), what calls the destroy event after the timeout.
        """
        self.wmp_deiconify()
        self.msg.focus_force()
        if self.timeout >= 0:
            self.after(self.timeout, self.destroy)
        self.wait_window()
        return self.retval


class ScrollBarBox(tk.Toplevel):
    def __init__(self, title, msg, parent, entry_list):
        # Initialize the ScrollBarBox class with the given parameters
        tk.Toplevel.__init__(self, parent)
        self.parent = parent
        self.title(title)

        # Create a scrollbar for the listbox
        scrollbar = ttk.Scrollbar(self)
        scrollbar.grid(row=2, column=3, sticky=tk.W + tk.N + tk.S)

        # Create a listbox with the provided 'entry_list' and link it to the scrollbar
        self.box = tk.Listbox(self, listvariable=entry_list)
        self.box.grid(row=2, column=1, columnspan=2)
        self.box.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.box.yview)

        # Create buttons
        self.goto_button = ttk.Button(
            self, text="Drive To", command=self.onGoTo)
        self.goto_button.grid(row=3, column=1)
        closebutton = ttk.Button(self, text="Close", command=self.onClose)
        closebutton.grid(row=3, column=2)

        # Set the 'onClose' function to handle window closing events
        self.protocol("WM_DELETE_WINDOW", self.onClose)

    def onClose(self):
        # Hide the window when 'Close' button or window close button is clicked
        self.withdraw()

    def onGoTo(self):
        # Get the selected target position from the listbox
        target = self.box.get(tk.ACTIVE)
        # Check if the target position is valid, and set the parent's mode to 'DRIVE_TO_POS'
        if target[0] > self.parent.counter_total.get():
            self.parent.drive_to_target_pos = target[0]
            self.parent.mode = Modes.DRIVE_TO_POS
            self.parent.start_positioning()

    def setButtonState(self, enabled):
        # Enable or disable the 'Drive To' button
        self.goto_button.config(state="normal" if enabled else "disabled")

    def show(self):
        # Make the window visible
        self.wm_deiconify()


class TestInfoPopup(tk.Toplevel):
    """provides a information popup to inform the user about the reached marking point
    includes a graphical representation of which packages on the current pos are faulty
    """

    def __init__(self, title, msg, parent, failed_idx_list):
        """constructor"""
        tk.Toplevel.__init__(self, parent)
        self.parent = parent
        self.title(title)
        self.geometry("+400+400")

        self.m = ttk.Label(
            self, text=msg, wraplength=tk.font.Font().measure("0") * 25)
        self.m.grid(row=1, sticky="ew")

        style_pass = ttk.Style()
        style_pass.configure("VisualsPass.TLabel", background="SpringGreen3")
        style_fail = ttk.Style()
        style_fail.configure("VisualsFail.TLabel", background="firebrick1")
        self.f = ttk.Frame(self)
        print_dbg("failed_idx_list", failed_idx_list)
        # y_idx is the position normal to the tape direction, it specifies the site at one position
        for y_idx in range(self.parent.packages_per_position.get()):
            if y_idx in failed_idx_list:
                ttk.Label(self.f, text="Fail", style="VisualsFail.TLabel")\
                   .pack(fill=tk.X, side=tk.BOTTOM)
            else:
                ttk.Label(self.f, text="Pass", style="VisualsPass.TLabel")\
                   .pack(fill=tk.X, side=tk.BOTTOM)
        self.f.grid(row=2)

        ttk.Button(self, text="OK", command=self.destroy).grid(row=3)

    def show(self):
        self.wm_deiconify()
        self.focus_force()
        self.wait_window()


class ModuleSelector(tk.Toplevel):

    def __init__(self, parent, module_image_dict):
        # Initialize the ModuleSelector class
        tk.Toplevel.__init__(self, parent)
        self.title("Module Selector")
        self.geometry("+400+400")

        self.label = ttk.Label(self, text="Choose a module to set parameters")
        self.label.pack()

        # Create frames to organize the module buttons
        self.module_frame = ttk.Frame(self)
        self.module_subframe = ttk.Frame(self)

        # Initialize the 'module_image_dict' attribute
        self.module_image_dict = module_image_dict
        self.ret_val = None

        # If 'module_image_dict' is None, create an empty dictionary
        if self.module_image_dict is None:
            self.module_image_dict = dict()

        # Load images for each module type from 'config.MODULE_SETUPS' and store them in 'module_image_dict'
        for module_type in config.MODULE_SETUPS:
            self.module_image_dict[module_type] = (ImageTk.PhotoImage(Image.open(module_type.IMG_PATH)),
                                                   Image.open(module_type.IMG_PATH))

        # Create buttons for each module with their respective images and set up the 'setup' function as the command
        for params, (img, thumbnail) in self.module_image_dict.items():
            print_dbg(params)
            print_dbg(img)
            print_dbg(thumbnail)
            panel = ttk.Button(self.module_subframe,
                               image=img,
                               command=lambda e=params: self.setup(e))
            panel.pack(side=tk.LEFT)

            # Check if five module buttons have been added to the subframe
            # If so, pack the subframe into the main frame and create a new subframe
            if len(self.module_subframe.winfo_children()) == 5:
                self.module_subframe.pack()
                self.module_subframe = ttk.Frame(self.module_frame)

        # Pack the last subframe into the main frame
        self.module_subframe.pack()
        self.module_frame.pack()

        # Create the button
        self.bCancel = ttk.Button(self, text="Cancel", command=self.onCancel)
        self.bCancel.pack()

    def onCancel(self):
        # Destroy the window when the 'Cancel' button is clicked
        self.destroy()

    def setup(self, param_struct):
        # Set 'ret_val' to the selected module's parameters and thumbnail image, then destroy the window
        self.ret_val = (
            param_struct, (self.module_image_dict[param_struct])[1])
        self.destroy()

    def show(self):
        # Make the window visible, focus on it, wait for it to close, and return 'ret_val'
        self.wm_deiconify()
        self.focus_force()
        self.wait_window()
        return self.ret_val


class ResizeDialog(tk.Toplevel):
    def __init__(self, parent):
        """Constructor for ResizeDialog class"""
        tk.Toplevel.__init__(self, parent)
        self.title("Resize Template")
        self.geometry("+400+400")
        self.parent = parent

        # Create and pack a label with instructions
        label = ttk.Label(
            self, text="Resize template to fit the package. \nPress \"Default\" to reset to the current package's default size.")
        label.pack()

        # Create buttons for template resizing
        templateSizer = ttk.Frame(self)
        bTemplateR = ttk.Button(templateSizer, text='-',
                                command=lambda: self.onClickTemplate('-'))
        bTemplateS = ttk.Button(
            templateSizer, text='Reset', command=lambda: self.onClickTemplate('.'))
        bTemplateB = ttk.Button(templateSizer, text='+',
                                command=lambda: self.onClickTemplate('+'))
        bTemplate1 = ttk.Button(templateSizer, width=config.BUTTON_WIDTH,
                                text='Template', command=self.onClickTemplate)

        # Pack the buttons to the left side in the frame
        bTemplateR.pack(side=tk.LEFT)
        bTemplateS.pack(side=tk.LEFT)
        bTemplateB.pack(side=tk.LEFT)
        bTemplate1.pack(side=tk.LEFT)

        # Pack the templateSizer frame
        templateSizer.pack()

    def onClickTemplate(self, resize=None):
        """Handle button clicks for template resizing."""
        try:
            self.parent.command_handshake(self.parent.handshake_to_vid, self.parent.handshake_from_vid,
                                          (vcm.TEMPLATE, resize), vcm.READY)
        except HandshakeException:
            pass

    def onCancel(self):
        """Destroy the dialog when canceled"""
        self.destroy()

    def show(self):
        """Show the ResizeDialog and wait for it to close"""
        self.wm_deiconify()
        self.focus_force()
        self.wait_window()


class ThemedSimpleDialog(tk.Toplevel):
    """A simple dialog asking for input. Replaces the unthemed tk.simpledialog"""

    def __init__(self, title="", message="", validity_check=lambda x: True, error_msg="", parent=None):
        """Constructor for ThemedSimpleDialog class"""
        tk.Toplevel.__init__(self, parent)
        self.title(title)
        self.geometry("+400+400")
        self.parent = parent
        self.validity_check = validity_check
        self.error_msg = error_msg
        self.ret_val = None

        # Create a label
        self.m = ttk.Label(self, text=message,
                           wraplength=tk.font.Font().measure("0") * 25)
        self.m.grid(row=1, sticky="ew")
        self.m.pack()

        # Create a frame to hold the input entry and the 'OK' button.
        self.f = ttk.Frame(self)
        self.e = ttk.Entry(self.f)
        self.b = ttk.Button(self.f, text="OK", command=self.onClickOK)

        # Pack the entry and button to the left and right side of the frame, respectively
        self.e.pack(side=tk.LEFT)
        self.b.pack(side=tk.RIGHT)
        self.f.pack()

    def onClickOK(self):
        """Handle 'OK' button click"""
        validated = self.validity_check(self, self.e.get())
        if validated:
            self.ret_val = validated
            self.destroy()

    def onError(self):
        """Display an error message when input validation fails"""
        mb.showerror(title="Invalid Value", message=self.error_msg)

    def show(self):
        """Show the dialog and wait for it to close, then return the result"""
        self.wm_deiconify()
        self.focus_force()
        self.wait_window()
        return self.ret_val

    @classmethod
    def askinteger(cls, title="", message="", minvalue=None, maxvalue=None, errormessage=None, parent=None):
        """Create a ThemedSimpleDialog and validate the input as an integer"""

        def validate(self, value_string):
            try:
                value_int = int(value_string)
                if (minvalue is not None and value_int < minvalue) or (maxvalue is not None and value_int > maxvalue):
                    raise ValueError()
                return value_int
            except ValueError:
                self.onError()
                return False

        if errormessage is None:
            errormessage = "Invalid value. Must be an integer." + ("\n Minimum value: " + str(
                minvalue)) if minvalue is not None else "" + ("\n Maximum value: " + str(maxvalue)) if maxvalue is not None else ""

        return cls(title, message, validate, errormessage, parent).show()


class DialogHandler:
    """container class for different popup callables

    popups should be called using the dialoghandler instance to ensure same look for all
    popups.
    provides different popup dialogs.
    """

    def __init__(self, parent):
        self.parent = parent

        self.target_list_popup = None
        self.module_image_dict = None

    def _timeout_resume_cancel(self, title, msg, parent, timeout=(-1), block=True):
        """abstraction of calling a timeout popup that blocks main

        args:
            title (string): the name of the window
            msg (str): message to be shown as info
            parent (str): parent tkinter object
            timeout (int): timeout, after which the window should be closed
                (< 0) deactivates the timeout
            block (bool): specifies if the popup block the main window
        """
        popup = TimeoutMessagebox(
            title, msg, parent, ("Resume", "Cancel"), timeout)
        if block:
            popup.grab_set()
        ret_val = popup.show()
        return ret_val

    def _timeout_retry_cancel(self, title, msg, parent, timeout=(-1), block=True):
        """abstraction of calling a timeout popup that blocks main"""
        popup = TimeoutMessagebox(
            title, msg, parent, ("Retry", "Cancel"), timeout)
        if block:
            popup.grab_set()
        ret_val = popup.show()
        return ret_val

    def _timeout_retry_pause_cancel(self, title, msg, parent, timeout=(-1), block=True):
        """abstraction of calling a timeout popup that blocks main"""
        popup = TimeoutMessagebox(
            title, msg, parent, ("Retry", "Pause", "Cancel"), timeout)
        if block:
            popup.grab_set()
        ret_val = popup.show()
        return ret_val

    def resumeableErrorPopup(self, calling_instance, msg, where="", title="Error", parent=None,
                             timeout=None, block=True):
        """calls a popup with choices (Resume, Cancel)

        returns the chosen string ore none if timeout was set and reached

        args:
            calling_instance (object): the instance calling the popup. should be called with self
            title (string): the name of the window
            msg (str): message to be shown as info
            where (string): optional param to specify calling point in the program further.
            parent (str): parent tkinter object. if None is set, the dialoghandlers parent is used
            timeout (int): timeout, after which the window should be closed
                (< 0) deactivates the timeout
            block (bool): specifies if the popup block the main window

        returns:
            (string) Resume, Cancel, depending on the choice or None if Timeout is reached.
        """
        calling_function = sys._getframe(1).f_code.co_name
        print_msg(calling_instance, msg, calling_func=calling_function, where=where,
                  color_loc=CMDColor.BG_RED)

        error_msg = where + "\nAn error ocurred:\n" + msg\
                          + "\n\nYou can resume to the failed action and ignore the error"\
                          + "or cancel the running job\n"
        if timeout:
            return self._timeout_resume_cancel(title, error_msg,
                                               parent=(
                                                   parent if parent else self.parent),
                                               timeout=timeout, block=block)
        else:
            return self._timeout_resume_cancel(title, error_msg,
                                               parent=(
                                                   parent if parent else self.parent),
                                               block=block)

    def retryableErrorPopup(self, calling_instance, msg, where="", title="Error", parent=None,
                            timeout=None, block=True):
        """like resumeableErrorPopup, but with choices (Retry, Cancel)"""
        calling_function = sys._getframe(1).f_code.co_name
        print_msg(calling_instance, msg, calling_func=calling_function, where=where,
                  color_loc=CMDColor.BG_RED)

        error_msg = where + "\nAn error ocurred:\n" + msg\
                          + "\n\nYou can retry the failed action action or cancel the running job\n"
        if timeout:
            return self._timeout_retry_cancel(title, error_msg,
                                              parent=(
                                                  parent if parent else self.parent),
                                              timeout=timeout, block=block)
        else:
            return self._timeout_retry_cancel(title, error_msg,
                                              parent=(
                                                  parent if parent else self.parent),
                                              block=block)

    def retryPauseCancelErrorPopup(self, calling_instance, msg, where="", title="Error",
                                   parent=None, timeout=None, block=True):
        """like resumeableErrorPopup, but with choices (Retry, Pause, Cancel)"""
        calling_function = sys._getframe(1).f_code.co_name
        print_msg(calling_instance, msg, calling_func=calling_function, where=where,
                  color_loc=CMDColor.BG_RED)

        error_msg = where + "\nAn error ocurred:\n" + msg\
                          + "\n\nYou can retry the failed action, cancel it and go to pause state"\
                          + "or cancel the running job\n"
        if timeout:
            return self._timeout_retry_pause_cancel(title, error_msg,
                                                    parent=(
                                                        parent if parent else self.parent),
                                                    timeout=timeout, block=block)
        else:
            return self._timeout_retry_pause_cancel(title, error_msg,
                                                    parent=(
                                                        parent if parent else self.parent),
                                                    block=block)

    def fatalErrorPopup(self, calling_instance, msg, where="", title="Fatal Error", parent=None):
        """calls a popup aking to return to idle (Yes, No)

        args:
            calling_instance (object): the instance calling the popup. should be called with self
            title (string): the name of the window
            msg (str): message to be shown as info
            where (string): optional param to specify calling point in the program further.
            parent (str): parent tkinter object. if None is set, the dialoghandlers parent is used

        returns:
            True for Yes, False for No
        """
        calling_function = sys._getframe(1).f_code.co_name
        print_msg(calling_instance, msg, calling_func=calling_function, where=where,
                  color_loc=CMDColor.BG_RED)

        error_msg = where + "\nAn fatal error ocurred:\n" + msg\
                          + "\n\nDo you want to return to idle? (click NO to end the program)\n"
        return mb.askyesno(title, error_msg, parent=(parent if parent else self.parent))

    def errorPopup(self, calling_instance, msg, where="", title="Error", parent=None):
        """calls a popup informing th user about an error
        """
        calling_function = sys._getframe(1).f_code.co_name
        print_msg(calling_instance, msg, calling_func=calling_function, where=where,
                  color_loc=CMDColor.BG_RED)

        error_msg = where + "\nAn error ocurred:\n" + msg\
                          + "\n\nReturning to idle"
        return mb.showinfo(title, error_msg, parent=(parent if parent else self.parent))

    def markingPopup(self, failed_idx_list):
        """calls a information popup"""
        print_dbg(failed_idx_list)
        return TestInfoPopup("Marking",
                             "Marking point reached, please mark the packages at the marking position",
                             self.parent, failed_idx_list).show()

    def setDriveTimerPopup(self):
        """calls a pinformation popup"""
        return mb.askokcancel("Watchdog Timer",
                              "The Handler will drive one position to determine watchdog timer",
                              parent=self.parent)

    def driveTimeoutInfo(self, value):
        """calls a pinformation popup"""
        mb.showinfo("Drive Timeout", "Timeout set to " + str(value))
        return

    def targetListInfo(self):
        """Display the target list popup if it is not already shown

        The target list popup shows the failed positions from the current/last run
        If the popup is already shown, it brings the existing popup to the front
        """
        if self.target_list_popup is None:
            # If the target list popup doesn't exist, create a new ScrollBarBox popup with the target list
            self.target_list_popup = ScrollBarBox("Targets",
                                                  "These are the failed positions from the current/last run",
                                                  self.parent, self.parent.failed_pos_listvar)
        # Show the target list popup (or bring the existing one to the front)
        self.target_list_popup.show()

    def targetListInfo_setButtonState(self, enable):
        """enables or disables the drive to pos button)"""
        if self.target_list_popup is not None:
            self.target_list_popup.setButtonState(enable)

    def noTimeoutSet_AskToSet(self):
        """calls a popup where the user needs to confirm the start of a test run

        needed if the timeout was not set yet"""
        # return mb.askokcancel("No Timeout Set", "Timeout was not set yet. The possible missing of "
        #                       "a position might not be noticed.", parent=self.parent)

        # instead of only informing the user that no timeout is set, a new option should be available that
        # lets him/her directly set drive timeout: running auto mode without any timeout set should be eliminated
        # for liability reasons.
        return mb.askyesno("Watchdog Timer", "Timer was not set yet or must be changed due to changed gear\
         speed.\nDo you want to set it now?", parent=self.parent)

    def showGearSpeedChangedError(self):
        """calls a popup where the user needs to confirm the start of a test run

        needed if the gear speed changed after timeout was already set"""
        # if self.parent.need_new_drive_timeout:
        mb.showinfo("New Timeout Needed", "Gear Speed changed, please reset Timeout. Otherwise a possible missing of "
                    "a position might not be noticed or might raise an error.", parent=self.parent)

    def getDriveToTargetPos(self):
        """Get the target position to be driven to

        Returns:
            int: The target position to be driven to (input by the user)
        """
        return ThemedSimpleDialog.askinteger("Drive to", "Enter position to be driven to.\nPosition must be after"
                                             " current position.",
                                             minvalue=0)

    def setPosTotalDialog(self):
        """Show a dialog to set the current position on the tape

        Returns:
            int: The value to be set as the current position on the tape (input by the user)
        """
        return ThemedSimpleDialog.askinteger("Change current position", "Enter value to be set as current position"
                                             " on tape.\nThe value corresponds to the position that is\n"
                                             "currently on the test site 1\n Attention:\nChanging the current pos"
                                             " to a SMALLER value can\nlead to the displaying of wrong counter"
                                             " values if a\ntest run is started without having reset the counter.",
                                             minvalue=0, parent=self.parent)

    def gettingStartedPopup(self):
        """Display a popup with getting started instructions."""
        getting_started_msg = ("1. " + "Set the number of positions or packages that are tested at "
                               "once per test run" + "\n" +
                               "2. " + "Set the total number of packages/positions to be tested "
                               "(can be set to zero to set no total number)" + " \n" +
                               "3. " + "Set the number of packages per position, i.e. how many devices "
                               "are there parallel to each other (...)" + "\n" +
                               "4. " + "Press 'set drive timeout' to evaluate the elapsed time "
                               "between two positions" + "\n" +
                               "5. " + "Press 'Run Auto' for automatic test, 'Test Only' for test run "
                               "without marking, or 'Marking Only' for test run without testing."
                               )
        mb.showinfo("Getting Started For Auto Run", getting_started_msg)

    def showHelp(self, msg):
        """Show a help popup with the provided message

        Parameters:
            msg (str): The help message to be displayed in the popup.
        """
        mb.showinfo("Help", msg)
        self.parent.set_state(States.IDLE)  # reset state
        self.parent.cursor = 'left_ptr'     # reset cursor

    def showModuleTypeSelector(self):
        """Show the module type selector popup.

        Returns:
            tuple: A tuple containing the selected module's parameters and thumbnail image.
        """
        module_selector = ModuleSelector(self.parent, self.module_image_dict)
        return module_selector.show()

    def showResizeDialog(self):
        """Show the resize dialog for template resizing

        Returns:
            None: The result of the resize dialog is handled internally.
        """
        resize_dialog = ResizeDialog(self.parent)
        return resize_dialog.show()

    def showAbout(self):
        """Show the 'About' popup"""
        mb.showinfo("About",
                    "Operation software for Reel2Reel-Handler (engineering)\n"
                    "Version information:\n"
                    + config.CURRENT_VERSION)

    def EndofTapePopup(self):
        """calls a pinformation popup"""
        mb.showinfo(
            "End of tape", "The Handler stopped because the end of the tape is reached - no more packages")
        return

    def MissingPackagePopup(self):
        """calls a pinformation popup"""
        mb.showinfo("Missing Package", "A package was missing")
        return

    def FailPopup(self):
        """calls a pinformation popup"""
        mb.showinfo("Fail", "The Handler stopped because fail")
        return

    def FirstPackagePopup(self):
        """calls a pinformation popup"""
        mb.showinfo(
            "First Package", "The Handler stopped because the beginning of the tape/first package was detected")
        return

    def onClickCLMorCDIF(self):
        checkpackage = tk.Toplevel()
        checkpackage.title("Check CLM or CDIF")
        # Add a message to the dialog
        message = tk.Label(
            checkpackage, text="Is this the package CLM or CDIF?")
        message.pack()

        # Function to handle "Yes" button click
        def on_CDIF():
            checkpackage.destroy()
            self.indicator = True
        # Function to handle "No" button click

        def on_CLM():
            checkpackage.destroy()
            self.indicator = False
        # Add "Yes" and "No" buttons
        CDIF_button = tk.Button(checkpackage, text="CDIF", command=on_CDIF)
        CDIF_button.pack(side="left")
        CLM_button = tk.Button(checkpackage, text="CLM", command=on_CLM)
        CLM_button.pack(side="right")
        checkpackage.wait_window()
        if self.indicator == True:
            return True
        else:
            return False

    def DoublecheckPackage(self, package_name, img_path):
        """ calls a window to check if the right package is detected 
            + displays image of package"""
        checkpackage = tk.Toplevel()
        checkpackage.geometry("300x300")
        checkpackage.title("Check Package")
        self.indicator = True

        # Add a message to the dialog
        message = tk.Label(
            checkpackage, text="Is this the right package?" + "\n" + str(package_name))
        message.pack()

        #image_path.thumbnail((60, 100))
        img = ImageTk.PhotoImage(Image.open(img_path))
        img_label = ttk.Label(checkpackage, image=img)

        img_label.pack()

        def on_yes():
            checkpackage.destroy()
            self.indicator = True
        # Function to handle no button click

        def on_no():
            checkpackage.destroy()
            self.indicator = False
        # Add yes and no buttons
        yes_button = tk.Button(checkpackage, text="Yes", command=on_yes)
        yes_button.pack(side="left")
        no_button = tk.Button(checkpackage, text="No", command=on_no)
        no_button.pack(side="right")
        checkpackage.wait_window()
        if self.indicator == True:
            return True
        else:
            return False
