"""defines the HandlerException"""


class HandlerException(Exception):
    """Basic handler Exception

    the deadly flag is used to determine if the process should be killed after handling.
    """

    def __init__(self, deadly=False):
        self.deadly = deadly


class HandshakeException(Exception):
    pass
