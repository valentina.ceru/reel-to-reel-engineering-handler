"""classes:
        VideoCounter: Subprocess for counting packages using piCam and template matching (opencv)
        VideoCounterMessages: defines message IDs for interprocesscommunication
"""

import multiprocessing as mpc
import time
import matplotlib.pyplot as plt

import imutils.video.pivideostream
import cv2
import RPi.GPIO as GPIO
import os
from numpy import asarray
import numpy as np

from configuration import HandlerConfig as config
from cmdPrinter import print_msg, print_dbg, CMDColor
import helper
from helper import try_put
import time
import queue
import tkinter as tk
import pickle


class VideoCounter (mpc.Process):
    """subprocess for package counting.

    this process uses picam via pivideostream to define a template that refers to the package
    position. using template matching the template is tracked in the stream. if the start position
    is reached the subprocess sends a COUNT to the main process. the main process can specify the
    number of positions to count before sending the STOP message.messages are defined in
    VideoCounterMessages. the instance is controlled via the com_in(out)_handshake queues. it checks
    for messages using _check_messages().
    """

    def __init__(self, comIN_handshake, comOUT_handshake, comOUT):
        super(VideoCounter, self).__init__()
        self.com_in_handshake = comIN_handshake
        self.com_out_handshake = comOUT_handshake
        self.com_out = comOUT

        """video processing parameters"""
        self.x_middle = config.X_MIDDLE
        self.y_middle = config.Y_MIDDLE
        self.height_video = config.HEIGHT_VIDEO
        self.width_video = config.WIDTH_VIDEO

        # template width default is used to store the default template size for a setup module type
        self.template_width_default = config.TEMPLATE_WIDTH
        self.template_width = config.TEMPLATE_WIDTH
        self.template_height = config.TEMPLATE_HEIGHT
        # additional padding for fitting target area
        self.padding_horizontal = self.template_width // 2
        self.padding_vertical = config.PADDING_VERTICAL
        # start point where new templates are taken and the counter gets triggered if the tracked
        # module passes
        self.top_left_start = (int(self.x_middle - self.template_width // 2),
                               int(self.y_middle - self.template_height // 2))

        # self.top_right_start = (self.top_left_start[0] + self.template_width,
        #                         self.top_left_start[1])

        # point at which the new tracking position is set to if a tracked module has reached the
        # release point
        self.jump_back = (int(self.x_middle - self.template_width),
                          int(self.y_middle - self.template_height // 2))

        self.jump_forward = (int(self.x_middle - (self.template_width // 2)),
                             int(self.y_middle - self.template_height // 2))

        # set the tracking position to start position for default
        self.fitted_top_left = self.top_left_start
        # self.fitted_top_right = self.top_right_start
        self.template = None
        self.fitting_method = config.FITTING_METHOD

        # 0rot -  1dunkelrot - 2gelb
        # 3grün - 4dunkelgrün - 5türkis
        # 6blau - 7dunkelblau - 8magenta
        self.color_defines = ((0, 0, 255), (0, 0, 190), (0, 255, 255),
                              (0, 255, 0), (0, 190, 0), (255, 255, 0),
                              (255, 0, 0), (190, 0, 0), (255, 0, 255))

        # control parameters
        self.counter = 0
        self.counter_packages = 0
        self.target_count = -1
        self.jump_back_flag = False
        self.jump_forward_flag = False
        self.running = False
        self.startup = False
        self.startup_pos = None
        self.feed_in = False

        # watchdog parameters
        self.watchdog = False
        self.watchdog_timeout_set = False
        self.watchdog_timeout_factor = config.WATCHDOG_TIMEOUT_FACTOR
        self.watchdog_start = 0
        self.watchdog_timeout = config.WATCHDOG_DEFAULT_TIMEOUT

        # end of tape
        self.end_of_tape_path = config.END_OF_TAPE
        self.end_of_tape = cv2.imread(config.END_OF_TAPE)
        self.missing_package_path = config.MISSING
        self.missing_package = cv2.imread(self.missing_package_path)
        self.fail_package_path = config.FAIL
        self.fail_package = cv2.imread(self.fail_package_path)
        self.name_current_package = tk.StringVar()
        self.name_current_package.set('Default')
        self.image_path_package = tk.StringVar()
        self.image_path_package.set("Default")

        self.counter_until_missing = 0
        self.counter_until_fail = 0
        self.indicator_missing = False
        self.indicator_fail = False
        self.position_fail = []
        # distance from camera to testing in positions
        self.distance_to_testing = config.DISTANCE_TO_TESTING
        # queue with positions of packages that are fail/missing
        #self.position_test = list()
        # with open('list.pickle','rb') as file:
        #     self.position_test = pickle.load(file)

        self.position_test = []
        self.testmissingfailpackage = False
        self.check_end_of_tape = False
        #self.lock = threading.Lock()

    def run(self):
        """starts the VideoCounting process.

        initializes the camera thread and the videostream and takes a start template. initializes
        the fitting target area. after initialization ist sends READY to the main process. then it
        starts the processing loop in which the frames are processed, the modules counted and the
        message check is done. when the counting is activated by main, a watchdog is set, that
        checks every loop if the time between two counts did take too long. this prevents missing a
        position unnoticed.
        """

        print_msg(self, "starting up video sampler thread")
        self.pvs = imutils.video.pivideostream.PiVideoStream((self.width_video, self.height_video),
                                                             config.FRAMERATE)
        self.video_stream = self.pvs.start()
        time.sleep(1)
        self._new_template()

        self.target_area_top = max(
            0, self.top_left_start[1] - self.padding_vertical)
        self.target_area_bottom = min(self.height_video, self.top_left_start[1]
                                      + self.template_height
                                      + self.padding_vertical)
        self.target_area_left = max(
            0, self.fitted_top_left[0] - self.padding_horizontal)
        self.target_area_right = min(self.width_video, self.fitted_top_left[0]
                                     + self.template_width
                                     + self.padding_horizontal)

        # send READY to main process to inform it about beeing ready to count modules
        try_put(self, self.com_out, (VideoCounterMessages.READY,))

        """processing loop

        checks if new messages from the main process have arrived.
        takes a new frame from the stream and performs the template matching for module tracking.
        if count or stop criteria is reached, send an concurring message to main.
        check watchdog (if activated) every loop.
        """
        while True:
            time.sleep(0.01)  # to prevent high cpu usage (0.01 ~ -10%)
            self._check_messages()

            frame = self.video_stream.read()

            # calculate borders from target area (is used so the fitting space is smaller
            # -> faster fitting)
            self.target_area_left = max(
                0, self.fitted_top_left[0] - self.padding_horizontal)
            self.target_area_right = min(self.width_video, self.fitted_top_left[0]
                                         + self.template_width
                                         + self.padding_horizontal)

            # draw middle line and target area into frame
            cv2.line(frame, (self.x_middle, 0), (self.x_middle, self.height_video),
                     self.color_defines[0], 1)
            cv2.rectangle(frame, (self.target_area_left, self.target_area_top),
                          (self.target_area_right,
                           self.target_area_bottom), self.color_defines[6],
                          2)

            # target area is cut out from frame and transformed into grayscale image
            # (increases performance of matching)
            target_area = cv2.cvtColor(frame[self.target_area_top:self.target_area_bottom,
                                             self.target_area_left:self.target_area_right],
                                       cv2.COLOR_BGR2GRAY)
            matching_result = cv2.matchTemplate(
                target_area, self.template, self.fitting_method)
            _, _, _, max_loc = cv2.minMaxLoc(matching_result)
            max_left, max_top = max_loc
            # position of fitted template is relative to target area
            self.fitted_top_left = (max_left + self.target_area_left,
                                    max_top + self.target_area_top)
            # self.fitted_top_right = (self.fitted_top_left[0] + self.template_width,
            #                         max_top + self.target_area_top)

            # draw fitted template and target line for left upper corner
            cv2.rectangle(frame, self.fitted_top_left, (self.fitted_top_left[0]
                                                        + self.template_width,
                                                        self.fitted_top_left[1]
                                                        + self.template_height),
                          self.color_defines[3], 2)
            cv2.line(frame, (self.top_left_start[0], 0),
                     (self.top_left_start[0], self.height_video), self.color_defines[0], 2)
            # show the frame with the drawn geometry
            cv2.imshow('Frame', frame)
            cv2.waitKey(1)

            """Feed in 
            
            handler drives to the first package and sets parameters
            """
            if self.feed_in:
                # Define the area that should be compared with the end-of-tape-image
                current_target_area = frame[self.target_area_top:self.target_area_bottom,
                                            self.target_area_left:self.target_area_right]
                end_of_tape_resized = cv2.resize(
                    self.end_of_tape, (current_target_area.shape[1], current_target_area.shape[0]))
                numpycurrent_target_area = asarray(current_target_area)
                numpyend_of_tape = asarray(end_of_tape_resized)
                # Calculate the difference between the two pictures
                difference = cv2.absdiff(
                    numpycurrent_target_area, numpyend_of_tape)
                gray = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)

                # Threshold the difference to get a binary image
                thresh = cv2.threshold(gray, 25, 255, cv2.THRESH_BINARY)[1]

                # Count the number of non-zero pixels in the binary image
                count_feedin = cv2.countNonZero(thresh)

                # If the number of non-zero pixels is above a certain threshold, the images are different
                if count_feedin > 18000:
                    template_area_to_compare = frame[self.target_area_top:
                                                     self.target_area_bottom, 0:self.template_width]

                    cv2.imwrite(
                        '/home/pi/Desktop/templates/template_area_to_compare.png', template_area_to_compare)
                    print_msg(self, "detected 1 package - stopping handler ")
                    time.sleep(1)
                    GPIO.output(40, GPIO.LOW)
                    try_put(self, self.com_out,
                            (VideoCounterMessages.FIRST_PACKAGE,))
                    self._idle()
                    self.feed_in = False

            """STARTUP

            when activating the counting, the startup starts the watchdog after the tape was moved
            (2) pixels. this ensures that the watchdog only start to watch when the tape start to
            move to prevent wrongful timeout errors.
            """

            if self.startup and self.fitted_top_left[0] > self.startup_pos + 2:
                self.startup = False
                self._start_watchdog()

            """JUMP

            if the tracked template has fully passed the middle line, the matching target area gets
            set back to the left side of the middle line (jump point) -> next template gets matched
            and tracked.
            """

            if self.fitted_top_left[0] >= self.x_middle:
                print_msg(self, "reached middle line - jumping back")
                self.fitted_top_left = self.jump_back
                # jump_back indicates that the matched template is newly matched after a jump and
                # was not counted yet
                self.jump_back_flag = True

                # draw fitted template and target line for left upper corner
                cv2.rectangle(frame, self.fitted_top_left, (self.fitted_top_left[0]
                                                            + self.template_width,
                                                            self.fitted_top_left[1]
                                                            + self.template_height),
                              self.color_defines[3], 2)
                cv2.line(frame, (self.top_left_start[0], 0),
                         (self.top_left_start[0], self.height_video), self.color_defines[0], 2)
                # show the frame with the drawn geometry
                cv2.imshow('Frame', frame)
                cv2.waitKey(1)

            elif (self.fitted_top_left[0] + self.template_width + (self.template_width // 2)) <= self.x_middle:
                print_msg(self, "reached middle line - jumping forward")
                self.fitted_top_left = self.jump_forward
                # jump_back indicates that the matched template is newly matched after a jump and
                # was not counted yet
                self.jump_forward_flag = True

                # draw fitted template and target line for left upper corner
                cv2.rectangle(frame, self.fitted_top_left, (self.fitted_top_left[0]
                                                            + self.template_width,
                                                            self.fitted_top_left[1]
                                                            + self.template_height),
                              self.color_defines[3], 2)
                cv2.line(frame, (self.top_left_start[0], 0),
                         (self.top_left_start[0], self.height_video), self.color_defines[0], 2)
                # show the frame with the drawn geometry
                cv2.imshow('Frame', frame)
                cv2.waitKey(1)

            """COUNT

            if tracked template ocurrence is newly matched (jump_back=True) and passes the start
            line, one whole module passed -> count
            """
            if self.jump_back_flag and self.fitted_top_left[0] >= self.top_left_start[0]:
                self.counter += 1
                self.counter_packages = (
                    config.PACKAGES_PER_POSITION * self.counter)
                self.jump_back_flag = False

                print_msg(self, "just counted - new counter value: " + str(self.counter)
                                + " (target: " + str(self.target_count) + ")")
                if self.running:
                    try_put(self, self.com_out, (VideoCounterMessages.COUNT,))
                    self._reset_watchdog()

                    """STOP

                    if the number of counted modules reached the target count (given by main), the
                    stop is triggered using the interrupt line gpio.40. then the main process is
                    informed about the stop.
                    """
                    if self.counter is self.target_count and (self.target_count != -1 and self.target_count != -2 and self.target_count != -3 and self.target_count != -4):
                        print_msg(
                            self, "reached target count - stopping handler")
                        GPIO.output(40, GPIO.LOW)
                        try_put(self, self.com_out,
                                (VideoCounterMessages.STOP,))
                        self._idle()
                with open('list.pickle', 'rb') as file:
                    self.position_test = pickle.load(file)
                if self.testmissingfailpackage == True:
                    for i in range(self.packages_per_position):
                        self.position_test.append("1")

                """End Of Tape
                
                if no more packages are detected, the
                stop is triggered using the interrupt line gpio.40. then the main process is
                informed about the stop.
                """
                # Define the area that should be compared with the end-of-tape-image
                current_target_area = frame[self.target_area_top:self.target_area_top+self.target_area_bottom -
                                            self.target_area_top, self.target_area_left:self.target_area_left+self.target_area_right - self.target_area_left]
                end_of_tape_resized = cv2.resize(
                    self.end_of_tape, (current_target_area.shape[1], current_target_area.shape[0]))
                numpy_target_area = asarray(current_target_area)
                numpy_end_of_tape = asarray(end_of_tape_resized)
                difference = cv2.absdiff(numpy_target_area, numpy_end_of_tape)
                grayscaled_image = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
                thresholded_image = cv2.threshold(
                    grayscaled_image, 25, 255, cv2.THRESH_BINARY)[1]
                count = cv2.countNonZero(thresholded_image)

                if count < 11000:
                    print_msg(
                        self, "reached end of tape - stopping handler " + str(count))
                    GPIO.output(40, GPIO.LOW)
                    try_put(self, self.com_out,
                            (VideoCounterMessages.END_OF_TAPE,))
                    self._idle()

            """ Countback
            
            if tracked template ocurrence is newly matched (jump_forward=True) and passes the start
            line, one whole module passed -> counter decrease
            """
            # and self.fitted_top_left[0] <= (self.top_left_start[0] + self.template_width) :
            elif self.jump_forward_flag:
                self.counter -= 1
                self.counter_packages = (
                    config.PACKAGES_PER_POSITION * self.counter)
                self.jump_forward_flag = False

                print_msg(self, "just counted - new counter value: " + str(self.counter)
                                + " (target: " + str(self.target_count) + ")")
                if self.running:
                    try_put(self, self.com_out,
                            (VideoCounterMessages.COUNTBACK,))
                    self._reset_watchdog()

                """STOP

                    if the number of counted modules reached the target count (given by main), the
                    stop is triggered using the interrupt line gpio.40. then the main process is
                    informed about the stop.
                    """
                if self.counter is self.target_count and (self.target_count != -1 and self.target_count != -2 and self.target_count != -3 and self.target_count != -4):
                    print_msg(
                        self, "reached target count - stopping handler")
                    GPIO.output(40, GPIO.LOW)
                    try_put(self, self.com_out,
                            (VideoCounterMessages.STOP,))
                    self._idle()
            if (self.check_end_of_tape == True) and (not self.feed_in):
                # Define the area that should be compared with the end-of-tape-image
                current_target_area = frame[self.target_area_top:self.target_area_bottom,
                                            self.target_area_left:self.target_area_right]
                end_of_tape_resized = cv2.resize(
                    self.end_of_tape, (current_target_area.shape[1], current_target_area.shape[0]))
                numpycurrent_target_area = asarray(current_target_area)
                numpyend_of_tape = asarray(end_of_tape_resized)
                # Calculate the difference between the two pictures
                difference = cv2.absdiff(
                    numpycurrent_target_area, numpyend_of_tape)
                gray = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)

                # Threshold the difference to get a binary image
                thresh = cv2.threshold(gray, 25, 255, cv2.THRESH_BINARY)[1]

                # Count the number of non-zero pixels in the binary image
                count_feedin = cv2.countNonZero(thresh)
                #print_msg(self, "count"+ str(count_feedin))

                # If the number of non-zero pixels is above a certain threshold, the images are different
                if count_feedin < 12500:
                    print_msg(self, "end of tape - stopping handler ")
                    self.check_end_of_tape = False
                    # time.sleep(1)
                    GPIO.output(40, GPIO.LOW)
                    try_put(self, self.com_out,
                            (VideoCounterMessages.END_OF_TAPE,))
                    self._idle()

            #print_msg(self, self.position_test)
            self.indicator_fail = False
            self._watchdog()

    def _start_watchdog(self):
        """sets watchdog flag so the timout gets handled.sets start time to system time so timeout
        can be calculated."""
        print_msg(self, "")
        self.watchdog = True
        self.watchdog_start = time.time()

    def _watchdog(self):
        """checks (if watchdog is calculated) if the timeout is reached.

        if that't the case, a package could have been missed by the counting algorithm, and the
        counter would be incorrect. the watchdog checks if the time between two counts is smaller
        than a defined timout value.
        """
        if (self.watchdog and time.time() > self.watchdog_start + self.watchdog_timeout
                and not self.watchdog_timeout_set):
            GPIO.output(40, GPIO.LOW)
            print_msg(self, "Counting timeout",
                      color_loc=CMDColor.BG_RED, color_msg=CMDColor.RED)
            self._stop_watchdog()
            try_put(self, self.com_out, (VideoCounterMessages.WATCHDOG,))

    def _reset_watchdog(self):
        """resets the watchdog time to zero.

        this is called at every count. if set_watchdog_timeout is active, the handler tries to
        determine the time between two counts to define the watchdog timeout value. if thats the
        case, the time since the start is set as timeout and the watchdog is stopped.
        """
        if self.watchdog_timeout_set:
            print_msg(self, "set watchdog timeout")
            counting_time = time.time() - self.watchdog_start
            self.watchdog_timeout = counting_time * self.watchdog_timeout_factor
            self.watchdog_timeout_set = False
            self.target_count = -1
            self._stop_watchdog()
            try_put(self, self.com_out,
                    (VideoCounterMessages.TIMEOUT_VALUE, counting_time))
        else:
            print_msg(self, "reset watchdog timer")
            self.watchdog_start = time.time()

    def _stop_watchdog(self):
        self.watchdog = False

    def _idle(self):
        """resets the counting algorithm, so there is no stop triggered. if target count is -1 it
        cannot be reached by the counter
        """

        self.running = False
        self.startup = False
        self.counter = 0
        self.counter_packages = 0
        self.target_count = -1
        self._stop_watchdog()

    def _check_messages(self):
        """checks if command messages have arrived from main. messages need to be answered with
        READY or ERROR."""

        while not self.com_in_handshake.empty():
            msg = self.com_in_handshake.get_nowait()
            print_msg(self, "got message: \"" + str(msg) + "\"")

            if msg[0] == VideoCounterMessages.START:
                """START (START, target_count)

                counter reset and targtet count is set to given value. also the state is set to
                running and the startup process is initialized.
                target count should be positive normally, -1 means the counting is disabled or idle.
                -2 is used for the only counting mode, where the end of the run is triggered by the
                end of the tape and the resulting watchdog trigger.
                """

                print_msg(self, "got START")
                if msg[1] and (msg[1] >= -4):
                    self.counter = 0
                    self.counter_packages = 0
                    self.target_count = msg[1]
                    self.running = True
                    self.startup = True
                    self.startup_pos = self.fitted_top_left[0]
                    if msg[1] == -3:
                        self.check_end_of_tape = True
                    if msg[1] == -4:
                        self.feed_in = True
                        self.jump_back_flag = False  # flags need to be reset to prevent wrong behavior
                    try_put(self, self.com_out_handshake,
                            (VideoCounterMessages.READY,))
                else:
                    err_msg = "Got invalid target count: " + str(msg[1])
                    print_msg(self, err_msg, color_loc=CMDColor.BG_RED,
                              color_msg=CMDColor.RED)
                    self._idle()
                    try_put(self, self.com_out_handshake,
                            (VideoCounterMessages.ERROR, err_msg))

            elif msg[0] == VideoCounterMessages.STOP_TIMEOUT:
                """STOP_TIMEOUT deactivates the processing of watchdog timeouts to prevent
                error messages"""

                print_msg(self, "got STOP_TIMEOUT")
                self._stop_watchdog()
                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            elif msg[0] == VideoCounterMessages.TEMPLATE:
                """TEMPLATE (TEMPLATE, resize)

                use the current position for a new template. because this defines the start
                position this is also the counting point. if resize is + or -, change the
                template size before takeing a new template. with "."" the size  is reset to
                default.
                if a value is given, the user has set a ned module type with has another
                template size. this leads to a change in template_size_default, so the user
                can set to the ned default size by pressing ".".
                resizing the template prevents wrong matches. if the template size is bigger
                than the area between the middle line and the border of the frame, the jump
                condition can never be met and the counting would not work. so if a template
                that big is needed the framesize must be altered in the configuration file.

                the padding is also depending from the template size now, to prevent a roi
                where more then a half template is fitting in the pad area (prevents wrong
                matches with small templates)
                """

                print_msg(self, "got TEMPLATE")
                # resize template if wanted
                if msg[1] == "+":
                    if self.template_width + 1 < (self.width_video // 2) - 5:
                        self.template_width += 1
                        self.padding_horizontal = self.template_width // 2
                        print_msg(self, "resizing tempate to " +
                                  str(self.template_width))
                elif msg[1] == "-":
                    if self.template_width > 1:
                        self.template_width -= 1
                        self.padding_horizontal = self.template_width // 2
                        print_msg(self, "resizing tempate to " +
                                  str(self.template_width))
                elif msg[1] == ".":
                    self.template_width = self.template_width_default
                    self.padding_horizontal = self.template_width // 2
                    print_msg(self, "resizing tempate to " +
                              str(self.template_width))
                elif msg[1] and msg[1] < (self.width_video / 2) - 5:
                    self.template_width = self.template_width_default = int(
                        msg[1])
                    self.padding_horizontal = self.template_width // 2
                self.jump_back_flag = False

                # recalculate fixed geometry because of changed template size
                self.top_left_start = (int(self.x_middle - self.template_width // 2),
                                       int(self.y_middle - self.template_height // 2))
                self.jump_back = (int(self.x_middle - self.template_width),
                                  int(self.y_middle - self.template_height // 2))

                self._new_template()
                self.fitted_top_left = self.top_left_start
                self.target_area_top = max(
                    0, self.top_left_start[1] - self.padding_vertical)
                self.target_area_bottom = min(self.height_video, self.top_left_start[1]
                                              + self.template_height
                                              + self.padding_vertical)
                self.target_area_left = max(0, self.fitted_top_left[0]
                                            - self.padding_horizontal)
                self.target_area_right = min(self.width_video, self.fitted_top_left[0]
                                             + self.template_width
                                             + self.padding_horizontal)

                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            elif msg[0] == VideoCounterMessages.MATCHTEMPLATE:
                """TEMPLATE (TEMPLATE, resize)

                use the current position for a new template. because this defines the start
                position this is also the counting point. if resize is + or -, change the
                template size before takeing a new template. with "."" the size  is reset to
                default.
                if a value is given, the user has set a ned module type with has another
                template size. this leads to a change in template_size_default, so the user
                can set to the ned default size by pressing ".".
                resizing the template prevents wrong matches. if the template size is bigger
                than the area between the middle line and the border of the frame, the jump
                condition can never be met and the counting would not work. so if a template
                that big is needed the framesize must be altered in the configuration file.

                the padding is also depending from the template size now, to prevent a roi
                where more then a half template is fitting in the pad area (prevents wrong
                matches with small templates)
                """

                print_msg(self, "got TEMPLATE")
                # resize template if wanted
                # if msg[1] == ".":
                #     self.template_width = self.template_width_default
                #     self.padding_horizontal = self.template_width // 2
                #     print_msg(self, "resizing tempate to " +
                #               str(self.template_width))
                # elifcom_outc msg[1] and msg[1] < (self.width_video / 2) - 5:
                #     self.template_width = self.template_width_default = int(
                #         msg[1])
                #     self.padding_horizontal = self.template_width // 2
                self.jump_back_flag = False

                # recalculate fixed geometry because of changed template size
                self.top_left_start = (int(self.x_middle - self.template_width // 2),
                                       int(self.y_middle - self.template_height // 2))
                self.jump_back = (int(self.x_middle - self.template_width),
                                  int(self.y_middle - self.template_height // 2))

                self._match_template()
                self.fitted_top_left = self.top_left_start
                self.target_area_top = max(
                    0, self.top_left_start[1] - self.padding_vertical)
                self.target_area_bottom = min(self.height_video, self.top_left_start[1]
                                              + self.template_height
                                              + self.padding_vertical)
                self.target_area_left = max(0, self.fitted_top_left[0]
                                            - self.padding_horizontal)
                self.target_area_right = min(self.width_video, self.fitted_top_left[0]
                                             + self.template_width
                                             + self.padding_horizontal)

                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            elif msg[0] == VideoCounterMessages.SET_TIMEOUT:
                """SET_TIMEOUT

                the timeout value determination process. this is needed to know the time needed
                for driving the tape from one count to the next.
                """
                print_msg(self, "got SET_TIMEOUT")
                self._new_template()
                self.counter = 0
                self.counter_packages = 0
                self.target_count = 1
                self.running = True
                self.startup = True
                self.startup_pos = self.top_left_start[0]
                self.jump_back_flag = False  # flags need to be reset to prevent wrong behaviour
                self.watchdog_timeout_set = True
                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            elif msg[0] == VideoCounterMessages.PARAMETER:
                """MISSING/FAIL IMG PATH
                    Sets path for missing and fail package.
                """
                print_msg(self, "got PATH")
                self.missing_package_path = str(msg[1])
                self.fail_package_path = str(msg[2])
                self.name_current_package = str(msg[3])
                self.packages_per_position = msg[4]
                self.image_path_package = str(msg[5])
                self.distance_to_testing = msg[6]
                self.position_test = []

                for i in range(self.distance_to_testing*self.packages_per_position):
                    self.position_test.append("1")
                    #print_msg(self, self.position_test)
                with open('list.pickle', 'wb') as file:
                    pickle.dump(self.position_test, file)

                if msg[7] == True:
                    print_msg(self, "check PACKAGE")
                    try_put(self, self.com_out, (VideoCounterMessages.CHECK_PACKAGE,
                            self.name_current_package, self.image_path_package))

                try_put(self, self.com_out,
                        (VideoCounterMessages.SETVALVE, self.name_current_package,))

                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            elif msg[0] == VideoCounterMessages.TESTMISSINGFAILPACKAGES:
                if msg[1] == True:
                    self.testmissingfailpackage = True
                elif msg[1] == False:
                    self.testmissingfailpackage = False
                try_put(self, self.com_out_handshake,
                        (VideoCounterMessages.READY,))

            else:
                print_msg(self, "Got unknown message id from main",
                          color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
                self._idle()

    def _new_template(self):
        """takes new template at the current position module tracking.

        the template is taken at the start position in the frame. what part of the tape is used for
        the template is actually not important, because the matching algorithm simply matches the
        template part of the tape to the tape in the frame.
        """
        print_msg(self, "current position is used for template")
        frame = self.video_stream.read()
        if frame is None:
            print_dbg("frame is None")
        self.template = cv2.cvtColor(frame[self.top_left_start[1]:(self.top_left_start[1]
                                                                   + self.template_height),
                                           self.top_left_start[0]:(self.top_left_start[0]
                                                                   + self.template_width)],
                                     cv2.COLOR_BGR2GRAY)
        cv2.imshow("Template", self.template)
        cv2.imwrite('/home/pi/Desktop/templates/video_now.png', self.template)
        cv2.waitKey(1)

    def _match_template(self):
        """takes new template at the current position module tracking.

        the template is taken at the start position in the frame. what part of the tape is used for
        the template is actually not important, because the matching algorithm simply matches the
        templatre part of the tape to the tape in the frame.
        """
        print_msg(self, "current position is used for template")
        frame = self.video_stream.read()
        self.target_area_left = max(
            0, self.fitted_top_left[0] - self.padding_horizontal)
        self.target_area_right = min(self.width_video, self.fitted_top_left[0]
                                     + self.template_width
                                     + self.padding_horizontal)

        # draw middle line and target area into frame
        cv2.line(frame, (self.x_middle, 0), (self.x_middle, self.height_video),
                 self.color_defines[0], 1)
        cv2.rectangle(frame, (self.target_area_left, self.target_area_top),
                      (self.target_area_right,
                       self.target_area_bottom), self.color_defines[6],
                      2)
        current_target_area = cv2.cvtColor(frame[self.target_area_top:self.target_area_bottom,
                                                 self.target_area_left:self.target_area_right],
                                           cv2.COLOR_BGR2GRAY)
        self.image_package = cv2.imread(self.image_path_package)
        gray = cv2.cvtColor(self.image_package, cv2.COLOR_BGR2GRAY)
        matching_result = cv2.matchTemplate(
            current_target_area, gray, self.fitting_method)
        _, _, _, max_loc = cv2.minMaxLoc(matching_result)
        max_left, max_top = max_loc
        # position of fitted template is relative to target area
        self.fitted_top_left = (max_left + self.target_area_left,
                                max_top + self.target_area_top)

        # draw fitted template and target line for left upper corner
        cv2.rectangle(frame, self.fitted_top_left, (self.fitted_top_left[0]
                                                    + self.template_width,
                                                    self.fitted_top_left[1]
                                                    + self.template_height),
                      self.color_defines[3], 2)
        cv2.line(frame, (self.top_left_start[0], 0),
                 (self.top_left_start[0], self.height_video), self.color_defines[0], 2)
        # show the frame with the drawn geometry
        cv2.imshow('Frame', frame)
        if frame is None:
            print_dbg("frame is None")
        self.template = cv2.cvtColor(frame[self.top_left_start[1]:(self.top_left_start[1]
                                                                   + self.template_height),
                                           self.top_left_start[0]:(self.top_left_start[0]
                                                                   + self.template_width)],
                                     cv2.COLOR_BGR2GRAY)
        cv2.imshow("Template", self.template)
        cv2.imwrite('/home/pi/Desktop/templates/video_now.png', self.template)
        cv2.waitKey(1)


class VideoCounterMessages:
    """message id definitions for interprocesscommnication between main and videocounter"""
    pass
    # to main
    READY = -1
    COUNT = -2
    STOP = -3
    ERROR = -4
    WATCHDOG = -5
    TIMEOUT_VALUE = -6
    END_OF_TAPE = -7
    MISSING = -8
    FAIL = -9
    FIRST_PACKAGE = -10
    DONT_TEST = -11
    CHECK_PACKAGE = -12
    CLMCDIF = -13
    SETVALVE = -14
    COUNTBACK = -15

    # from main
    START = 1
    STOP_TIMEOUT = 2
    TEMPLATE = 3
    SET_TIMEOUT = 4
    #FEED_IN = 5
    PARAMETER = 6
    CHECK_IF_RIGHT_PACKAGE = 7
    MATCHTEMPLATE = 8
    TESTMISSINGFAILPACKAGES = 9
