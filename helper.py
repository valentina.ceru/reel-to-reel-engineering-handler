"""module with some handy functions and the extended IntVar

functions:
    clear_queue:  empties a mpc.queue
    try_put:   tries to put a msg into a queue and handes the queue.Full exception
"""
import sys
import queue
from tkinter import *
from tkinter import ttk
# from ttkthemes import themed_tk as tk

from cmdPrinter import print_msg, print_dbg, CMDColor


def clear_queue(q):
    """ensures a queue to be empty

    arg:
        q (queue). queue to be cleared
    """
    while not q.empty():
        try:
            q.get(False)
        except queue.Empty:  # q.empty might be returning a wrong value
            break


def try_put(calling_instance, q, msg, cleanup_func=None):
    """tries putting a message into a queue.

    if the queue.full exception occurs, a cleanup function is called (if specified)

    args:
        calling_instance (object): the instance trying to send a message. should be called with self
        q (queue): queue that is used for sending
        msg (object): message to be put into queue
        cleanup_func=None (function): Function to be called in case of queue.Full exception
    """
    try:
        q.put_nowait(msg)
    except queue.Full as f:
        message = "Deadly communication exception raised:" + type(f).__name__
        print_msg(calling_instance, message, calling_func=sys._getframe(
            1).f_code.co_name, color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
        if cleanup_func is None:
            exit(1)
        else:
            cleanup_func()


def print_widget_data(widg):
    """prints all parameter values of a given widget. for debug purposes"""
    print_dbg('\nWidget Name: {}'.format(widg.winfo_class()))
    keys = widg.keys()
    for key in keys:
        print_dbg("Attribute: {:<20}".format(key))
        value = widg[key]
        vtype = type(value)
        print_dbg('Type: {:<30} Value: {}'.format(str(vtype), value))


class extIntVar(IntVar):
    """IntVar object that extends the basic tkinter IntVar.

    adds basic arithmetic and logical operators
    """

    def __init__(self, *args, **kwargs):
        super(extIntVar, self).__init__(*args, **kwargs)

    # +=
    def __iadd__(self, other):
        if isinstance(other, IntVar):
            self.set(self.get() + other.get())
        else:
            self.set(self.get() + other)
        return self

    # +
    def __add__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(self.get() + other.get())
        else:
            tmp.set(self.get() + other)
        return tmp

    # + (called if __add__ not implemented for left summand)
    def __radd__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(self.get() + other.get())
        else:
            tmp.set(self.get() + other)
        return tmp

    # -=
    def __isub__(self, other):
        if isinstance(other, IntVar):
            self.set(self.get() - other.get())
        else:
            self.set(self.get() - other)
        return self

    # -
    def __sub__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(self.get() - other.get())
        else:
            tmp.set(self.get() - other)
        return tmp

    # see radd
    def __rsub__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(other.get() - self.get())
        else:
            tmp.set(other.get() - self)
        return tmp

    # *
    def __mul__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(other.get() * self.get())
        else:
            tmp.set(other * self.get())
        return tmp

    # /
    def __div__(self, other):
        tmp = extIntVar()
        if isinstance(other, IntVar):
            tmp.set(self.get() / other.get())
        else:
            tmp.set(self.get() / other)
        return tmp

    def __truediv__(self, other):
        return self.__div__(other)

    # <
    def __lt__(self, other: IntVar):
        return (self.get() < other.get())

    # >
    def __gt__(self, other: IntVar):
        return (self.get() > other.get())


class mcuMessages():
    # don't use 2 endcharacters just /r or /n
    STOP_ALL = "J\r".encode('utf-8')
    TOP_VALVE_UP = "K;1\r".encode('utf-8')
    TOP_VALVE_DOWN = "K;0\r".encode('utf-8')
    BOT_VALVE_UP = "M;0\r".encode('utf-8')
    BOT_VALVE_DOWN = "M;1\r".encode('utf-8')
    IR_ON = "P;1\r".encode('utf-8')
    IR_OFF = "P;0\r".encode('utf-8')
    IR_NOSPACER = "P;2\r".encode('utf-8')
    #CLOSE_BRIDGE    = "C;0\r".encode('utf-8')
    #OPEN_BRIDGE     = "C;1\r".encode('utf-8')
    STEP_BACK = "A;0;1;30;1\r".encode('utf-8')
    STEP = "A;1;0;30;1\r".encode('utf-8')
    AUTOWINDUP_R = "S;1\r".encode('utf-8')
    AUTOWINDUP_L = "S;0\r".encode('utf-8')

    def START_INTERRUPTABLE(drive_speed, approaching_speed=None):
        return "R;0;{};{}\r".format(drive_speed,
                                    approaching_speed if approaching_speed is not None else drive_speed)\
            .encode('utf-8')

    def START_REWIND(rewind_speed):
        return "O;0;1;{}\r".format(rewind_speed).encode('utf-8')

    def START_DRIVE(drive_speed):
        return "O;1;0;{}\r".format(drive_speed).encode('utf-8')


class helpMessages():
    """includes all help messages"""
    ########################################## parameter frame: ##############################################
    # buttons:
    SET_DRIVE_TIMEOUT_MSG = ("Before a new test run a timer has to be set to properly count packages.\n" +
                             "Click this button to set this timer. Also needs to be set if the gear speed " +
                             "changed after Timeout was already set\nGreen: Time is set\nRed: Time needs to be set!")
    SET_POSITIONS_MSG = ("Sets how many positions/packages are tested at once. First enter value," +
                         " then acknowledge with 'set'.")
    SET_POSITIONS_TOTAL_MSG = ("Sets total number of positions/packages that are tested.\n" +
                               "Enter 0 for infinite runs.")
    SET_PACKAGES_PER_POSITION_MSG = "Defines how many packages fit into one position."
    SET_DISTANCE_TESTING_MARKING_MSG = "Defines the distance between testing position and marking position."
    SET_ALL_MSG = "Saves all entered values."
    TEMPLATE_MSG = ("Takes the current video stream frame as new template. Template size can be " +
                    "altered - should be less than two positions so that the counting still " +
                    "works properly.")
    TEMPLATE_RESET_MSG = "Resets the template size to the default size.\n" +\
        "(Default size changes depending on the chosen AutoSet package.)"
    TEMPLATE_RESIZE_MSG = "Opens the template resize dialog where the template size can ba altered manually."
    POS_PACK_VIEW_MSG = "Specifies which value it expects from the user to enter."
    GETTING_STARTED_MSG = "Shows first steps."
    HELP_MSG = "Press any button or label to show help. Before, press Help again."
    SETUP_PACKAGE_MSG = ("Different Packages can be selected to automatically adjust all parameters.\n" +
                         "Until now, these parameters only include dummy values.")
    AUTOSET_PACKAGE_MSG = "Sets Package automatically"
    FEED_IN_MSG = "Drives until a package is detected"
    ABOUT_MSG = "Shows information about the running software."

    # labels in parameter frame:
    LABEL_POSITIONS_MSG = SET_POSITIONS_MSG
    LABEL_POSITIONSTOTAL_MSG = SET_POSITIONS_TOTAL_MSG
    LABEL_PACKPERPOS_MSG = SET_PACKAGES_PER_POSITION_MSG
    LABEL_GEARSPEED_MSG = ("Sets speed of stepper motor which drives the gear for precise positioning. After changing that value, " +
                           "the 'set drive out' function has to be run again to ensure correct timing behavior.")
    LABEL_DRIVESPEED_MSG = "Sets speed of stepper motor which drives the rear reel for fast forwarding."
    LABEL_REWINDSPEED_MSG = "Sets speed of stepper motor which drives the rear reel for rewinding."
    LABEL_DISTTOMARK_MSG = SET_DISTANCE_TESTING_MARKING_MSG

    # entries in parameter frame:
    ENTRY_POSITIONS_MSG = SET_POSITIONS_MSG
    ENTRY_POSITIONSTOTAL_MSG = SET_POSITIONS_TOTAL_MSG
    ENTRY_PACKPERPOS_MSG = SET_PACKAGES_PER_POSITION_MSG
    ENTRY_DISTTOMARK_MSG = SET_DISTANCE_TESTING_MARKING_MSG

    # frames in parameter frame
    FRAME_TESTERCONN_MSG = "Indicates the connection to the tester.\nIf the Tester is not connected, a run can't be started"
    FRAME_PACKAGE_TYPE_MSG = "If AutoSet was used, the chosen package type is shown."

    ############################################# control frame: #############################################
    # buttons:
    REWIND_MSG = "Rewinds the reel with the defined speed or step-wise."
    STOP_MSG = "Stops current task and switches state to idle."
    PAUSE_MSG = "Pauses at testing or marking position. Enables possibility of retesting or remarking."
    RESUME_MSG = "Resumes paused task."
    DRIVE_MSG = "Starts to unwind the reel in the forward direction."
    RUN_MSG = ("Starts fully automated test. Only stops if a position failed the test and reached the " +
               "marking position defined by the user ('Positions from Testing to Marking'), since marking is not " +
               "automated yet.")
    TESTONLY_MSG = ("Starts test run with marking disabled. Current implementation needs the user to mark faulty positions manually "
                    + "and thus pauses the program.")
    MARKONLY_MSG = ("Starts test run without testing, only failed positions (stored in history) will be marked. Currently, marking "
                    + "is done manually, in the near future a automated marking station is added.")
    COUNTONLY_MSG = ("Starts run without testing or marking. No target count is used. This is used for counting the Modules. "
                     + "(Mode is more or less experimental, needs user action to ensure correct values because of lead"
                     + "tape or in case of a drive timeout error!)")
    FEED_IN_MSG = "Drives until the first package is detected"
    MODE_SELECTOR_MSG = ("Choose run mode to use different of the handlers functionalities."
                         + "For further information select a mode and use the help mode on the run button.")
    STEP_MSG = "Drives the stepper motors one step in either direction: left button rewinds, right button forwards."
    VALVE_MSG = "Allows manual control of the valves. They can also be manually driven by the three switches at the handler."
    RETEST_MSG = "Retests current position."
    REMARK_MSG = "Remarks current position. "

    ############################################# counter frame: #############################################
    # buttons:
    HISTORY_MSG = "Shows history of failed positions with the ability to drive directly to them for testing."
    RESET_MSG = "Resets counter."
    SETPOSTOTAL_MSG = "Current position on the reel can be set manually"
    GOTO_MSG = "Goes directly to wanted position. Suitable for manual testing or marking of one specific pos."

    # labels:
    LABEL_COUNTERPOS_MSG = "Displays current position."
    LABEL_COUNTERPOSPACK_MSG = "Displays counted packages."
    LABEL_COUNTERPOSTOTAL_MSG = "Shows total positions."
    LABEL_NEXTMARKING_MSG = "Shows when the next marking position is reached."
    LABEL_COUNTERTESTED_MSG = "Displays the number of tested packages."
    LABEL_COUNTERPASSED_MSG = "Displays the number of packages which passed the test."
    LABEL_COUNTERFAILED_MSG = "Displays the number of packages which failed the test."
    LABEL_COUNTERMARKED_MSG = "Displays the number of packages which are already marked."
    LABEL_COUNTERPASSTIME_MSG = "Displays the ratio of passed positions over failed ones."
    LABEL_ESTTIME_MSG = "Displays estimated time when the test is finished. Not useful when infinite packages is set."

    # entries:
    ENTRY_COUNTERPOS_MSG = LABEL_COUNTERPOS_MSG
    ENTRY_COUNTERPOSPACK_MSG = LABEL_COUNTERPOSPACK_MSG
    ENTRY_COUNTERPOSTOTAL_MSG = LABEL_COUNTERPOSTOTAL_MSG
    ENTRY_NEXTMARKING_MSG = LABEL_NEXTMARKING_MSG
    ENTRY_COUNTERTESTED_MSG = LABEL_COUNTERTESTED_MSG
    ENTRY_COUNTERPASSED_MSG = LABEL_COUNTERPASSED_MSG
    ENTRY_COUNTERFAILED_MSG = LABEL_COUNTERFAILED_MSG
    ENTRY_COUNTERMARKED_MSG = LABEL_COUNTERMARKED_MSG
    ENTRY_COUNTERPASSTIME_MSG = LABEL_COUNTERPASSTIME_MSG
    ENTRY_ESTTIME_MSG = LABEL_ESTTIME_MSG

    # progressbar
    PROGRESSBAR_RUNPROGRESS_MSG = "Shows the progress of the test run.\n" +\
                                  "Only active if a target package count is set."

    # frame
    ENTRY_TESTVISUALS_FRAME_MSG = "Here are the results of the last test run shown."
