"""module file parameterFrame. used to slim down the main file

classes:
    ParameterFrame: the part of the gui where the config elements are located.
"""

# import tkinter as tk
# from tkinter import ttk

from tkinter import *
# import tkinter as tk
from tkinter import ttk
from ttkthemes import themed_tk as tk  # TODO: update GUI with themed tk
from tkinter import font
import skimage
from skimage.metrics import structural_similarity as ssim

import numpy as np
from PIL import ImageTk, Image
import cv2
import imutils.video.pivideostream
import cv2

from configuration import HandlerConfig as config
from errorHandler import HandshakeException

from programStates import States
from cmdPrinter import print_msg, print_dbg
from videoCounter import VideoCounterMessages as vcm


from helper import helpMessages as help
import helper
from helper import mcuMessages as MCU
import videoCounter
from videoCounter import VideoCounter as vc


class ParameterFrame(Frame):
    """frame where the config elements are located.

    the access of maun process' members is done directly via self.parent.member. this might be not
    pretty but the frame files are to be seen as extension from the main file with the only task to
    split the main file in smaller files.
    buttons trigger onButtonClick callbacks, the enabled button change according to the system state.

    buttons/elements:
        set pos: value defines the distance from test to test - defines how many positions are
            tested simultaneously
        set pos total: value of total positing to be tested. if -1 is set, the handler works until
            stop it clicked (or end of tape leads to timeout error)
        package per position: how many packages are at one position (2,3,4,..). depending on tape
            type.
        drive speed: speed for gear at driving command
        gear speed: speed for gear at test run
        rewind speed: speed for rewind reel
            speeds don't need to be set explicitly by pressing set
        distance testing marking: distance from test position do marking positions in n (int)
            positions
        set all: sets all previous values at once
        drive timeout: opens popup to ask for starting the drive timeout set procedure
        +, ., -, template: template commands. plus and minus alters the template with before taking
            new template. "." resets size to default before taking new template. template takes new
            template without altering size.
    """

    def __init__(self, parent, **options):
        ttk.Frame.__init__(self, parent, **options)
        self.parent = parent
        # self.pos_pack_var = IntVar()
        # self.pos_pack_var.set(1)
        # self.pos_pack_var_old_val = 1

        self.pos_pack_var_old_val = "Packages"
        self.pos_pack_var = "Packages"

        self.connection_state = StringVar()
        self.connection_state.set("Connecting/Undefined")

        self.module_thumbnail = None
        self.help_msg_dict = dict()

        self.add_widgets()
        self.setup_help_msg_dict()

    def onClickSetPositions(self, entry_pos, entry_pos_tot):
        """ sets the position/packages value to the value specified in entry.

        the value gets clipped to below zero.below. depending on the chosen view packages or position
        is entered. im packages is chosen, the value gets changed to a even multiple of the
        packages per pos value

        args:
            entry (tk.entry): textfield where the new value is entered.
            entry_pos_tot:      textfield where the value for positions total might be updated
        """
        # show some help if state is set to HELP
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.SET_POSITIONS_MSG)
            return

        print_msg(self, "packages:" + entry_pos.get())
        if entry_pos.get().isdigit():
            if self.pos_pack_var == "Positions":  # pos view
                self.parent.positions.set(
                    np.clip(int(entry_pos.get()), 1, None))
                self.parent.packages = self.parent.positions\
                    * self.parent.packages_per_position
                entry_pos.delete(0, len(entry_pos.get()))
                entry_pos.insert(0, str(self.parent.positions.get()))
                # pos_total should be at least pos (if its infinite pos, no change)
                if self.parent.positions_total.get() != (-1) and self.parent.positions_total < self.parent.positions:
                    self.parent.positions_total.set(
                        self.parent.positions.get())
                    self.parent.packages_total = self.parent.positions_total\
                        * self.parent.packages_per_position
                    entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                    entry_pos_tot.insert(
                        0, str(self.parent.positions_total.get()))

            if self.pos_pack_var == "Packages":  # pack view
                self.parent.packages.set(
                    np.clip(int(entry_pos.get()), 1, None))
                self.parent.positions = self.parent.packages\
                    / self.parent.packages_per_position
                # if entered packages are not a multiple of packages per positions
                # change the value to the next full multiple
                if self.parent.packages.get() % self.parent.packages_per_position.get() != 0:
                    self.parent.positions += 1
                    self.parent.packages = self.parent.positions\
                        * self.parent.packages_per_position
                entry_pos.delete(0, len(entry_pos.get()))
                entry_pos.insert(0, str(self.parent.packages.get()))

                # pack total should be at least pack (if infinite pack, no change)
                if self.parent.packages_total.get() != (-1) and self.parent.packages_total < self.parent.packages:
                    self.parent.packages_total.set(self.parent.packages.get())
                    self.parent.positions_total = self.parent.packages_total\
                        / self.parent.packages_per_position
                    entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                    entry_pos_tot.insert(
                        0, str(self.parent.packages_total.get()))
        else:
            # if entered value is not a number, delete it
            entry_pos.delete(0, len(entry_pos.get()))
            if self.pos_pack_var == "Positions":  # pos
                entry_pos.insert(0, str(self.parent.positions.get()))
            if self.pos_pack_var == "Packages":  # pack
                entry_pos.insert(0, str(self.parent.packages.get()))

    def onClickSetPositionsTotal(self, entry_pos, entry_pos_tot):
        """ sets the total position/packages value to the value specified in entry.

        if the value is 0, the entry value gets set to "infinite". the actual parent.member gets set
        to -1. values smaller than 0 get ignored and entry field is reset.
        depending on the chosen view packages or position
        is entered. im packages is chosen, the value gets changed to a even multiple of the
        packages per pos value

        args:
            entry (tk.entry): textfield where the new value is entered.
        """
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.SET_POSITIONS_TOTAL_MSG)
            return

        print_msg(self, "packages total:" + entry_pos_tot.get())
        if entry_pos_tot.get().isdigit() and int(entry_pos_tot.get()) >= 0:
            if int(entry_pos_tot.get()) == 0:  # if pack_tot/pos_tot is set to 0 -> infinite packages
                self.parent.positions_total.set(-1)
                self.parent.packages_total.set(-1)

                entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                entry_pos_tot.insert(0, str("Infinite"))
                # self.parent.counter_frame.pbProgress["mode"] = "indeterminate"
                return
            else:
                if self.pos_pack_var == "Positions":  # pos view
                    self.parent.positions_total.set(int(entry_pos_tot.get()))
                    self.parent.packages_total = self.parent.positions_total\
                        * self.parent.packages_per_position
                    entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                    entry_pos_tot.insert(
                        0, str(self.parent.positions_total.get()))

                    # pos should be maximum pos_tot
                    if self.parent.positions_total < self.parent.positions:
                        self.parent.positions.set(
                            self.parent.positions_total.get())
                        self.parent.packages = self.parent.positions\
                            * self.parent.packages_per_position
                        entry_pos.delete(0, len(entry_pos.get()))
                        entry_pos.insert(0, str(self.parent.positions.get()))
                if self.pos_pack_var == "Packages":  # pack view
                    self.parent.packages_total.set(int(entry_pos_tot.get()))
                    self.parent.positions_total = self.parent.packages_total\
                        / self.parent.packages_per_position
                    # if entered packages_tot are not a multiple of packages per positions
                    # change the value to the next full multiple
                    if self.parent.packages_total.get() % self.parent.packages_per_position.get() != 0:
                        self.parent.positions_total += 1
                        self.parent.packages_total = self.parent.positions_total\
                            * self.parent.packages_per_position
                    entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                    entry_pos_tot.insert(
                        0, str(self.parent.packages_total.get()))

                    # packages should be maximum packages total
                    if self.parent.packages_total < self.parent.packages:
                        self.parent.packages.set(
                            self.parent.packages_total.get())
                        self.parent.positions = self.parent.packages\
                            / self.parent.packages_per_position
                        entry_pos.delete(0, len(entry_pos.get()))
                        entry_pos.insert(0, str(self.parent.packages.get()))

        # invalid value -> reset value
        else:
            entry_pos_tot.delete(0, len(entry_pos_tot.get()))
            if self.pos_pack_var == "Positions":  # pos
                entry_pos_tot.insert(0, str(self.parent.positions_total.get()))
            if self.pos_pack_var == "Packages":
                entry_pos_tot.insert(0, str(self.parent.packages_total.get()))
            if self.parent.positions_total == -1:
                entry_pos_tot.insert(0, str("Infinite"))

        print_msg(self, "maximum changed!")
        self.parent.counter_frame.pbProgress['maximum'] = self.parent.packages_total.get(
        )

    def onClickSetPackagesPerPosition(self, entry_ppp, entry_pos, entry_pos_tot):
        """ sets the position value to the value specified in entry.

        values smaller than 1 gets ignored and entry field is reset.

        args:
            entry (tk.entry): textfield where the new value is entered.
        """
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(
                help.SET_PACKAGES_PER_POSITION_MSG)
            return

        print_msg(self, "packages per position:" + entry_ppp.get())
        if entry_ppp.get().isdigit() and int(entry_ppp.get()) >= 1:
            self.parent.packages_per_position.set(int(entry_ppp.get()))

            if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                entry_pos_tot.delete(0, len(entry_pos_tot.get()))
            entry_pos.delete(0, len(entry_pos.get()))
            entry_ppp.delete(0, len(entry_ppp.get()))
            entry_ppp.insert(0, str(self.parent.packages_per_position.get()))

            if self.pos_pack_var == "Positions":  # pos view
                # set packages according to new pack_per_pos (pos stay the same)
                self.parent.packages.set(
                    self.parent.packages_per_position.get() * self.parent.positions.get())
                entry_pos.insert(0, str(self.parent.positions.get()))
                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    self.parent.packages_total.set(
                        self.parent.packages_per_position.get() * self.parent.positions_total.get())
                    entry_pos_tot.insert(
                        0, str(self.parent.positions_total.get()))

            elif self.pos_pack_var == "Packages":  # pack view
                # if packages are not a full multiple of pack_per_pos, set it to the next full multiple
                if self.parent.packages.get() % self.parent.packages_per_position.get() != 0:
                    self.parent.positions.set(
                        (self.parent.packages.get() // self.parent.packages_per_position.get()) + 1)
                    self.parent.packages.set(self.parent.positions.get(
                    ) * self.parent.packages_per_position.get())

                    if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                        self.parent.positions_total.set(
                            (self.parent.packages_total.get() // self.parent.packages_per_position.get()) + 1)
                        # pos total should be at least pos
                        if self.parent.positions_total < self.parent.positions:
                            self.parent.positions_total.set(
                                self.parent.positions.get())
                        self.parent.packages_total.set(
                            self.parent.positions_total.get() * self.parent.packages_per_position.get())

                # if packages are a full multiple of pack_per_pos just change position(_tot)
                else:
                    self.parent.positions.set(self.parent.packages.get(
                    ) // self.parent.packages_per_position.get())
                    if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                        self.parent.positions_total.set(
                            self.parent.packages_total.get() // self.parent.packages_per_position.get())

                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    entry_pos_tot.insert(
                        0, str(self.parent.packages_total.get()))
                entry_pos.insert(0, str(self.parent.packages.get()))

    def onClickSetDriveSpeed(self, val_string, label):
        """ sets the drive speed (gear speed at drive command)

        value is directly connected with scale variable. this function is only for the displaying of
        the speed. the speed a value (254 - 0, where 0 is the FASTEST) gets converted to a value
        from 0-100 and displayed at label.

        args:
            val_string: a string containing the scales value. unused but necessary, because the
                scale calls the command with this argument. this is specified by tkinter and not
                changeable.
            label: the ttk.label where the speed is displayed
        """
        displayed = str(100 - int((self.parent.drive_speed.get() - config.GEAR_SPEED_LIMIT)
                                  / (254 - config.GEAR_SPEED_LIMIT) * 100))
        print_msg(self, "driveSpeed total:" + displayed + " (real value: "
                        + str(self.parent.drive_speed.get()) + ")")
        label.config(text=displayed)

    def onClickSetGearSpeed(self, val_string, label):
        """ sets the gear speed (gear speed at test run)

        value is directly connected with scale variable. this function is only for the displaying of
        the speed. the speed a value (254 - 0, where 0 is the FASTEST) gets converted to a value
        from 0-100 and displayed at label.

        args:
            val_string: a string containing the scales value. unused but necessary, because the
                scale calls the command with this argument. this is specified by tkinter and not
                changeable.
            label: the ttk.label where the speed is displayed
        """
        displayed = str(100 - int((self.parent.gear_speed.get() - config.GEAR_SPEED_LIMIT)
                                  / (254 - config.GEAR_SPEED_LIMIT) * 100))
        print_msg(self, "gearSpeed total:" + displayed + " (real value: "
                        + str(self.parent.gear_speed.get()) + ")")
        label.config(text=displayed)

        # after changing the gear speed, new timeout has to be measured
        print_msg(self, "Need new timeout")
        self.parent.drive_timeout_set = False

    def onClickSetRewindSpeed(self, val_string, label):
        """ sets the rewind speed (rewind reel speed at rewind command)

        value is directly connected with scale variable. this function is only for the displaying of
        the speed. the speed a value (254 - 0, where 0 is the FASTEST) gets converted to a value
        from 0-100 and displayed at label.

        args:
            val_string: a string containing the scales value. unused but necessary, because the
                scale calls the command with this argument. this is specified by tkinter and not
                changeable.
            label: the ttk.label where the speed is displayed
        """
        displayed = str(100 - int((self.parent.rewind_speed.get() - config.WHEEL_SPEED_LIMIT)
                                  / (254 - config.WHEEL_SPEED_LIMIT) * 100))
        print_msg(self, "rewindSpeed total:" + displayed + " (real value: "
                        + str(self.parent.rewind_speed.get()) + ")")
        label.config(text=displayed)

    def onClickSetDistanceTestingMarking(self, entry):
        """ sets the distance from testing to marking pos to value specified in entry.

        values smaller than 1 gets ignored and entry field is reset.

        args:
            entry (tk.entry): textfield where the new value is entered.
        """
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(
                help.SET_DISTANCE_TESTING_MARKING_MSG)
            return

        print_msg(self, "distance:" + entry.get())
        if entry.get().isdigit() and int(entry.get()) >= 1:
            self.parent.distance_testing_marking.set(int(entry.get()))

        entry.delete(0, len(entry.get()))
        entry.insert(0, str(self.parent.distance_testing_marking.get()))

    def onClickSetAll(self, e_packages, e_p_total, e_ppp, e_dtm):
        """sets all parameters above"""
        # show some help if needed
        print_msg(self, "")
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.SET_ALL_MSG)
            return

        self.onClickSetPositions(self.ePositions, e_p_total)
        self.onClickSetPositionsTotal(e_packages, e_p_total)
        self.onClickSetPackagesPerPosition(e_ppp, e_packages, e_p_total)
        self.onClickSetDistanceTestingMarking(e_dtm)

    def onClickSetDriveTimeout(self):
        """starts call drive timout procedure if concurring popup yields confirming user input"""
        print_msg(self, "")
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(
                help.SET_DRIVE_TIMEOUT_MSG)  # show popup of help
            return   # return so normal button function is not executed

        # state is not help
        if self.parent.dialog_handler.setDriveTimerPopup():
            self.parent.set_drive_timeout()
            self.parent.drive_timeout_set = True

    def onClickTemplate(self, resize=None, reset=None):
        """triggers template command

        args:
            resize: specifies if template size gets altered before taking new template
                +: with incremented
                -: width decremented
                .: reset to default
                else: no altering of size
        """
        # show some help if needed
        if self.parent.state is States.HELP:
            if resize is not None:
                self.parent.dialog_handler.showHelp(help.TEMPLATE_RESIZE_MSG)
            elif reset is not None:
                self.parent.dialog_handler.showHelp(help.TEMPLATE_RESET_MSG)
            else:
                self.parent.dialog_handler.showHelp(help.TEMPLATE_MSG)
            return

        print_msg(self, "onClickTemplate")
        self.parent.stop_handler()
        if resize is True:
            self.parent.dialog_handler.showResizeDialog()
        else:
            if reset is True:
                resize = "."
            try:
                self.parent.command_handshake(self.parent.handshake_to_vid,
                                              self.parent.handshake_from_vid, (
                                                  vcm.TEMPLATE, resize),
                                              vcm.READY)
            except HandshakeException:
                pass

    def onClickMatchTemplate(self, reset=None):
        """triggers template command

        args:
            resize: specifies if template size gets altered before taking new template
                +: with incremented
                -: width decremented
                .: reset to default
                else: no altering of size
        """
        # show some help if needed
        if self.parent.state is States.HELP:
            if reset is not None:
                self.parent.dialog_handler.showHelp(help.TEMPLATE_RESET_MSG)
            else:
                self.parent.dialog_handler.showHelp(help.TEMPLATE_MSG)
            return

        print_msg(self, "onClickMatchTemplate")
        self.parent.stop_handler()
        try:
            self.parent.command_handshake(self.parent.handshake_to_vid,
                                          self.parent.handshake_from_vid, (
                                              vcm.MATCHTEMPLATE,),
                                          vcm.READY)
        except HandshakeException:
            pass

    def onClickPosPackView(self, entry_pos, entry_pos_tot):
        """changes the view between positions and packages for the pos and pos total entrys"""
        # if self.parent.state is States.HELP:
        #      #self.parent.dialog_handler.showHelp(help.POS_PACK_VIEW_MSG)
        #      return
        print_msg(self, str(self.cbChoosePosPack.get()))
        self.pos_pack_var = str(self.cbChoosePosPack.get())
        # check state of combobox:
        if self.pos_pack_var_old_val != self.pos_pack_var:
            self.pos_pack_var_old_val = self.pos_pack_var

            # infinite positions means also infinite packages
            if self.parent.positions_total == (-1):
                entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                entry_pos_tot.insert(0, str("Infinite"))

            if self.pos_pack_var_old_val == "Positions":  # pos view
                entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                entry_pos_tot.insert(0, str(self.parent.positions_total.get()))
                entry_pos.delete(0, len(entry_pos.get()))
                entry_pos.insert(0, str(self.parent.positions.get()))

            elif self.pos_pack_var_old_val == "Packages":  # packages view
                entry_pos_tot.delete(0, len(entry_pos_tot.get()))
                entry_pos_tot.insert(0, str(self.parent.packages_total.get()))
                entry_pos.delete(0, len(entry_pos.get()))
                entry_pos.insert(0, str(self.parent.packages.get()))

    def onClickGettingStarted(self):
        """shows getting started popup"""
        # show some help if needed
        if self.parent.state is States.HELP:
            self.parent.dialog_handler.showHelp(help.GETTING_STARTED_MSG)
            return
        self.parent.dialog_handler.gettingStartedPopup()

    def onClickHelp(self):
        """ changes state to HELP, shows now a popup window when pressing a button its function.
            only works if previous state is IDLE, so help is unavailable during testing.
            no args.
        """
        # show some help if needed
        print_msg(self, "onClickHelp" + ", state is: " +
                  States(self.parent.state).name)
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(help.HELP_MSG)
            return

        if self.parent.state == States.IDLE:
            self.parent.set_state(States.HELP)
            self.parent.cursor = 'question_arrow'

    def onClickAutoSetPackage(self, feed_in):
        """sets Package automatically"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(help.AUTOSET_PACKAGE_MSG)
            return

        # Get list of template image file paths from config file
        #template_paths = config.TEMPLATES
        template_paths = [param.IMG_PATH for param in config.MODULE_SETUPS]

        # Create an empty list to store the resized template images
        resized_templates = []

        # Loop through each template image file path, read the image and resize it to 160x200
        for path in template_paths:
            img = cv2.imread(path)
            resized = cv2.resize(img, (160, 200))
            resized_templates.append(resized)

        # Read video_now.png image and resize it to 160x200
        if feed_in == True:
            video = cv2.imread(
                '/home/pi/Desktop/templates/template_area_to_compare.png')
        else:
            video = cv2.imread('/home/pi/Desktop/templates/video_now.png')
        resizevideo = cv2.resize(video, (160, 200))

        # Calculate the normalized cross-correlation (NCC) score between resizevideo and each template image
        ncc_scores = []
        for template in resized_templates:
            ncc_score = cv2.matchTemplate(
                resizevideo, template, cv2.TM_CCORR_NORMED)
            ncc_scores.append(ncc_score)

        # Find the index of the template image with the highest NCC score
        max_index = np.argmax(ncc_scores)

        # Get the path of the matching template image
        matching_template_path = template_paths[max_index]

        # Determine the setup values based on the matching template
        for i, path in enumerate(template_paths):
            if template_paths[i] == matching_template_path:
                GEAR_SPEED, DRIVE_SPEED, REWIND_SPEED, PACKAGES_PER_POSITION, TEMPLATE_WIDTH, IMG_PATH, IMG_PATH_MISSING, IMG_PATH_FAIL, PACKAGE_NAME, DISTANCE_TEST = config.MODULE_SETUPS[
                    i]
                thumbnail = (Image.open(IMG_PATH))
                break

         # setup speed variables
        self.parent.gear_speed.set(GEAR_SPEED)
        self.onClickSetGearSpeed("dummy", self.vGearSpeed)
        self.parent.drive_speed.set(DRIVE_SPEED)
        self.onClickSetDriveSpeed("dummy", self.vDriveSpeed)
        self.parent.rewind_speed.set(REWIND_SPEED)
        self.onClickSetRewindSpeed("dummy", self.vRewindSpeed)

        # setup packages per pos and update the entrys in parameterframe according to the new value
        # and the current viewmode (packages or positions)
        self.parent.packages_per_position.set(PACKAGES_PER_POSITION)

        if self.parent.packages_total.get() != (-1):  # no change at infinite packages
            self.ePositionsTotal.delete(0, len(self.ePositionsTotal.get()))
        self.ePositions.delete(0, len(self.ePositions.get()))
        self.ePacketsPerPosition.delete(0, len(self.ePacketsPerPosition.get()))
        self.ePacketsPerPosition.insert(
            0, str(self.parent.packages_per_position.get()))

        if self.pos_pack_var == "Positions":  # pos view
            # set packages according to new pack_per_pos (pos stay the same)
            self.parent.packages.set(
                self.parent.packages_per_position.get() * self.parent.positions.get())
            self.ePositions.insert(0, str(self.parent.positions.get()))
            if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                self.parent.packages_total.set(
                    self.parent.packages_per_position.get() * self.parent.positions_total.get())
                self.ePositionsTotal.insert(
                    0, str(self.parent.positions_total.get()))

        elif self.pos_pack_var == "Packages":  # pack view
            # if packages are not a full multiple of pack_per_pos, set it to the next full multiple
            if self.parent.packages.get() % self.parent.packages_per_position.get() != 0:
                self.parent.positions.set(
                    (self.parent.packages.get() // self.parent.packages_per_position.get()) + 1)
                self.parent.packages.set(self.parent.positions.get(
                ) * self.parent.packages_per_position.get())
                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    self.parent.positions_total.set(
                        (self.parent.packages_total.get() // self.parent.packages_per_position.get()) + 1)
                    # pos total should be at least pos
                    if self.parent.positions_total < self.parent.positions:
                        self.parent.positions_total.set(
                            self.parent.positions.get())
                    self.parent.packages_total.set(
                        self.parent.positions_total.get() * self.parent.packages_per_position.get())

            # if packages are a full multiple of pack_per_pos just change position(_tot)
            else:
                self.parent.positions.set(self.parent.packages.get(
                ) // self.parent.packages_per_position.get())
                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    self.parent.positions_total.set(
                        self.parent.packages_total.get() // self.parent.packages_per_position.get())

            self.ePositions.insert(0, str(self.parent.packages.get()))
            if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                self.ePositionsTotal.insert(
                    0, str(self.parent.packages_total.get()))

        # change default template size
        self.parent.stop_handler()
        try:
            self.parent.command_handshake(self.parent.handshake_to_vid,
                                          self.parent.handshake_from_vid, (
                                              vcm.TEMPLATE, TEMPLATE_WIDTH),
                                          vcm.READY)
            if feed_in:
                self.parent.command_handshake(self.parent.handshake_to_vid,
                                              self.parent.handshake_from_vid, (
                                                  vcm.PARAMETER, IMG_PATH_MISSING, IMG_PATH_FAIL, PACKAGE_NAME, PACKAGES_PER_POSITION, IMG_PATH, DISTANCE_TEST, TRUE),
                                              vcm.READY)
            else:
                self.parent.command_handshake(self.parent.handshake_to_vid,
                                              self.parent.handshake_from_vid, (
                                                  vcm.PARAMETER, IMG_PATH_MISSING, IMG_PATH_FAIL, PACKAGE_NAME, PACKAGES_PER_POSITION, IMG_PATH, DISTANCE_TEST, FALSE),
                                              vcm.READY)

        except HandshakeException:
            pass

        # set up the thumbnail in the main window that shows the set module type
        self.update_module_thumbnail(thumbnail)
        self.lNameModule.config(text=PACKAGE_NAME)

    def onClickSetUpPackage(self):
        """opens module select dialog, where the use can select a module type. the parameters
        are set accordingly automatically"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(help.SETUP_PACKAGE_MSG)
            return

        # returns a a parameterstruct object that contains the paramters of the chosen module type,
        # and Image object of the template
        ret = self.parent.dialog_handler.showModuleTypeSelector()
        if ret is None:
            return
        param_struct, thumbnail = ret

        # setup speed variables
        self.parent.gear_speed.set(param_struct.GEAR_SPEED)
        self.onClickSetGearSpeed("dummy", self.vGearSpeed)
        self.parent.drive_speed.set(param_struct.DRIVE_SPEED)
        self.onClickSetDriveSpeed("dummy", self.vDriveSpeed)
        self.parent.rewind_speed.set(param_struct.REWIND_SPEED)
        self.onClickSetRewindSpeed("dummy", self.vRewindSpeed)

        # setup packages per pos and update the entrys in parameterframe according to the new value
        # and the current viewmode (packages or positions)
        self.parent.packages_per_position.set(
            param_struct.PACKAGES_PER_POSITION)

        if self.parent.packages_total.get() != (-1):  # no change at infinite packages
            self.ePositionsTotal.delete(0, len(self.ePositionsTotal.get()))
        self.ePositions.delete(0, len(self.ePositions.get()))
        self.ePacketsPerPosition.delete(0, len(self.ePacketsPerPosition.get()))
        self.ePacketsPerPosition.insert(
            0, str(self.parent.packages_per_position.get()))

        if self.pos_pack_var == "Positions":  # pos view
            # set packages according to new pack_per_pos (pos stay the same)
            self.parent.packages.set(
                self.parent.packages_per_position.get() * self.parent.positions.get())
            self.ePositions.insert(0, str(self.parent.positions.get()))
            if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                self.parent.packages_total.set(
                    self.parent.packages_per_position.get() * self.parent.positions_total.get())
                self.ePositionsTotal.insert(
                    0, str(self.parent.positions_total.get()))

        elif self.pos_pack_var == "Packages":  # pack view
            # if packages are not a full multiple of pack_per_pos, set it to the next full multiple
            if self.parent.packages.get() % self.parent.packages_per_position.get() != 0:
                self.parent.positions.set(
                    (self.parent.packages.get() // self.parent.packages_per_position.get()) + 1)
                self.parent.packages.set(self.parent.positions.get(
                ) * self.parent.packages_per_position.get())
                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    self.parent.positions_total.set(
                        (self.parent.packages_total.get() // self.parent.packages_per_position.get()) + 1)
                    # pos total should be at least pos
                    if self.parent.positions_total < self.parent.positions:
                        self.parent.positions_total.set(
                            self.parent.positions.get())
                    self.parent.packages_total.set(
                        self.parent.positions_total.get() * self.parent.packages_per_position.get())

            # if packages are a full multiple of pack_per_pos just change position(_tot)
            else:
                self.parent.positions.set(self.parent.packages.get(
                ) // self.parent.packages_per_position.get())
                if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                    self.parent.positions_total.set(
                        self.parent.packages_total.get() // self.parent.packages_per_position.get())

            self.ePositions.insert(0, str(self.parent.packages.get()))
            if self.parent.packages_total.get() != (-1):  # no change at infinite packages
                self.ePositionsTotal.insert(
                    0, str(self.parent.packages_total.get()))

        # change default template size
        self.parent.stop_handler()
        try:
            self.parent.command_handshake(self.parent.handshake_to_vid,
                                          self.parent.handshake_from_vid, (
                                              vcm.TEMPLATE, param_struct.TEMPLATE_WIDTH),
                                          vcm.READY)
            self.parent.command_handshake(self.parent.handshake_to_vid,
                                          self.parent.handshake_from_vid, (vcm.PARAMETER, param_struct.IMG_PATH_MISSING, param_struct.IMG_PATH_FAIL,
                                                                           param_struct.PACKAGE_NAME, param_struct.PACKAGES_PER_POSITION, param_struct.IMG_PATH, param_struct.DISTANCE_TEST, FALSE),
                                          vcm.READY)

        except HandshakeException:
            pass

        # set up the thumbnail in the main window that shows the set module type
        self.update_module_thumbnail(thumbnail)
        self.lNameModule.config(text=param_struct.PACKAGE_NAME)

    def onClickAbout(self):
        """calls dialog to show infos about the running software"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(help.ABOUT_MSG)
        else:
            self.parent.dialog_handler.showAbout()

    def onLooseFocusSetPositions(self, e):
        """if the set positions entry field looses focus, is is reset to the current value if
        the newly focused widget is not the corresponding set or the set all button"""
        # print_dbg("onclickloosefocus:", e.widget.get())
        if not self.focus_displayof() in (self.bSetAll, self.bPositions):
            self.ePositions.delete(0, len(self.ePositions.get()))
            if self.pos_pack_var == "Positions":  # pos
                self.ePositions.insert(
                    0, str(self.parent.positions_total.get()))
            if self.pos_pack_var == "Packages":  # pack
                self.ePositions.insert(
                    0, str(self.parent.packages_total.get()))

    def onLooseFocusSetPositionsTotal(self, e):
        """if the set positions total entry field looses focus, is is reset to the current value if
        the newly focused widget is not the corresponding set or the set all button"""
        if not self.focus_displayof() in (self.bSetAll, self.bPositionsTotal):
            self.ePositionsTotal.delete(0, len(self.ePositionsTotal.get()))
            if self.pos_pack_var == "Positions":  # pos
                self.ePositionsTotal.insert(
                    0, str(self.parent.positions_total.get()))
            if self.pos_pack_var == "Packages":
                self.ePositionsTotal.insert(
                    0, str(self.parent.packages_total.get()))
            if self.parent.positions_total == -1:
                self.ePositionsTotal.insert(0, str("Infinite"))

    def onLooseFocusPackagesPerPosition(self, e):
        """if the set packages per position entry field looses focus, is is reset to the current value if
        the newly focused widget is not the corresponding set or the set all button"""
        if not self.focus_displayof() in (self.bSetAll, self.bPacketsPerPosition):
            self.ePacketsPerPosition.delete(
                0, len(self.ePacketsPerPosition.get()))
            self.ePacketsPerPosition.insert(
                0, str(self.parent.packages_per_position.get()))

    def onLooseFocusDistanceTestingMarking(self, e):
        """if the set distance to marking entry field looses focus, is is reset to the current value if
        the newly focused widget is not the corresponding set or the set all button"""
        if not self.focus_displayof() in (self.bSetAll, self.bDistanceToMarker):
            self.eDistanceToMarker.delete(0, len(self.eDistanceToMarker.get()))
            self.eDistanceToMarker.insert(
                0, str(self.parent.distance_testing_marking.get()))

    def get_help_msg_callback(self, event):
        """callback that is used by widgets that have no "command" parameter to trigger help popup.
        popup is only triggered if state=HELP, else just do nothing -> that means the bound widgets are
        always clickable, but the callback just triggers no action.
        a callback bound to a event by bind() always gets the event given as argument. the event triggering
        widget can be accessed via event.widget"""
        if self.parent.state == States.HELP:
            self.parent.dialog_handler.showHelp(
                self.help_msg_dict[event.widget])

    def add_widgets(self):
        """"adds the needed elements

        command=lambda: self.onClickAction(arg) is used so it is possible to call the constructor
        with an argument in the onClick callable.
        """
        # set with for whole param frame and make subframes fill horizontal
        # to enable flexible vertical spacing with propagating
        self.configure(width=config.PARAMETER_WIDTH)

        # Declare all subframes

        # divide parameterFrame into sub Frames
        self.parameters = ttk.LabelFrame(
            self, padding=config.PADDING, text="Parameters")
        self.connectionToTester = ttk.LabelFrame(
            self, padding=config.PADDING, text="Connection to Tester")
        self.stateAndTemplateFrame = ttk.Frame(self)
        self.systemState = ttk.LabelFrame(
            self.stateAndTemplateFrame, padding=config.PADDING, text="System State")
        self.packageType = ttk.LabelFrame(self.stateAndTemplateFrame, padding=(
            config.PADDING, config.PADDING_SMALL), text="Package")
        self.spacerframe = ttk.Frame(self)

        # align Subframes, add spacers
        self.parameters.grid(
            row=1, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.spacerframe.grid(row=2, column=1)
        self.grid_rowconfigure(2, weight=1)
        self.connectionToTester.grid(
            row=5, column=1, pady=config.PADDING_GRID, sticky="ew")
        self.systemState.grid(
            row=1, column=1, pady=config.PADDING_GRID, sticky="nsew")
        self.packageType.grid(
            row=1, column=2, pady=config.PADDING_GRID, sticky="nsew")
        self.stateAndTemplateFrame.grid(row=7, column=1, sticky="ew")
        self.stateAndTemplateFrame.grid_columnconfigure(1, weight=2)
        self.stateAndTemplateFrame.grid_columnconfigure(2, weight=1)

        # parameters also has two sub frames
        self.parametersSub1 = ttk.Frame(self.parameters)
        self.parametersSub2 = ttk.Frame(self.parameters)

        self.parametersSub1.grid(row=1, column=1)
        self.parametersSub2.grid(row=2, column=1, pady=10)

        # Preview for selectable templates
        self.lModuleThumbnail = ttk.Label(self.packageType)
        self.lNameModule = ttk.Label(self.packageType)
        # self.fConnectionInfo    = ttk.Frame(self.connectionToTester)  # Connection to tester information

        self.cbChoosePosPack = ttk.Combobox(self.parametersSub1, values=[
                                            "Positions", "Packages"], width=8)
        self.cbChoosePosPack.current(1)

        self.bPositions = ttk.Button(self.parametersSub1, width=5, text="set",
                                     command=lambda:
                                     self.onClickSetPositions(self.ePositions,
                                                              self.ePositionsTotal))
        self.bPacketsPerPosition = ttk.Button(self.parametersSub1, width=5, text="set",
                                              command=lambda:
                                              self.onClickSetPackagesPerPosition(self.ePacketsPerPosition,
                                                                                 self.ePositions,
                                                                                 self.ePositionsTotal))
        self.bPositionsTotal = ttk.Button(self.parametersSub1, width=5, text="set",
                                          command=lambda:
                                          self.onClickSetPositionsTotal(self.ePositions,
                                                                        self.ePositionsTotal))
        self.bDistanceToMarker = ttk.Button(self.parametersSub1, width=5, text="set",
                                            command=lambda:
                                            self.onClickSetDistanceTestingMarking(self.eDistanceToMarker))
        self.bSetAll = ttk.Button(self.parametersSub1, width=5, text="set all",
                                  command=lambda:
                                  self.onClickSetAll(self.ePositions,
                                                     self.ePositionsTotal,
                                                     self.ePacketsPerPosition,
                                                     self.eDistanceToMarker))

        self.sGearSpeed = ttk.Scale(self.parametersSub2, from_=254, to=config.GEAR_SPEED_LIMIT,
                                    orient=HORIZONTAL, variable=self.parent.gear_speed,
                                    command=lambda x: self.onClickSetGearSpeed(x, self.vGearSpeed))
        self.sRewindSpeed = ttk.Scale(self.parametersSub2, from_=254, to=config.WHEEL_SPEED_LIMIT,
                                      orient=HORIZONTAL, variable=self.parent.rewind_speed,
                                      command=lambda x: self.onClickSetRewindSpeed(x, self.vRewindSpeed))
        self.sDriveSpeed = ttk.Scale(self.parametersSub2, from_=254, to=config.GEAR_SPEED_LIMIT,
                                     orient=HORIZONTAL, variable=self.parent.drive_speed,
                                     command=lambda x: self.onClickSetDriveSpeed(x, self.vDriveSpeed))

        # self.lModuleThumbnailLabel  = ttk.Label(self.fThumbnailFrame, text="", anchor="w")
        # self.lModuleThumbnail       = ttk.Label(self.fThumbnailFrame, text="", anchor="e")
        self.lPositions = ttk.Label(
            self.parametersSub1, anchor='e', text='per Test:')
        self.lPositionsTotal = ttk.Label(
            self.parametersSub1, anchor='e', text='in Total:')
        self.lPacketsPerPosition = ttk.Label(
            self.parametersSub1, anchor='e', text='Packages per Position:')
        # self.infoPositionsTotal     = ttk.Label(self.parametersSub1Sub1, anchor='w', text=' set 0')
        self.lDistanceToMarker = ttk.Label(
            self.parametersSub1, anchor='e', text='Distance to Marker Pos:')
        self.lDriveSpeed = ttk.Label(
            self.parametersSub2, anchor='w', text='Drive Speed')
        self.lGearSpeed = ttk.Label(
            self.parametersSub2, anchor='w', text='Gear Speed')
        self.vGearSpeed = ttk.Label(self.parametersSub2, anchor='w', text='')
        self.vDriveSpeed = ttk.Label(self.parametersSub2, anchor='w', text='')
        self.lRewindSpeed = ttk.Label(
            self.parametersSub2, anchor='w', text='Rewind Speed')
        self.vRewindSpeed = ttk.Label(self.parametersSub2, anchor='w', text='')
        # self.lTemplateSize          = ttk.Label(self.templateSizer, text='Size')
        # self.lTemplateSizePlain     = ttk.Label(self.templateSizer, width=config.BUTTON_WIDTH, text='')
        self.lTesterConnection = ttk.Label(
            self.connectionToTester, textvariable=self.connection_state)
        # , font='Lato')
        self.lState = ttk.Label(
            self.systemState, textvariable=self.parent.state_name)

        self.ePositions = ttk.Entry(
            self.parametersSub1, width=config.ENTRY_WIDTH)
        self.ePositionsTotal = ttk.Entry(
            self.parametersSub1, width=config.ENTRY_WIDTH)
        self.ePacketsPerPosition = ttk.Entry(
            self.parametersSub1, width=config.ENTRY_WIDTH)
        self.eDistanceToMarker = ttk.Entry(
            self.parametersSub1, width=config.ENTRY_WIDTH)
        # enties have a binding to the enter button, to be able to enter a value and the confirm it with enter
        self.ePositions.bind("<Return>", lambda e:
                             self.onClickSetPositions(self.ePositions,
                                                      self.ePositionsTotal))
        self.ePositionsTotal.bind("<Return>", lambda e:
                                  self.onClickSetPositionsTotal(self.ePositions,
                                                                self.ePositionsTotal))
        self.ePacketsPerPosition.bind("<Return>", lambda e:
                                      self.onClickSetPackagesPerPosition(self.ePacketsPerPosition,
                                                                         self.ePositions,
                                                                         self.ePositionsTotal))
        self.eDistanceToMarker.bind("<Return>", lambda e:
                                    self.onClickSetDistanceTestingMarking(self.eDistanceToMarker))

        # bind entriy widgts to corresponding loosfocus callbacks to be able to reset them if user
        # dous not click ar the corresponig set buttons
        self.ePositions.bind("<FocusOut>", self.onLooseFocusSetPositions)
        self.ePositionsTotal.bind(
            "<FocusOut>", self.onLooseFocusSetPositionsTotal)
        self.ePacketsPerPosition.bind(
            "<FocusOut>", self.onLooseFocusPackagesPerPosition)
        self.eDistanceToMarker.bind(
            "<FocusOut>", self.onLooseFocusDistanceTestingMarking)

        # self.canvTesterConnection = Canvas(self.fConnectionInfo, width=20, height=20)

        # self.bGettingStarted.grid       (row=7, column=1, sticky="ew")
        # self.bHelp.grid                 (row=7, column=2, sticky='w')
        # self.bAutoSetUp.grid            (row=7, column=3, sticky="ew")
        # self.lModuleThumbnailLabel.grid (row=1, column=0, sticky="nw")
        self.lNameModule.grid(row=1, column=1, sticky="nesw")
        self.lModuleThumbnail.grid(row=2, column=1, sticky="nesw")
        # self.fThumbnailFrame.grid       (row=8, rowspan=4, column=3, )
        # self.rbChoosePos.grid           (row=8, column=1)
        # self.rbChoosePack.grid          (row=8, column=2)
        self.cbChoosePosPack.grid(row=1, column=1, rowspan=2, padx=10)
        self.lPositions.grid(row=1, column=2, sticky="e",
                             padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.ePositions.grid(row=1, column=3, sticky="w",
                             padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bPositions.grid(row=1, column=4, sticky="e", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.lPositionsTotal.grid(
            row=2, column=2, sticky="e", padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.ePositionsTotal.grid(
            row=2, column=3, sticky="w", padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bPositionsTotal.grid(row=2, column=4, sticky="e", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        # self.infoPositionsTotal.grid    (row=2, column=5, sticky="w")
        self.lPacketsPerPosition.grid(
            row=3, column=1, sticky="e", columnspan=2, padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.ePacketsPerPosition.grid(
            row=3, column=3, sticky="w", padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bPacketsPerPosition.grid(row=3, column=4, sticky="e", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.lDistanceToMarker.grid(row=4, column=1, sticky="e", columnspan=2,
                                    padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.eDistanceToMarker.grid(
            row=4, column=3, sticky="w", padx=config.PAD_WIDGETS_X, pady=config.PAD_WIDGETS_Y)
        self.bDistanceToMarker.grid(row=4, column=4, sticky="e", padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.bSetAll.grid(row=5, column=3, sticky="e", columnspan=2, padx=(
            config.PAD_WIDGETS_X, 0), pady=config.PAD_WIDGETS_Y)
        self.lGearSpeed.grid(row=1, column=1, sticky="w",
                             padx=config.PAD_WIDGETS_X, pady=7)
        self.vGearSpeed.grid(row=1, column=3, sticky="w",
                             padx=config.PAD_WIDGETS_X, pady=7)
        self.sGearSpeed.grid(row=1, column=2, sticky="w",
                             padx=config.PAD_WIDGETS_X, pady=7)
        self.lDriveSpeed.grid(row=2, column=1, sticky="w",
                              padx=config.PAD_WIDGETS_X, pady=7)
        self.vDriveSpeed.grid(row=2, column=3, sticky="w",
                              padx=config.PAD_WIDGETS_X, pady=7)
        self.sDriveSpeed.grid(row=2, column=2, sticky="w",
                              padx=config.PAD_WIDGETS_X, pady=7)
        self.lRewindSpeed.grid(row=3, column=1, sticky="w",
                               padx=config.PAD_WIDGETS_X, pady=7)
        self.vRewindSpeed.grid(row=3, column=3, sticky="w",
                               padx=config.PAD_WIDGETS_X, pady=7)
        self.sRewindSpeed.grid(row=3, column=2, sticky="w",
                               padx=config.PAD_WIDGETS_X, pady=7)
        # self.bSetAll.grid               (row=24, column=2, sticky="w")
        # self.bTemplateB.grid            (row=1, column=1, sticky="w")
        # self.bTemplateS.grid            (row=1, column=2, sticky="w")
        # self.bTemplateR.grid            (row=1, column=3, sticky="w")
        # self.lTemplateSize.grid         (row=1, column=2, sticky="w")
        # self.lTemplateSizePlain.grid    (row=1, column=1, sticky="w")
        # self.bTemplate1.grid            (row=1, column=4, columnspan=3, sticky="e")
        # self.templateSizer.grid         (row=6, column=1, columnspan=4, sticky="ew")
        # self.bDriveTime.grid            (row=26, column=1, columnspan=2, sticky="e")
        self.lTesterConnection.grid(row=0, column=1, sticky="w")
        # self.canvTesterConnection.grid  (row=0, column=2, sticky="nsw")
        # self.fConnectionInfo.grid       (row=27, column=1)
        self.lState.grid(row=1, column=1)

        # self.fThumbnailFrame.columnconfigure(1, weight=1)
        self.eDistanceToMarker.insert(
            0, self.parent.distance_testing_marking.get())
        self.ePositions.insert(0, self.parent.packages.get())
        self.ePositionsTotal.insert(0, self.parent.packages_total.get())
        self.ePacketsPerPosition.insert(
            0, self.parent.packages_per_position.get())

        # bind combobox selcted event to callback, also call <Button-1> (normal click)
        # combobox
        self.cbChoosePosPack.bind("<<ComboboxSelected>>",
                                  lambda x: self.onClickPosPackView(self.ePositions, self.ePositionsTotal))

        # binding of defaultwise non-clickable widgets is done here, bc this function is only called once and because of some
        # weird tkinter behaviour, multiple calls to bind() (bind the same callback repeatedly to the same widget) lead to a
        # memory leak
        # labels
        self.lPositions.bind("<Button-1>", self.get_help_msg_callback)
        self.lPositionsTotal.bind("<Button-1>", self.get_help_msg_callback)
        self.lPacketsPerPosition.bind("<Button-1>", self.get_help_msg_callback)
        self.lGearSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.vGearSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.lDriveSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.vDriveSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.lRewindSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.vRewindSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.lDistanceToMarker.bind("<Button-1>", self.get_help_msg_callback)
        # entries
        self.ePositions.bind("<Button-1>", self.get_help_msg_callback)
        self.ePositionsTotal.bind("<Button-1>", self.get_help_msg_callback)
        self.ePacketsPerPosition.bind("<Button-1>", self.get_help_msg_callback)
        self.eDistanceToMarker.bind("<Button-1>", self.get_help_msg_callback)
        # comboboxes
        self.cbChoosePosPack.bind("<Button-1>", self.get_help_msg_callback)
        # scales
        self.sGearSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.sDriveSpeed.bind("<Button-1>", self.get_help_msg_callback)
        self.sRewindSpeed.bind("<Button-1>", self.get_help_msg_callback)
        # frames
        self.connectionToTester.bind("<Button-1>", self.get_help_msg_callback)
        self.packageType.bind("<Button-1>", self.get_help_msg_callback)

        #  populate the menu bar
        self.parent.menu_help.add("command",
                                  label="Getting Started",
                                  command=self.onClickGettingStarted)
        self.parent.menu_help.add("command",
                                  label="Help",
                                  command=self.onClickHelp)
        self.parent.menu_help.add("command",
                                  label="About",
                                  command=self.onClickAbout)

        self.parent.menu_setup.add("command",
                                   label="Auto Set Package",
                                   command=lambda: self.onClickAutoSetPackage(False))

        self.parent.menu_setup.add("command",
                                   label="SetUp Package",
                                   command=self.onClickSetUpPackage)

        self.parent.menu_setup.add("command",
                                   label="Set Drivetimeout",
                                   command=self.onClickSetDriveTimeout)

        self.parent.menu_control.add("command",
                                     label="New Template",
                                     command=self.onClickTemplate)
        self.parent.menu_control.add("command",
                                     label="Match Template",
                                     command=self.onClickMatchTemplate)
        self.parent.menu_control.add("command",
                                     label="Resize Template",
                                     command=lambda: self.onClickTemplate(resize=True))
        self.parent.menu_control.add("command",
                                     label="Reset Templatesize",
                                     command=lambda: self.onClickTemplate(reset=True))

        self.onClickSetGearSpeed(None, self.vGearSpeed)
        self.onClickSetDriveSpeed(None, self.vDriveSpeed)
        self.onClickSetRewindSpeed(None, self.vRewindSpeed)

        self.parent.menu_control.entryconfig("New Template", state="normal")
        self.parent.menu_control.entryconfig("Resize Template", state="normal")
        self.parent.menu_control.entryconfig(
            "Reset Templatesize", state="normal")

    def setup_help_msg_dict(self):
        """set up the help_msg_dict, a dictionary where you can access the corresponding help
        messages via the widget object. the widget object is accessable by event.widget, where
        event is always given to the callback bound by bind"""
        # labels
        self.help_msg_dict[self.lPositions] = help.LABEL_POSITIONS_MSG
        self.help_msg_dict[self.lPositionsTotal] = help.LABEL_POSITIONSTOTAL_MSG
        self.help_msg_dict[self.lPacketsPerPosition] = help.LABEL_PACKPERPOS_MSG
        self.help_msg_dict[self.lGearSpeed] = help.LABEL_GEARSPEED_MSG
        self.help_msg_dict[self.vGearSpeed] = help.LABEL_GEARSPEED_MSG
        self.help_msg_dict[self.lDriveSpeed] = help.LABEL_DRIVESPEED_MSG
        self.help_msg_dict[self.vDriveSpeed] = help.LABEL_DRIVESPEED_MSG
        self.help_msg_dict[self.lRewindSpeed] = help.LABEL_REWINDSPEED_MSG
        self.help_msg_dict[self.vRewindSpeed] = help.LABEL_REWINDSPEED_MSG
        self.help_msg_dict[self.lDistanceToMarker] = help.LABEL_DISTTOMARK_MSG
        # entries
        self.help_msg_dict[self.ePositions] = help.ENTRY_POSITIONS_MSG
        self.help_msg_dict[self.ePositionsTotal] = help.ENTRY_POSITIONSTOTAL_MSG
        self.help_msg_dict[self.ePacketsPerPosition] = help.ENTRY_PACKPERPOS_MSG
        self.help_msg_dict[self.eDistanceToMarker] = help.ENTRY_DISTTOMARK_MSG
        # combobox
        self.help_msg_dict[self.cbChoosePosPack] = help.POS_PACK_VIEW_MSG
        # scales
        self.help_msg_dict[self.sGearSpeed] = help.LABEL_GEARSPEED_MSG
        self.help_msg_dict[self.sDriveSpeed] = help.LABEL_DRIVESPEED_MSG
        self.help_msg_dict[self.sRewindSpeed] = help.LABEL_REWINDSPEED_MSG
        # frames
        self.help_msg_dict[self.connectionToTester] = help.FRAME_TESTERCONN_MSG
        self.help_msg_dict[self.packageType] = help.FRAME_PACKAGE_TYPE_MSG

    def update_module_thumbnail(self, thumbnail):
        """fit module image (height) to 2 gridrows and show it in the main window.
        arg:
            thumbnail: PIL.Image Object: the module type template to be shown in the mainwindow

        PhotoImage is a tkinter object type that is needed to show a image in a label, but
        resizing is very poorly implemented for this object type, so the given argument must be
        PIL.Image type to be resizable.
        """
        thumbnail.thumbnail((self.bPositions.winfo_height()
                            * 10, self.bPositions.winfo_height() * 2))
        self.module_thumbnail = ImageTk.PhotoImage(thumbnail)
        self.lModuleThumbnail.config(
            image=self.module_thumbnail, borderwidth=2, relief="groove")

    def connection_changed(self, state):
        """changes the color of the connection status dot according of the state

        args:
            state: the new connection state: c .. connected (green)
                                             d .. disconnected (red)
                                            else .. connecting/undefined (blue)
        """
        if state == "c":
            self.connection_state.set("Connected")
        elif state == "d":
            self.connection_state.set("Disconnected")
        else:
            self.connection_state.set("Disconnected - Trying to Reconnect")

    def set_state(self, state):
        """disables or enabled the buttons according to the state

        parameters are only changeable in idle mode
        """
        # change color of 'set drive timeout' button to indicate when it has to be set (red)
        # if self.parent.drive_timeout_set:
        #     self.bDriveTime.config(bg='SpringGreen2')
        # else:
        #     self.bDriveTime.config(bg="firebrick1")

        # print_msg(self, "Setting state to: " + str(States(state).name))
        # in both states all buttons should be activated.

        if state == States.IDLE or state == States.HELP:
            for w in self.winfo_children():  # iterates through all childs of the parameter frame
                if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                    w.state(['!disabled'])
                # children of frames need to be iterated through too
                if isinstance(w, ttk.LabelFrame):
                    for w_sub in w.winfo_children():
                        if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                            w_sub.state(['!disabled'])
                        if isinstance(w_sub, ttk.Frame):
                            for w_subsub in w_sub.winfo_children():
                                if isinstance(w_subsub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                                    w_subsub.state(['!disabled'])
            # enable all menu entries
            # update is condtional bc if one updates the state callbacks are canceled bc of some reason
            # -> with the frequenzt gui updates in idle state, buttons could not be pressed anymore
            if not self.parent.menubar.entrycget("Control", "state") == "normal":
                self.parent.menubar.entryconfig("Control", state="normal")
                self.parent.menubar.entryconfig("Setup", state="normal")
                self.parent.menubar.entryconfig("Help", state="normal")

        else:
            for w in self.winfo_children():
                if isinstance(w, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                    w.state(['disabled'])
                if isinstance(w, ttk.LabelFrame):
                    for w_sub in w.winfo_children():
                        if isinstance(w_sub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                            w_sub.state(['disabled'])
                        if isinstance(w_sub, ttk.Frame):
                            for w_subsub in w_sub.winfo_children():
                                if isinstance(w_subsub, (ttk.Entry, ttk.Button, ttk.Checkbutton, ttk.Scale, ttk.Radiobutton)):
                                    w_subsub.state(['disabled'])
            # disable all menu entries
            self.parent.menubar.entryconfig("Control", state="disabled")
            self.parent.menubar.entryconfig("Setup", state="disabled")
            self.parent.menubar.entryconfig("Help", state="disabled")

    def DoublecheckPackage(self, package_name, img_path):
        """ calls a window to check if the rght package is detected 
            + displays image of package"""
        checkpackage = tk.Toplevel()
        checkpackage.title("Check Package")

        # Add a message to the dialog
        message = tk.Label(checkpackage, text="Is this the right package?" +
                           "\n" + str(package_name) + str(img_path))
        message.pack()
        # Load the image
        #image_path = Image.open(img_path)
        img = ImageTk.PhotoImage(Image.open(img_path))
        # Create a Label widget to display the image
        img_label = tk.Label(checkpackage, image=img)
        img_label.pack()
        # Function to handle "Yes" button click

        def on_yes():
            checkpackage.destroy()
            return True
        # Function to handle "No" button click

        def on_no():
            checkpackage.destroy()
            return False
        # Add "Yes" and "No" buttons
        yes_button = tk.Button(checkpackage, text="Yes", command=on_yes)
        yes_button.pack(side="left")
        no_button = tk.Button(checkpackage, text="No", command=on_no)
        no_button.pack(side="right")
