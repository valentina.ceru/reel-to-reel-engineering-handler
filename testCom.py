import select
import socket
import struct
import threading
import time
import pickle

from cmdPrinter import print_msg, print_dbg, CMDColor
from configuration import HandlerConfig as config
from programStates import TestStates


class SocketException(Exception):
    """a exception that is used to mask connection errors (like socket.error, OSError)"""
    pass


class UnexpectedResponseException(Exception):
    """a exception that is used to indicate incoming data, that is not expected"""
    pass


class TesterTimeoutException(Exception):
    """exception that is used to indicate a socket.timeout"""
    pass


class TestCom():
    """class, that implements the communication with the tester and the communication protocol

    for the connection the socket module is used. the handler acts as the server and waits for the
    tester to connect to the socket.

    the sending and receiving of the messages are done in an extra thread to prevent freezing the
    app. if there is no connection established, the thread tries to reconnect automatically.

    the test procedure is controlled via a test status that indicates the current point in the communication
    protocol. the processing of incoming message is triggered by main and should be done periodically.
    if there is no test running main should trigger a no_action message periodically to prevent the connection
    from closing.
    """

    def __init__(self, parent):
        """ constructor

        initiates the tcp server and starts the send/recv thread
        """
        self.parent = parent
        self.test_state = TestStates.IDLE

        # used to lock the send/recv message bc of multithreading
        self.msg_lock = threading.Lock()
        self.received_string = ""
        self.recv_msg = None
        self.send_msg = None
        self.recvd_msg_not_result = 0
        self.new_test = False
        self.clear_received_msg = False
        self.distance_to_testing = config.DISTANCE_TO_TESTING
        with open('list.pickle', 'rb') as file:
            self.position_test = pickle.load(file)

        # used to describe communication errors in case a test is started but no connection is
        # established. if this is the case because the first connection after startup is not set up,
        # this default message is used
        self.last_error_description = "not yet connected"
        self.force_close = False

        # response watchdog is used to detect communication errors that are not leading to socket error
        self.response_watchdog = False
        self.send_message_time = None

        # init new ip/tcp socket (AF_INET - IPv4, SOCK_STREAM - TCP)
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # bind connects the socket to a ip address at the local machine.self
        # so devices in the network can connect to this socket using the ip
        # bind() -> local adapter, connect() -> ip of server (used by client)
        try:
            self.server_socket.bind((config.TCP_HOST, config.TCP_PORT))
            self.server_socket.listen()  # start listening for connections

        except OSError as error:
            print_msg(self, str(error), color_loc=CMDColor.BG_RED)
            self.spea_connection = None

        # accepting connections is done in receiver thread
        self.spea_connection = None
        self.spea_address = None

        self.communication_thread = threading.Thread(target=self._loop)
        self.communication_thread.start()

    def _loop(self):
        """receiver/sender loop"""
        print_msg(self, "Started receiver/sender loop...")
        while True:
            # if handler is shut down. force close socket and end task with return
            # !! no lock held here -> do not try to get mutex after changing self.force_close to True
            if self.force_close:
                self._disconnect()
                return

            with self.msg_lock:
                try:
                    self._check_connection()
                    self._receive_msg()
                    self._send_msg()
                except SocketException as s:
                    self.response_watchdog = False
                    self._set_connection("d")
                    self.last_error_description = s
            time.sleep(0.3)  # to prevent high cpu usage

    def _check_connection(self):
        # connection gets set to none if disconnecting was detected -> try reconnect
        if self.spea_connection is None:
            self._try_reconnect()

        # select returns readable, writeable and sockets with a "special status"
        # if socket is not readable the connection is faulty
        r, w, x = select.select([], [self.spea_connection], [])
        if self.spea_connection not in w:
            self._try_reconnect()

        # if tester takes too long to reply to a no_action command (not in a test run) the tester is stuck,
        # not running or an other connection error, thats not detected by the socket has occurred
        # example: physical disconnection
        if self.response_watchdog\
           and (self.send_message_time + config.TCP_IDLE_TIMEOUT) < time.time():
            print_msg(self, "Response timeout, resetting connection, send: {}, now: {}".format(
                self.send_message_time, time.time()))
            self._try_reconnect()

    def _try_reconnect(self):
        """disconnect socket if it is not already disconnected and tries to reestablish the connection"""
        self.response_watchdog = False
        if self.spea_connection is not None:
            self._disconnect()
        self._set_connection("r")
        self._connect()

    def _connect(self, timeout=config.TCP_CONNECTING_TIMEOUT):
        """waits for the client to reconnect to the socket"""
        try:
            self.server_socket.settimeout(timeout)
            self.spea_connection, self.spea_address = self.server_socket.accept()  # this is blocking
            self.spea_connection.settimeout(config.TCP_SENDRECV_TIMEOUT)
            self._set_connection("c")

        except socket.timeout as t:
            print_msg(self, str(t), color_loc=CMDColor.BG_RED)
            self.spea_connection = None
            raise SocketException(t)
        except socket.error as s:
            print_msg(self, str(s), color_loc=CMDColor.BG_RED)
            self.spea_connection = None
            raise SocketException(s)

    def _disconnect(self):
        """disconnects the socket using LINGER (to close it immediately) and sets the socket to Null"""
        # print_msg(self, "disconnecting tester")
        self.response_watchdog = False
        if self.spea_connection is not None:
            # to prevent "fd already used" error bc socket is stuck in time wait state
            # after use
            self.spea_connection.setsockopt(socket.SOL_SOCKET,
                                            socket.SO_LINGER,
                                            struct.pack('ii', 1, 0))
            self.spea_connection.close()
            self.spea_connection = None  # disconnected socket is set to None
            self.spea_address = None
        self._set_connection("d")

    def _set_connection(self, connection_state):
        """changes the connection state variable

        lock is needed bc main thread also needs the variable (for set_state)
        """
        with self.parent.connection_flag_lock:
            if connection_state == "c":
                print_msg(self, "Connection established")
                self.parent.connection_state = "c"
            elif connection_state == "d":
                print_msg(self, "Disconnected from Tester")
                self.parent.connection_state = "d"
            elif connection_state == "r":
                print_msg(self, "Trying to connect to Tester")
                self.parent.connection_state = "r"

    def _receive_msg(self):
        """checks if there is pending data to read in the socket, and if there is, read it into a receive buffer

        checks the receive buffer for a completed message (ended with  \n) and put it into recv_msg in case
        """
        # no connection detected
        if self.spea_connection is None:
            pass
        # if no message is pending -> try read a new one
        elif self.recv_msg is None:
            try:
                readable_connections, _, _ = select.select(
                    [self.spea_connection], [], [], 0)
                if readable_connections:
                    new_data = self.spea_connection.recv(
                        config.TCP_BUFFER_SIZE).decode('utf-8')
                    # empty string recvd -> means connection was closed by peer
                    if new_data == "":
                        # disconnecting from handler side, new loop iteration will reconnect
                        self._disconnect()
                    else:
                        self.received_string += new_data
                if "\n" in self.received_string:  # check for end of message identifier
                    # watchdog watches time between sending and receiving
                    self.response_watchdog = False
                    # split the received string once: in msg (b4 \n) and rest
                    self.recv_msg, self.received_string = self.received_string.split(
                        "\n", 1)
                elif len(self.received_string) > config.MAX_RECV_CHAR:
                    print_msg(self,
                              "More than {} char were read, but no delimiter was found"
                              .format(config.MAX_RECV_CHAR),
                              color_loc=CMDColor.BG_RED)
                    raise UnexpectedResponseException("More than {} char were read, "
                                                      "but no delimiter:\n"
                                                      .format(config.MAX_RECV_CHAR)
                                                      + self.received_string)
            except socket.error as s:
                print_msg(self, "Exception:\n" + str(s),
                          color_loc=CMDColor.BG_RED,
                          color_msg=CMDColor.RED)
                raise SocketException(str(s))

    def _send_msg(self):
        # no connection detected
        if self.spea_connection is None:
            pass

        elif self.send_msg is not None:  # no message to send -> just return
            try:
                self.spea_connection.sendall(
                    (self.send_msg + "\n").encode("utf-8"))
            except socket.timeout as t:
                print_msg(self, str(t), color_loc=CMDColor.BG_RED)
                self.spea_connection = None
                raise SocketException(t)
            except socket.error as s:
                print_msg(self, str(s), color_loc=CMDColor.BG_RED)
                self.spea_connection = None
                raise SocketException(s)
            finally:  # finally is always executed -> send msg must always be set to None or it would be sent again
                self.send_msg = None

    def test(self, missing_fail_package):
        """is called if handler is in testing mode. call periodically.

        handles the test procedure

        returns: None:  if no check was done (no lock acquired or no QUERY at test start and clear_received_msg)
                 False: if test procedure was checked but no result was yielded yet
                 Result list: if Test run finished
        """

        # acquire with blocking=False does NOT block until the lock is acquired, instead it returns
        # None if lock is not acquirable
        if not self.msg_lock.acquire(False):
            return None
        # response watchdog gets deactivated, bc the test has already a timeout check in main thread
        self.response_watchdog = False
        if self.new_test:  # resets the test state, so handler waits for QUERY and starts new test, gets set by main before init a new test
            self.test_state = TestStates.IDLE
            self.new_test = False
        try:
            if self.recv_msg:
                print_dbg("recv_msg", self.recv_msg)
                # if clear_received_msg is set, every msg that is not QUERY gets answered by NO ACTION
                # and the test is not started yet. also the receiving string is cleared from possible (multiple)
                # received messages that would lead to errors. this is done to prevent errors when retrying
                # a test after timeout and the tester would send the result later, before the expected query.
                if self.clear_received_msg:
                    print_dbg("rcvd string", self.received_string)
                    if "QUERY" not in self.recv_msg:
                        print_msg(self, "Got a unexpected message in clear_received_msg mode: "
                                        + str(self.recv_msg),
                                  color_loc=CMDColor.BG_RED, color_msg=CMDColor.BG_RED)
                        self.received_string = ""
                        self.recv_msg = None
                        self.send_msg = "NO ACTION"
                        return None
                    else:
                        self.clear_received_msg = False
                # if tcom is in idle -> wait for QUERY
                if self.test_state == TestStates.IDLE and "QUERY" in self.recv_msg:
                    self.test_state = TestStates.WAIT_TEST_RUN
                    # appended to (RE)TEST are the sites to be tested, starting with 1 (range 1, ...)
                    # parents packages attribute specifies the number of packages to be tested ->
                    # = sites (1 .. +1 because of tester starting counting with 1 not 0)
                    with open('list.pickle', 'rb') as file:
                        self.position_test = pickle.load(file)
                    print_msg(self, self.position_test)
                    if not missing_fail_package:
                        self.send_msg = ("RETEST " if self.parent.retest else "TEST ")\
                            + ",".join(self.position_test.pop(0) for i in range(1,
                                                                                self.parent.packages.get() + 1))
                    else:
                        self.send_msg = ("RETEST " if self.parent.retest else "TEST ")\
                            + ",".join(self.position_test.pop(0) for i in range(1,
                                                                                self.parent.packages.get() + 1))
                    with open('list.pickle', 'wb') as file:
                        pickle.dump(self.position_test, file)
                    print_msg(self, self.position_test)

                    # recvd_msg_not_result counts the messages that are received when waiting for result
                    # like PACKAGE_ID, -_POS,... if this value passes a limit, an error is triggered,
                    # bc if too many msgs are received that are not the test result, something is most
                    # likely wrong
                    self.recvd_msg_not_result = 0

                # if tcom is in WAIT_TEST_RUN and TEST RUN is received, send ack to start test
                elif self.test_state == TestStates.WAIT_TEST_RUN and "TEST RUN" in self.recv_msg:
                    self.test_state = TestStates.WAIT_RESULT
                    self.send_msg = "ACK"
                    self.recvd_msg_not_result += 1

                # if tcom is in WAIT_RESULT and RESULT is received, go into IDLE
                elif self.test_state == TestStates.WAIT_RESULT and "RESULT" in self.recv_msg:
                    self.test_state = TestStates.IDLE
                    # remove identifier and whitespaces at beginning and end
                    result = self.recv_msg.replace("RESULT", "").strip()
                    self.send_msg = "ACK"
                    # start watchdog after test bc tester should be sending query now
                    self.start_response_watchdog()
                    # print_dbg(self, "result list before altering: {}".format(result))
                    result_list = result.split(",")
                    # print_dbg(self, "result list after first altering (split): {}".format(result_list))
                    # slice the list to get the right number of tested packages
                    result_list = result_list[0:self.parent.packages.get()]
                    # print_msg(self, "got results back: {}".format(result_list))
                    self.recv_msg = None
                    return result_list

                # other messages that could occur before TEST RUN or RESULT are handled here
                elif self.test_state == TestStates.WAIT_TEST_RUN\
                        or self.test_state == TestStates.WAIT_RESULT:
                    self.recvd_msg_not_result += 1
                    if "TESTER_DEVICE_ID" in self.recv_msg:
                        pass  # nothing to do -> msg data is not handled
                    elif "DEVICE_ID" in self.recv_msg:
                        # send dummy device ids (site nr.)
                        self.send_msg = "DEVICE_ID "\
                                        + ",".join(str(i)
                                                   for i
                                                   in range(1, self.parent.packages.get() + 1))
                    elif "DEVICE_POS" in self.recv_msg:
                        # send site pos list in x;y format, where x is tape direction, y the index at a tape position
                        # y is in range packages_per_pos, while x is in range positions
                        self.send_msg = "DEVICE_POS "\
                                        + ",".join((str(int(i /
                                                            self.parent.packages_per_position.get()))
                                                    + ";"
                                                    + str(i %
                                                          self.parent.packages_per_position.get())
                                                    for i in range(0, self.parent.packages.get())))
                    else:
                        msg = self.recv_msg
                        self.recv_msg = None
                        self.send_msg = "NO ACTION"
                        # start watchdog after no action bc tester should be sending query now
                        self.start_response_watchdog()
                        raise UnexpectedResponseException("Unexpected msg from Tester: \"{}\""
                                                          .format(msg))

                else:
                    msg = self.recv_msg
                    self.recv_msg = None
                    self.send_msg = "NO ACTION"
                    # start watchdog after no action bc tester should be sending query now
                    self.start_response_watchdog()
                    raise UnexpectedResponseException("Unexpected msg from Tester: \"{}\""
                                                      .format(msg))

                self.recv_msg = None
                # if tester sent more than 10 messages that are not RESULT, something is most likely wrong
                if self.recvd_msg_not_result >= config.MAX_RECV_MSG_NO_RESULT:
                    self.recvd_msg_not_result = 0
                    self.send_msg = "NO ACTION"
                    # start watchdog after no action bc tester should be sending query now
                    self.start_response_watchdog()
                    raise UnexpectedResponseException(
                        "Too many messages, no RESULT")
            else:
                pass
            return False
        # use try..finally to always release lock if aquired
        finally:
            self.msg_lock.release()

    def no_action(self):
        """sends NO ACTION to the tester to keep the connection busy.

        should be called periodically by main thread if there is no test running.
        after sending the response watchdog is started to limit the time the tester may need for a response at idle.
        this can detect errors that are not detectable by the socket module (for example a physical disconnect)
        """
        # acquire with blocking=False does NOT block until the lock is acquired, instead it returns
        # None if lock is not acquirable
        if not self.msg_lock.acquire(False):
            return
        try:
            # the flags get reset here too, in case the test run was aborted and the handler
            # is going back to sending NO ACTION
            self.new_test = False
            self.clear_received_msg = False

            if self.recv_msg:
                print_dbg("msg ", self.recv_msg)
                print_dbg("string ", self.received_string)

                # resetting rcvd string to prevent pending messages if tester sends 2 messages
                # (at test timeout for example)
                self.received_string = ""
                self.send_msg = "NO ACTION"
                self.start_response_watchdog()
                # if msg is not QUERY -> print warning but no exception is raised bc it is not a real
                # problem at this point
                if "QUERY" not in self.recv_msg:
                    print_msg(self, "unexpected msg recived from tester:" + str(self.recv_msg),
                              color_loc=CMDColor.BG_RED, color_msg=CMDColor.BG_RED)
                self.recv_msg = None
        # use try..finally to always release lock if acquired
        finally:
            self.msg_lock.release()

    def start_response_watchdog(self):
        """starts the response watchdog by setting the according flag to true.
        the current system time stored in send_message_time is later used to determine the response time.
        """
        self.response_watchdog = True
        self.send_message_time = time.time()
