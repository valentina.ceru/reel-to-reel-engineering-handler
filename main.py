
"""entrance file for handler software. contains and starts main process

class:
    MainGUI: the main system including gui window and the system logic
"""
# export DISPLAY=:0.0

import serial
import RPi.GPIO as GPIO

import time
import multiprocessing as mpc
import threading
import queue

from tkinter import *
# import tkinter as tk
from tkinter import ttk
from ttkthemes import themed_tk as tk  # TODO: update GUI with themed tk
from tkinter import font

import numpy as np
import helper
from helper import mcuMessages as MCU
from configuration import HandlerConfig as config
from programStates import States, Modes, WindupStates
from cmdPrinter import print_msg, print_dbg, CMDColor
from errorHandler import HandlerException, HandshakeException

import parameterFrame
import controlFrame
import counterFrame
import dialogHandler
import pickle

import testCom
import videoCounter
from videoCounter import VideoCounterMessages as vcm


class MainGUI(tk.ThemedTk):
    # class MainGUI(tk.Tk):
    """main process containing the gui and the control logic for the handler

    the class handles the handlers system logic. the system can go into different states
    (see programStates.States), depending on the current task. the transition between the states is
    triggered by a buttonclick (e.g. by the user) or by a event triggered by a subprocess.

    the video counter process is a subprocesses of this. the communication uses a thread.
    communication between processes is handled by a set of 3 multiprocessing.queue. 2 a set as a
    handshake kommunication line, the third is used as a event lne from subprocess to main. the
    event line is not always listened to (depending on the state), to prevent a forbidden state
    transition to be triggered. the same procedure is done to the buttons, which are only enabled in
    the state their event is allowed to happen.

    most gui parts are in extra frame areas (parameterframe, controlframe, counterframe). this is
    due to the massive amount of code the gui implementation causes. this code is put into extra
    files to keep the main file at a reasonable size.
    the same procedure is done for dialogs, which are implemented in the dialoghandler class, and
    the tryput method, that abstracts putting a item into the queue excepting the queue.full
    exception.
    """

    def __init__(self, parent):
        """constructor

        initializes the class members. state get set to startup. most variables get initialized by
        the data in the config file. instead of normal int object extIntVar is used, a extension of
        the tkinter IntVar (see helper.py). this is due to the easier interaction with the tkiner
        gui parts using the IntVar. The extension is done to enable arithmetic and logic operators
        IntVar has not implemented.

        serial port is enabled. used for communication with the mcu.
        GPIO output is enabled. this is used for the interrupt line between raspberry and mcu
        (used to stop the handler when reaching the desired position)

        the communication queues and subprocesses are started and waited for to signal to be ready.

        the gui is built via _create_gui_parts using the frame files and the dialoghandler gets
        instantiated.
        """
        # tk.Tk.__init__(self, parent)
        tk.ThemedTk.__init__(self, parent)
        # self.style = ttk.Style()
        # self.style.theme_use("default")
        #print_msg(self, str(self.get_themes()))
        self.theme_list = self.get_themes()
        self.theme_idx = 0
        #print_msg(self, str(font.families()))
        self.set_theme("scidblue")
        print_dbg(ttk.Style().lookup("TLabel", "font"))
        # self.set_theme("clam")
        # self.set_theme("classic")

        print_msg(self, "starting up...", where="at start")
        self.title("PackageTest")

        self.parent = parent
        self.state = States.STARTUP
        self.windup_state = WindupStates.STOPPED
        self.mode = Modes.AUTO
        self.state_name = StringVar()
        self.state_name.set(States(self.state).name)
        # self.geometry("2000x500")

        # debug
        self.debug_m = None
        self.debug_value = 0

        # #parameter values
        self.positions = helper.extIntVar()            # positions to be tested at once
        self.positions.set(config.POSITIONS)
        self.positions_total = helper.extIntVar()      # total positions to be tested
        self.positions_total.set(config.POSITIONS_TOTAL)
        self.packages_per_position = helper.extIntVar()
        self.packages_per_position.set(config.PACKAGES_PER_POSITION)
        self.packages = helper.extIntVar()      # packages to be tested at once
        self.packages.set(config.POSITIONS * config.PACKAGES_PER_POSITION)
        self.packages_total = helper.extIntVar()      # total packages to be tested
        if config.POSITIONS_TOTAL != (-1):
            self.packages_total.set(
                config.POSITIONS_TOTAL * config.PACKAGES_PER_POSITION)
        else:
            self.packages_total.set(config.POSITIONS_TOTAL)

        # dow many positions from die 1 pos to marking pos
        self.distance_testing_marking = helper.extIntVar()
        self.distance_testing_marking.set(config.DISTANCE_TESTING_MARKING)

        self.drive_to_target_pos = 0  # specifies the target position in DRIVE_TO mode
        self.gear_speed = helper.extIntVar()  # forward speed in positioning mode
        self.gear_speed.set(config.GEAR_SPEED)
        self.drive_speed = helper.extIntVar()  # forward speed in manual drive mode
        self.drive_speed.set(config.DRIVE_SPEED)
        self.rewind_speed = helper.extIntVar()
        self.rewind_speed.set(config.REWIND_SPEED)

        self.missing_package_path = StringVar()
        self.fail_package_path = StringVar()
        self.indicator_missing_fail_package = False
        self.position_fail = []
        self.indicator_wrong_package = False
        self.counter_until_fail = 0

        # #counter values
        self.counter = helper.extIntVar()              # count position not packages
        self.counter_packages = helper.extIntVar()        # count packages
        self.counter_total = helper.extIntVar()        #
        # marking targets specify positions where min 1 die failed,
        self.next_marking_target = helper.extIntVar()
        self.next_marking_target.set(-1)
        self.marking_targets = []
        # list of all failed pieces from the current run (also already marked ones)
        self.failed_pos_history = []
        self.failed_pos_listvar = StringVar()
        self.failed_pos_listvar.set([])

        # count packages (position * packages_per_position)
        self.packages_tested = helper.extIntVar()
        self.packages_passed = helper.extIntVar()
        self.packages_failed = helper.extIntVar()
        self.packages_marked = helper.extIntVar()
        self.pass_ratio = StringVar()

        # #control variables
        self.pause_flag_marking = False
        self.pause_flag_testing = False
        self.pause_flag_counting = False
        self.safed_paused_state = None

        self.remark = False
        self.retest = False
        self.last_testrun_result = None

        self.handle_drive_timeout = False
        self.drive_timeout_set = False

        # timeout fort testing
        self.test_timeout_start_time = None

        # time estimation
        self.estimated_time = StringVar()
        self.estimated_time.set("")
        self.testing_time = None
        self.counting_time = None
        self.test_start_time = None

        # #serial communication
        self.portUSB0 = serial.Serial(config.SERIAL_PORT_MCU, baudrate=config.SERIAL_BAUDRATE_MCU,
                                      timeout=1.0)
        self.portUSB0.read(100)  # read init message if it exists
        print_msg(self, "Set up serial connection to mcu...")

        # #Interrupt
        # GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(40, GPIO.OUT)

        # connection state is locked, bc it is changed by the comm thread but also used by set_state do
        # determine the buttons to enable
        self.connection_flag_lock = threading.Lock()
        self.connection_state = "d"  # d-disconnectd, c-connected, r-reconnecting

        # cursor style
        self.cursor = 'left_ptr'

        # #GUI stuff
        self.dialog_handler = dialogHandler.DialogHandler(self)
        self._create_GUI_parts()
        # binds _cleanup() to closing window
        self.protocol("WM_DELETE_WINDOW", self._cleanup)

        # #multiprocessing and communication

        # used for event triggered system flow, also preparation for
        # marking module
        self.from_marker = mpc.Queue()
        # used to simulate process communication
        self.MARKING_NEEDED = 1

        self.tCom = testCom.TestCom(self)
        print_msg(self, "Started tCom...")

        self.handshake_to_vid = mpc.Queue()
        self.handshake_from_vid = mpc.Queue()
        self.from_vid = mpc.Queue()
        self.videoCounter = videoCounter.VideoCounter(self.handshake_to_vid,
                                                      self.handshake_from_vid, self.from_vid)
        # set subprocess as deamon, so it's no need to wait for it to finish when closing app
        self.videoCounter.daemon = True
        self.videoCounter.start()

        try:
            try:
                if self.from_vid.get(True, config.STARTUP_TIMEOUT)[0] is not vcm.READY:
                    raise HandlerException(
                        "Startup Error: got wrong response from Video Counter")
                print_msg(self, "Started video counting process...")
                print_msg(self, "videocounter PID:" +
                          str(self.videoCounter.pid))
            except queue.Empty:
                raise HandlerException(
                    "Startup error: Did not get response in time (queue.Empty")
        except HandlerException as h:
            print_msg(self, str(h), color_loc=CMDColor.BG_PURPLE,
                      color_msg=CMDColor.BG_PURPLE)
            print_msg(self, "Aborting StartUp",
                      color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
            self._cleanup()
            return

        self.set_state(States.IDLE)

    def _create_GUI_parts(self):
        """builds the gui parts using the _frame files."""

        print_msg(self, "creating the parts of the gui...")

        # create menubar
        self.menubar = Menu(self)
        self.menu_control = Menu(self.menubar, tearoff=0)
        self.menu_setup = Menu(self.menubar, tearoff=0)
        self.menu_help = Menu(self.menubar, tearoff=0)
        self.menubar.add("cascade", label="Control", menu=self.menu_control)
        self.menubar.add("cascade", label="Setup", menu=self.menu_setup)
        self.menubar.add("cascade", label="Help", menu=self.menu_help)
        self.configure(menu=self.menubar)

        # create subframes
        self.parameter_frame = parameterFrame.ParameterFrame(
            self, padding=config.PADDING)
        self.parameter_frame.grid(row=0, column=0, sticky=N + S)
        self.control_frame = controlFrame.ControlFrame(
            self, padding=config.PADDING)
        self.control_frame.grid(row=0, column=1, sticky=N + S)
        self.counter_frame = counterFrame.CounterFrame(
            self, padding=config.PADDING)
        self.counter_frame.grid(row=0, column=3, sticky=N + S)

        style = ttk.Style(self)
        style.configure('Debug.TButton')
        debugB = ttk.Button(self, text="debug",
                            command=self.debugCallback, style='Debug.TButton')
        debugB.grid(row=1, column=10)

    def _check_pause_flag_testing(self):
        """checks the pause flag for the testing pause and changes the state to pause if true

        the pause flag is set by the pause button before or while testing. it acts
        like a pending pause request.. the Testing state is stored in safed pause state to enable
        returning to the right state at resume.

        returns:
            if pause flag was set (True / False)
        """
        if self.pause_flag_testing:
            print_msg(self, "checking testing pause flag: flag = True")
            self.safed_paused_state = States.TESTING
            self.stop_handler(stop_drive_timer=True)
            self.set_state(States.PAUSE)
            self.pause_flag_testing = False
            self.control_frame.bPauseTesting.state(["!pressed"])
            return True
        return False

    def _check_pause_flag_marking(self):
        """see check_pause_flag_testing"""
        if self.pause_flag_marking:
            print_msg(self, "checking marking pause flag: flag = True")
            # self.safed_new_state = newstate
            self.safed_paused_state = States.MARKING
            self.stop_handler(stop_drive_timer=True)
            self.set_state(States.PAUSE)
            self.pause_flag_marking = False
            self.control_frame.bPauseMarking.state(["!pressed"])
            return True
        return False

    def _check_pause_flag_counting(self):
        """see check_pause_flag_testing"""
        if self.pause_flag_counting:
            print_msg(self, "checking marking pause flag: flag = True")
            # self.safed_new_state = newstate
            self.safed_paused_state = States.RUNNING
            self.stop_handler(stop_drive_timer=True)
            self.set_state(States.PAUSE)
            self.pause_flag_counting = False
            return True
        return False

    def set_state(self, newstate):
        """sets the state to newstate and calls the frames setstate methods to dis/enable the
        concurring buttons.

        connection flag lock is used bc of dependence of control button state from connection established flag
        """
        with self.connection_flag_lock:
            self.parameter_frame.set_state(newstate)
            self.parameter_frame.connection_changed(self.connection_state)
            # self.parameter_frame.onClickPosPackView(self.parameter_frame.ePositions, self.parameter_frame.ePositionsTotal)

            self.control_frame.set_state(newstate)
            self.counter_frame.set_state(newstate, self.state)
            self.dialog_handler.targetListInfo_setButtonState(enable=True
                                                              if newstate is States.IDLE
                                                              else False)
            self.state = newstate
            self.state_name.set(States(self.state).name)

    def set_mode(self, newmode):
        """sets the systems mode to newmode"""

        print_msg(self, "changing state from " + Modes(self.mode).name + " to " + Modes(newmode).name,
                  color_msg=CMDColor.YELLOW)
        self.mode = newmode

    def reset_handler(self, no_handshake=False, reset_counter=False):
        """stops the handler and returns it so the IDLE

        args:
            no_handshake (bool): defines, if the subprocesses should be contacted using the
                handshake line. if the reset is caused by faulty communication, this must be called
                with True, to prevent another communication attempt that is most likey to fail.
            reset_counter (bool): defines if the counter should be set to the default values
        """
        self.stop_handler(stop_drive_timer=(not no_handshake))
        self.control_frame.stop_windup()
        self.pause_flag_testing = False
        self.pause_flag_marking = False
        self.safed_parent_state = None
        self.remark = False
        self.retest = False
        self.handle_drive_timeout = False

        if reset_counter:
            self.reset_counter()
        self.set_state(States.IDLE)

    def reset_counter(self):
        """resets the counter and target values to the default values"""
        self.counter.set(0)
        self.counter_packages.set(0)
        self.counter_total.set(0)
        self.marking_targets.clear()
        self.failed_pos_history.clear()
        self.failed_pos_listvar.set([])
        self.next_marking_target.set(-1)
        self.packages_tested.set(0)
        self.packages_passed.set(0)
        self.packages_failed.set(0)
        self.packages_marked.set(0)
        self.pass_ratio.set("")
        self.estimated_time.set("")
        self.counting_time
        self.testing_time = None
        self.counter_frame.update_test_result_visuals(None)

    def stop_handler(self, stop_drive_timer=False):
        """stopping hte handlers movement and disables the drive timeout watchdog

        args:
            stop_drive_timer (bool): defines, if the video counting subprocess is informed to stop
                the timeout watchdog. if the stop was caused by faulty communication, this must be
                called with False, to prevent another communication attempt that is most likely to
                fail.
        """
        print_msg(self, "stopping handler movement")
        self.portUSB0.write(MCU.STOP_ALL)  # stop motor
        self.portUSB0.write(MCU.TOP_VALVE_UP)  # top valve up
        self.portUSB0.write(MCU.BOT_VALVE_DOWN)  # bottom valve down
        if stop_drive_timer:
            self.handle_drive_timeout = False
            try:
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.STOP_TIMEOUT,), vcm.READY)
            except HandshakeException:
                pass

    def start_positioning(self):
        """starts the handler and setups the video counting process to stop at a defined position.

        if the stopping position is the next marking or testing position depends on the current
        mode, if the marking-targets list is empty, and on the distance to testing and marking
        target (the nearest is driven to). the videocounting process only gets a single value
        pointing at a position where it stops the handler at. drive timeout handling is enabled
        after startup was successful.
        """
        print_dbg("mode", Modes(self.mode).name,
                  "state", States(self.state).name)
        print_dbg("next_marking_target", self.next_marking_target.get(), "marking_targets",
                  self.marking_targets[0] if self.marking_targets else "Empty")
        print_dbg("couter_total", self.counter_total.get(
        ), "counter_packages", self.counter_packages, "counter", self.counter.get())
        print_dbg("pos_total", self.positions_total.get(),
                  "pos", self.positions.get())
        print_dbg("retest", self.retest, "remark", self.remark)

        self.last_testrun_result = None  # reset last testrun to none bc we drive to new pos
        self.set_state(States.RUNNING)
        if self.control_frame.ValveTopStop.get() == True:
            self.portUSB0.write(MCU.TOP_VALVE_UP)  # top valve up
        if self.control_frame.ValveBotStop.get() == True:
            self.portUSB0.write(MCU.BOT_VALVE_DOWN)  # bottom valve down
        helper.clear_queue(self.from_vid)
        try:
            if (  # check if there are still pos to test or infinite pos
                (self.counter_total < (self.positions_total - self.positions)
                 or self.positions_total.get() == -1)
                # check if mode is TEST_ONLY, or if AUTO,
                # next test pos is nearer than next marking pos
                and (self.mode is Modes.TEST_ONLY
                     or (self.mode is Modes.AUTO
                         and (not self.marking_targets
                              or ((self.positions - self.counter).get()
                                  < (self.marking_targets[0][0] - self.counter_total.get())
                                  )
                              )
                         )
                     )
            ):
                print_msg(self, "start driving to next position - testing")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START, (self.positions - self.counter).get()),
                                       vcm.READY)

            elif (
                    (self.mode is Modes.AUTO
                     or self.mode is Modes.MARKING_ONLY)
                    and self.marking_targets
            ):
                print_msg(self, "start driving to next position - marking")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START,
                                        (self.marking_targets[0][0] - self.counter_total.get())),
                                       vcm.READY)

            elif (
                    self.mode is Modes.DRIVE_TO_POS
                    # and self.counter_total.get() < self.drive_to_target_pos
            ):
                print_msg(self, "start driving to specified pos")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START,
                                        self.drive_to_target_pos - self.counter_total.get()),
                                       vcm.READY)
            elif (
                    self.mode is Modes.COUNTING_ONLY
            ):
                # counting only gives -2 as counting target. (negative numbers are never reached by the videocounter)
                # (-1 is already used as a control value in the videocounter class)
                # counting ends with the watchdog signaling the end of the tape
                print_msg(self, "start for counting only")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START,
                                        -2),
                                       vcm.READY)
            elif (
                    self.mode is Modes.COUNTING_ONLY_BACKWARDS
            ):
                print_msg(self, "start for counting only")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START,
                                        -3),
                                       vcm.READY)

                # show some help if needed
                if self.state is States.HELP:
                    self.dialog_handler.showHelp(help.REWIND_MSG)
                    return

                # self.stop_handler()
                # self.set_state(States.DRIVE)
                # self.control_frame.stop_windup()
                if self.control_frame.ValveTopStop.get() == True:
                    self.portUSB0.write(MCU.TOP_VALVE_UP)   # top valve up
                if self.control_frame.ValveBotStop.get() == TRUE:
                    # bottom valve down
                    self.portUSB0.write(MCU.BOT_VALVE_DOWN)
                self.control_frame.auto_windup(MCU.AUTOWINDUP_L)
                time.sleep(1)
                self.portUSB0.write(MCU.START_REWIND(self.rewind_speed.get()))
            elif (
                    self.mode is Modes.FEED_IN
            ):
                # counting only gives -2 as counting target. (negative numbers are never reached by the videocounter)
                # (-1 is already used as a control value in the videocounter class)
                # counting ends with the watchdog signaling the end of the tape
                print_msg(self, "start for detecting first package")
                self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                       (vcm.START,
                                        -4),
                                       vcm.READY)

            else:  # no packages left to test/mark
                self.reset_handler()
                return
        except HandshakeException:
            return

        self.handle_drive_timeout = True  # drive timeout error gets enabled
        GPIO.output(40, GPIO.HIGH)  # set interrupt output interruptable

        if (
                self.mode is not Modes.COUNTING_ONLY_BACKWARDS
        ):
            # self.portUSB0.write(MCU.IR_ON)  # enable ir
            self.control_frame.auto_windup(MCU.AUTOWINDUP_R)
            # self.portUSB0.write(MCU.CLOSE_BRIDGE)  # close gear-bridge
            # start driving (interruptable)
            time.sleep(1)
            # start driving while setting the mcu to stop at gpio interrupt
            self.portUSB0.write(MCU.START_INTERRUPTABLE(self.gear_speed.get()))

    def start_testing(self):
        """starts the test process.

        sets up all flags, the state and sends the needed commands to the mcu.
        the time for the test timeout and the test time calculation gets initialized.
        set the new_test flag in tCom, so a call to tCom.test() triggers a new test run. (gets reset by tCom)
        then check-test_progress is scheduled to 1 ms (pretty much instantly). this is done
        to use the handling of tCom.test()'s return value implemented in check_test_progress().
        """
        print_msg(self, "start test process")
        #print_msg(self, self.position_test.pop())
        self.test_start_time = time.time()
        self.test_timeout_start_time = time.time()
        self.portUSB0.write(MCU.STOP_ALL)  # stop motor
        if self.control_frame.ValveTopStop.get() == TRUE:
            self.portUSB0.write(MCU.TOP_VALVE_DOWN)  # top valve up
        if self.control_frame.ValveBotStop.get() == TRUE:
            self.portUSB0.write(MCU.BOT_VALVE_UP)  # bottom valve down
        self.set_state(States.TESTING)
        self.tCom.new_test = True  # gets reset by tCom
        self.after(1, self.check_test_progress)

    def check_test_progress(self):
        """calls tCom.test() and handles the occurring return value.

            if the result from tCom.test is None, the msg lock was not acquired, or the tester did
            not send QUERY after the user retried a test run after a timeout.
            if it is False, the test runs, but it did not yield a result yet.
            in this both cases, the timeout gets checked and this function gets rescheduled.

            if a return was yielded, it gets returned by tCom.test()
            in this case the result of the test process gets processed, according to the outcome.

        """
        # if state is not TESTING, the call was scheduled before the user pressed STOP.
        # so this return prevents an ongoing test after STOP
        if self.state != States.TESTING:
            return
        try:
            #print_msg(self, self.videoCounter.position_test.get())
            result = self.tCom.test(self.indicator_missing_fail_package)
            # self.indicator_missing_fail_package = False
            self.position_fail = []
            if result:
                self._update_testing_time()
                print_msg(self, "got test results: " + str(result))
                # if this was a retest -> undo all changes done due to the last testrun
                if self.retest:
                    # retest result same as last testrun result -> no change needed
                    if self.last_testrun_result == result:
                        pass
                    else:
                        # filter out all entries from the last test in marking targets
                        # and failed pos history

                        # all positions that were tested at the last testrun are possible failed
                        # and therefore possibly in the marking target and history list.
                        # to get the positions add the recent total position to every position
                        # that was tested in the last run (last_result / packages per position).
                        # note that this test positions, calculated by length and packages per pos
                        # are relative to the pos of test site 1 which is at position 0, so its more
                        # an index, indexing the position on the tape
                        possible_failed_positions = [i + self.counter_total.get()
                                                     for
                                                     i in range(len(self.last_testrun_result)
                                                                // self.packages_per_position
                                                                       .get()
                                                                )]
                        print_msg(self, "possible_failed_positions",
                                  str(possible_failed_positions))
                        print_msg(self, "self.marking_targets",
                                  self.marking_targets)
                        if self.mode is Modes.AUTO:
                            # print_dbg("reducing targets")
                            # generate a new marking targets list, that contains all elements of the old
                            # one, EXCEPT the one that correlate to any position in the possible_faild_pos
                            # list.
                            # note that in possible_failed_pos, the positions are stored at test position,
                            # while in marking target it is stored for the marking positions. so before
                            # comparing one need to substract the distance from test to marking pos.
                            self.marking_targets = [t for t
                                                    in self.marking_targets
                                                    if not t[0] - self.distance_testing_marking.get()
                                                    in possible_failed_positions]
                        print_msg(self, "self.marking_targets",
                                  self.marking_targets)

                        # same procedure as with marking target, but at failed pos history the test
                        # pos is stored, not the marking pos
                        self.failed_pos_history = [t for t
                                                   in self.failed_pos_history
                                                   if not t[0]
                                                   in possible_failed_positions]
                        self.failed_pos_listvar.set(self.failed_pos_history)
                        # undo counter incrementation
                        self.packages_tested -= len(self.last_testrun_result)
                        # a result "1" means passed, everythin else fail
                        self.packages_passed -= self.last_testrun_result.count(
                            "1")
                        self.packages_failed -= (len(self.last_testrun_result)
                                                 - self.last_testrun_result.count("1"))
                # update counters
                self.packages_tested += len(result)
                self.packages_passed += result.count("1")
                self.packages_failed += (len(result) - result.count("1"))

                # generate a dictionary of positions and sites on a position to represent all failed packages.
                # the key is the position of the failed package, while the value is a list of indexes, that
                # represent the packages on the position defined by key.
                # note that position means in this context the index of the position i the current test,
                # so if you test 6 packages with 2 packages per pos, the possible keys are 0,1,2
                failed_pos = dict()
                # numerate yields a list of indexes corresponding to
                for site_idx, result in enumerate(result):
                    # the given list and the given list
                    if result == "1":   # result 1 means pass, so no need to put it into failed pos dict
                        pass
                    else:
                        # pos defines the position on the tape in direction of the tape while y defines
                        # the location of the package on the position (pos), so its the position in direction
                        # normal to the tape direction
                        pos = site_idx // self.packages_per_position.get()
                        y = site_idx % self.packages_per_position.get()
                        # packages are stored in position on tape, (tuple: sites on position)
                        # this enabled the storage of multiple failed packages on one position
                        # in one list-entry
                        if pos not in failed_pos:
                            # if pos was not put into dict yet we need to make a new entry
                            failed_pos[pos] = []
                        failed_pos[pos].append(y)
                self.counter_frame.update_test_result_visuals(failed_pos)
                # add all failed pos to marking targets (if mode == AUTO) and failed history
                for pos_idx, y_list in failed_pos.items():
                    if self.mode is Modes.AUTO:
                        # pos_idx 0 is referring to the position at the current counter_total, so is needs to be added before
                        # pushing the value into marking targets
                        # furthermore the distance to marking must be added
                        self.marking_targets.append(
                            [pos_idx + self.counter_total.get() + self.distance_testing_marking.get(), y_list])
                    # (see 4 lines above)
                    # every package on a position gets an entry in history, if any failed package is on this position.
                    # failed packages get a F (they must occur in y_list) the passed a P
                    self.failed_pos_history.append([pos_idx + self.counter_total.get(),
                                                    ["F" if i in y_list else "P" for i in range(self.packages_per_position.get())]])
                    self.failed_pos_listvar.set(self.failed_pos_history)
                print_dbg("marking targets", self.marking_targets)
                self.next_marking_target.set(self.marking_targets[0][0]
                                             if self.marking_targets
                                             else (-1))
                self.pass_ratio.set("{:.2f}".format(self.packages_passed.get()
                                                    / self.packages_tested.get()
                                                    * 100))
                self.last_testrun_result = result
                self.counter.set(0)
                self.counter_packages.set(0)
                if self._check_pause_flag_testing():
                    # if pause is entered after testing, every following test is a retest
                    self.retest = True
                else:
                    self.retest = False  # reset retest flag when start moving
                    self.start_positioning()
            else:
                # if no result was returned by tCom, check if timeout was exceeded, and if it was
                # ask user if the handler should keep waiting. if no timeout wsa reached,
                # reschedule the result check
                if time.time() > self.test_timeout_start_time + config.TESTER_TIMEOUT:
                    # the popup times out after TCOM_COM_TIMEOUT_POPUP_TIMEOUT and the handler
                    # keeps waiting
                    timeout_handling = self.dialog_handler.resumeableErrorPopup(self,
                                                                                "Timeout while waiting for "
                                                                                "response from tester",
                                                                                timeout=(config.TCOM_COM_TIMEOUT_POPUP_TIMEOUT * 1000))
                    if timeout_handling in ("Resume", None):
                        self.test_timeout_start_time = time.time()
                    else:
                        raise testCom.TesterTimeoutException("Tester timeout was reached and the user"
                                                             "chose to abort testing.")
                self.after(300, self.check_test_progress)

        except (testCom.UnexpectedResponseException, testCom.TesterTimeoutException) as t:
            # unexpectedesponse or testertimeout a raised by tcom, most possibly a timeout or
            # a misfunction of the tester
            print_msg(self, "Something was wrong with the communication to tester. "
                            + str(t), color_loc=CMDColor.BG_RED)
            task = self.dialog_handler.retryPauseCancelErrorPopup(self,
                                                                  str(t),
                                                                  where="Communication with Tester:",
                                                                  block=False)
            if task == "Retry":
                # to prevent error bc of tester sending result later, reset in tCom.test()
                self.tCom.clear_received_msg = True
                self.start_testing()
            if task == "Cancel":
                self.reset_handler()
            if task == "Pause":
                self.stop_handler()
                self.pause_flag_testing = True
                # retest flag is not set to true bc if the first test is not working there
                # is no finished test and if it was a retest that failed, it is already set
                self._check_pause_flag_testing()
        except testCom.SocketException as s:
            # socketexception gets raised by tester and indicates a connection-related error
            # bc this is something that should only happen if Tcom is not able to automatically
            # reconnect, this error leads to a stop of the test run
            print_msg(self, "something was wrong with the communication to tester\n"
                            + str(s), color_loc=CMDColor.BG_RED)
            self.dialog_handler.errorPopup(self, str(s))
            self.reset_handler()

    def tester_no_action(self):
        """ if there is no test running at the moment, the handler sends no action to the tester
        to keep the connection alive"""
        if self.state != States.TESTING:
            self.tCom.no_action()
        self.after(2000, self.tester_no_action)

    def start_marking(self):
        """stopped the handler, sets stat and sends marking request to queue

        the queue and request handling is all handled in this request. this is only preparation for
        working with an extra marking module"""
        print_msg(self, "start marking")
        self.portUSB0.write(MCU.STOP_ALL)  # stop motor
        self.set_state(States.MARKING)
        helper.clear_queue(self.from_marker)
        helper.try_put(self, self.from_marker, (self.MARKING_NEEDED, self.marking_targets[0][1]),
                       self._cleanup)

    def set_drive_timeout(self):
        """starts the set procedure of the drive timeout."""
        print_msg(self, "setting driver timer timeout")
        self.set_state(States.SET_DRIVE_TIMEOUT)
        self.portUSB0.write(MCU.TOP_VALVE_UP)  # top valve up
        self.portUSB0.write(MCU.BOT_VALVE_DOWN)  # bottom valve down
        helper.clear_queue(self.from_vid)
        try:
            # make new template first to use current position for timeout measurement
            # -> prevents wrong timeout value when previously tracked template is not at the start
            # position at the moment
            self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                   (vcm.TEMPLATE, None), vcm.READY)
            self.command_handshake(self.handshake_to_vid, self.handshake_from_vid,
                                   (vcm.SET_TIMEOUT, 0), vcm.READY)
        except HandshakeException:
            return

        GPIO.output(40, GPIO.HIGH)  # set interrupt output interruptable
        # self.portUSB0.write(MCU.IR_ON)  # enable ir
        self.control_frame.auto_windup(MCU.AUTOWINDUP_R)
        # self.portUSB0.write(MCU.CLOSE_BRIDGE)  # close gear-bridge
        # start driving (interruptable)
        self.portUSB0.write(MCU.START_INTERRUPTABLE(self.gear_speed.get()))

    def command_handshake(self, q_out, q_in, command, handshake_response):
        """command handshake ensures the communication process to be successful.self

        after sending a command to a subprocess, the response is waited for. if the response is
        unexpected, a error popup is called and the user is informed. timeouts (queue.Full, .Empty)
        do cause a instant system shutdown. communication errors are always threated as deadly
        (causing return to idle or shutdown), because if the communication between processes is
        faulty, there is no way the correct behavior of the system can be ensured.
        this should in fact not happen at all, because mostly this will be caused by a subprocess
        that gets terminated.

        args:
            q_out (queue): the queue where the command is put.
            q_in (queue): the queue on which the response is waited for.
            command (object): the command that is sent to the subprocess.
            response (object): the expected response from the subprocess.
        """
        try:
            try:
                helper.clear_queue(q_out)
                helper.clear_queue(q_in)
                q_out.put_nowait(command)
                response = q_in.get(True, config.HANDSHAKE_REPLY_TIMEOUT)
                if response[0] is not handshake_response:
                    raise HandlerException(
                        "Handshake Error: got wrong response" + str(response[0]))
            except queue.Full:
                print_msg(self, "Deadly communication exception raised",
                          color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
                self._cleanup()
                raise HandshakeException()
            except queue.Empty:
                raise HandlerException(
                    "Handshake error: Did not get response in time")
        except HandlerException as h:
            print_msg(self, str(h), color_loc=CMDColor.BG_PURPLE,
                      color_msg=CMDColor.BG_PURPLE)
            if self.dialog_handler.fatalErrorPopup(self, str(h)):
                self.reset_handler(no_handshake=True)
            else:
                self._cleanup()
            raise HandshakeException()

    def _update_estimated_time(self):
        """updated the estimated time if counting time and testing time are set"""
        print_msg(self, "")
        # print_dbg("testing", self.testing_time, "counting", self.counting_time, "setup_done", self.timeout_setup_done)
        # check if testing/counting time are set. also if timeout is not set,
        # counting time is invalid
        if self.testing_time is None or self.counting_time is None or not self.drive_timeout_set:
            return
        # with infinite packages total, time cannot be estimated
        if self.packages_total == -1:
            return

        est_seconds = np.clip(int((((self.positions_total - self.counter_total) * self.counting_time)
                                   + ((self.packages_total - self.packages_tested) * self.testing_time / self.packages))
                                  .get()), 0, None)

        # time shows one hour too much

        # print_dbg(est_seconds)
        self.estimated_time.set(time.strftime(
            "%H:%M:%S", time.localtime(est_seconds - 3600)))

    def _update_testing_time(self):
        """updated the testing time corresponding to the current system time

        updating the time is done via "floating mean"
        """
        if self.testing_time is None:
            self.testing_time = time.time() - self.test_start_time
        else:
            self.testing_time = (self.testing_time +
                                 time.time() - self.test_start_time) / 2

    def marking_stop_needed(self):
        """checks if stop for marking is needed and start the corresponding task

        returns:
            true if a stop was needed, False else

        if pause flag is set, pause is triggered. at this place, the pause is triggered before the
        state change. so when going to pause, marking was not done yet. that leads to a automatic
        marking when resuming, except a manuel test was triggered via the retest button.
        """
        stop_is_needed = False
        if (self.mode is Modes.AUTO or self.mode is Modes.MARKING_ONLY)\
           and self.marking_targets\
           and self.counter_total.get() == self.marking_targets[0][0]:
            stop_is_needed = True
            if (self._check_pause_flag_marking()):
                return stop_is_needed
            self.start_marking()
        return stop_is_needed

    def testing_stop_needed(self):
        """checks if stop for testing is needed and start the corresponding task

        returns
            true if a stop was needed, False else

        if pause flag is set, pause is triggered. at tis place, the pause is triggered before the
        state change. so when going to pause, testing was not done yet. that leads to a automatic
        testing when resuming, except a manuel test was triggered via the retest button.
        """
        stop_is_needed = False
        if (self.mode is Modes.AUTO or self.mode is Modes.TEST_ONLY)\
           and (self.counter.get() == self.positions.get() or self.counter.get() == 0):
            stop_is_needed = True
            if self._check_pause_flag_testing():
                return stop_is_needed
            self.start_testing()
        return stop_is_needed

    def drive_to_pos_stop_needed(self):
        """checks if the stop was necessary bc of a target pos to be driven to"""
        stop_is_needed = False
        print_dbg("mode", Modes(self.mode).name, "counter", self.counter_total.get(
        ), "drive_to_target_pos", self.drive_to_target_pos)
        if self.mode is Modes.DRIVE_TO_POS and self.drive_to_target_pos == self.counter_total.get():
            self.reset_handler()
            stop_is_needed = True
        return stop_is_needed

    def _check_messages(self):
        """checks the event queues for incoming messages, depending on the system state

        events from video counter are only treated in state RUNNING or SET_DRIVE_TIMEOUT.
        the restriction prevents unexpected system behavior at the
        occurrence of events at a time they shouldn't happen.
        unexpected messages cause system shutdown because that can only mean that somewhere a
        untreated message is implemented.
        """
        if self.state == States.RUNNING or self.state == States.SET_DRIVE_TIMEOUT or self.state == States.IDLE:
            while not self.from_vid.empty():
                msg = self.from_vid.get_nowait()
                print_msg(self, "got message: " + str(msg))

                if msg[0] == vcm.COUNT:
                    self.counter += 1
                    #self.position_test = msg[1]
                    print_dbg("self.counter_packages", self.counter_packages)
                    print_dbg("self.counter_packages_type",
                              type(self.counter_packages))
                    print_dbg("new_val", self.counter.get() *
                              self.packages_per_position.get())
                    self.counter_packages.set(
                        self.counter.get() * self.packages_per_position.get())
                    self.counter_total += 1
                    self._update_estimated_time()

                elif msg[0] == vcm.COUNTBACK:
                    self.counter -= 1
                    print_dbg("self.counter_packages", self.counter_packages)
                    print_dbg("self.counter_packages_type",
                              type(self.counter_packages))
                    print_dbg("new_val", self.counter.get() *
                              self.packages_per_position.get())
                    self.counter_packages.set(
                        self.counter.get() * self.packages_per_position.get())
                    self.counter_total -= 1
                    self._update_estimated_time()

                elif msg[0] == vcm.STOP:
                    # if a stop occurred where it shouldn't have, something is seriously wrong.
                    # the stop_needed functions handle and start the required actions in case of a valid stop
                    if not (self.marking_stop_needed()
                            or self.testing_stop_needed()
                            or self.drive_to_pos_stop_needed()):
                        self.reset_handler()
                        if not self.dialog_handler.fatalErrorPopup(self,
                                                                   "STOP was received from the Video Counter"
                                                                   " where no STOP should be."):
                            self._cleanup()

                elif msg[0] == vcm.TIMEOUT_VALUE:
                    self.stop_handler()
                    self.dialog_handler.driveTimeoutInfo(
                        msg[1] * config.WATCHDOG_TIMEOUT_FACTOR)
                    self.counting_time = msg[1]
                    self.drive_timeout_set = True
                    self.reset_handler(reset_counter=True)

                elif msg[0] == vcm.WATCHDOG:
                    self.reset_handler()
                    if not self.dialog_handler.fatalErrorPopup(self,
                                                               "WATCHDOG: Timeout for counting reached.\n"
                                                               "One or more packages might have been missed."):
                        self._cleanup()

                elif msg[0] == vcm.END_OF_TAPE:
                    self.stop_handler()
                    self.reset_handler()
                    self.dialog_handler.EndofTapePopup()

                elif msg[0] == vcm.FIRST_PACKAGE:

                    self.stop_handler()
                    self.reset_handler()
                    self.dialog_handler.FirstPackagePopup()
                    self.parameter_frame.onClickAutoSetPackage(True)
                    self.control_frame.ValveTopStop.set(1)

                elif msg[0] == vcm.CHECK_PACKAGE:
                    if self.dialog_handler.DoublecheckPackage(msg[1], msg[2]) == False:
                        self.parameter_frame.onClickSetUpPackage()

                elif msg[0] == vcm.SETVALVE:
                    if msg[1] == 'MOB6':
                        self.control_frame.ValveTopStop.set(1)
                        self.control_frame.ValveBotStop.set(0)
                    elif msg[1] == 'MOA8':
                        self.control_frame.ValveTopStop.set(1)
                        self.control_frame.ValveBotStop.set(0)
                    elif msg[1] == 'LTO':
                        self.control_frame.ValveTopStop.set(1)
                        self.control_frame.ValveBotStop.set(0)
                    elif msg[1] == 'CLM_CDIF':
                        if self.dialog_handler.onClickCLMorCDIF() == True:
                            self.control_frame.ValveTopStop.set(1)
                            self.control_frame.ValveBotStop.set(1)
                        else:
                            self.control_frame.ValveTopStop.set(1)
                            self.control_frame.ValveBotStop.set(0)
                    elif msg[1] == 'SIM':
                        self.control_frame.ValveTopStop.set(1)
                        self.control_frame.ValveBotStop.set(0)

                else:
                    print_msg(self, "Got unknown message id from videoCounter" + str(msg[0]),
                              color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
                    self._cleanup()

        elif self.state == States.MARKING:
            while not self.from_marker.empty():
                msg = self.from_marker.get_nowait()

                if msg[0] == self.MARKING_NEEDED:  # trigger marking popup
                    print_dbg("marking msg:", msg)
                    self.dialog_handler.markingPopup(msg[1])
                    # if its a remarking process -> do nothing
                    # this prevents that the counter has a invalid value
                    # and the marking target list gets corrupted.
                    print_dbg("making_needed - ", self.remark)
                    if self.remark:
                        pass
                    else:
                        self.packages_marked += self.packages_per_position
                        self.marking_targets.pop(0)
                        self.next_marking_target.set(self.marking_targets[0][0]
                                                     if self.marking_targets
                                                     else (-1))
                    if self._check_pause_flag_marking():
                        self.remark = True
                    elif not self.testing_stop_needed():  # might start testing task -> testing is always checked after marking
                        self.start_positioning()  # no testing on this pos needed

        self.after(70, self._check_messages)

    def _gui_update(self):
        """gui update is periodically done, because the enabled buttons depend on variables that might
        have changed since last update. for example the connection state."""
        self.set_state(self.state)
        self.after(70, self._gui_update)

    def _check_children_alive(self):
        """check periodically if subprocesses/threads have terminated and shuts down system if one did."""
        # is_alive is a function of threading and multiprocessing
        if not self.tCom.communication_thread.is_alive():
            print_msg(self, "tCom has died -> killing program", color_loc=CMDColor.BG_PURPLE,
                      color_msg=CMDColor.BG_PURPLE)
        elif not self.videoCounter.is_alive():
            print_msg(self, "videoCounter has died -> killing program",
                      color_loc=CMDColor.BG_PURPLE, color_msg=CMDColor.BG_PURPLE)
        # add other children
        else:
            # calls _check_children_alive after 70 ms
            self.after(70, self._check_children_alive)
            return

        # if any child as found dead
        self._cleanup()

    def _cleanup(self):
        """ensures that the handler is stopped if the system shuts down."""
        self.portUSB0.write(MCU.STOP_ALL)  # stop motor
        self.portUSB0.write(MCU.TOP_VALVE_UP)  # top valve up
        self.portUSB0.write(MCU.BOT_VALVE_DOWN)  # bottom valve down
        self.handle_drive_timeout = False

        # signal tcom thread to suicide and wait for its termination
        # bc sockets should be closed.
        self.tCom.force_close = True
        self.tCom.communication_thread.join()

        self.destroy()

    def debugCallback(self):
        GPIO.output(40, GPIO.HIGH)

        """print_dbg("setting theme " + self.theme_list[self.theme_idx])
        self.set_theme(self.theme_list[self.theme_idx])

        style = ttk.Style(self)
        # style.configure('Debug.TButton', bordercolor="red2")
        style.configure('Debug.TButton', background="red2")

        self.theme_idx = self.theme_idx + 1
        if self.theme_idx == len(self.theme_list):
            self.theme_idx = 0
        """
        if self.debug_value == 0:
            self.portUSB0.write(MCU.IR_OFF)  # enable infrared barrier
            self.windup_state = WindupStates.STOPPED
            self.portUSB0.write(MCU.AUTOWINDUP_R)
            self.debug_value = 1
        elif self.debug_value == 1:
            self.portUSB0.write(MCU.IR_ON)  # enable infrared barrier
            self.windup_state = WindupStates.AUTO
            self.portUSB0.write(MCU.AUTOWINDUP_R)
            self.debug_value = 2
        elif self.debug_value == 2:
            self.portUSB0.write(MCU.IR_NOSPACER)  # enable infrared barrier
            self.windup_state = WindupStates.AUTO
            self.portUSB0.write(MCU.AUTOWINDUP_R)
            self.debug_value = 0


if __name__ == "__main__":
    """entrance point for handler software

    instantiate the gui window (main system process). sets the systems
    state to idle after init is complete. calls subprocess monitoring
    function (_check_children_alive()) and message check function
    (_check_messages), that checks, if event triggering massage arrived.
    these functions rescedule themself using .after().
    calls .mainloop to start the event processing loop (tkinter).
    """
    root = MainGUI(None)
    root.after(70, root._gui_update)
    root.after(70, root._check_children_alive)
    root.after(70, root._check_messages)
    root.after(70, root.tester_no_action)
    root.mainloop()
